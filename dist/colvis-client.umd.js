(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["colvis-client"] = factory();
	else
		root["colvis-client"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "014b":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__("e53d");
var has = __webpack_require__("07e3");
var DESCRIPTORS = __webpack_require__("8e60");
var $export = __webpack_require__("63b6");
var redefine = __webpack_require__("9138");
var META = __webpack_require__("ebfd").KEY;
var $fails = __webpack_require__("294c");
var shared = __webpack_require__("dbdb");
var setToStringTag = __webpack_require__("45f2");
var uid = __webpack_require__("62a0");
var wks = __webpack_require__("5168");
var wksExt = __webpack_require__("ccb9");
var wksDefine = __webpack_require__("6718");
var enumKeys = __webpack_require__("47ee");
var isArray = __webpack_require__("9003");
var anObject = __webpack_require__("e4ae");
var isObject = __webpack_require__("f772");
var toObject = __webpack_require__("241e");
var toIObject = __webpack_require__("36c3");
var toPrimitive = __webpack_require__("1bc3");
var createDesc = __webpack_require__("aebd");
var _create = __webpack_require__("a159");
var gOPNExt = __webpack_require__("0395");
var $GOPD = __webpack_require__("bf0b");
var $GOPS = __webpack_require__("9aa9");
var $DP = __webpack_require__("d9f6");
var $keys = __webpack_require__("c3a1");
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function' && !!$GOPS.f;
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__("6abf").f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__("355d").f = $propertyIsEnumerable;
  $GOPS.f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__("b8e3")) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
// https://bugs.chromium.org/p/v8/issues/detail?id=3443
var FAILS_ON_PRIMITIVES = $fails(function () { $GOPS.f(1); });

$export($export.S + $export.F * FAILS_ON_PRIMITIVES, 'Object', {
  getOwnPropertySymbols: function getOwnPropertySymbols(it) {
    return $GOPS.f(toObject(it));
  }
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    $replacer = replacer = args[1];
    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    if (!isArray(replacer)) replacer = function (key, value) {
      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__("35e8")($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),

/***/ "01f9":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("2d00");
var $export = __webpack_require__("5ca1");
var redefine = __webpack_require__("2aba");
var hide = __webpack_require__("32e9");
var Iterators = __webpack_require__("84f2");
var $iterCreate = __webpack_require__("41a0");
var setToStringTag = __webpack_require__("7f20");
var getPrototypeOf = __webpack_require__("38fd");
var ITERATOR = __webpack_require__("2b4c")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "02f4":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("4588");
var defined = __webpack_require__("be13");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "0390":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var at = __webpack_require__("02f4")(true);

 // `AdvanceStringIndex` abstract operation
// https://tc39.github.io/ecma262/#sec-advancestringindex
module.exports = function (S, index, unicode) {
  return index + (unicode ? at(S, index).length : 1);
};


/***/ }),

/***/ "0395":
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__("36c3");
var gOPN = __webpack_require__("6abf").f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),

/***/ "07e3":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "0bfb":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 21.2.5.3 get RegExp.prototype.flags
var anObject = __webpack_require__("cb7c");
module.exports = function () {
  var that = anObject(this);
  var result = '';
  if (that.global) result += 'g';
  if (that.ignoreCase) result += 'i';
  if (that.multiline) result += 'm';
  if (that.unicode) result += 'u';
  if (that.sticky) result += 'y';
  return result;
};


/***/ }),

/***/ "0d58":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("ce10");
var enumBugKeys = __webpack_require__("e11e");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "0d61":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CFilter_vue_vue_type_style_index_0_id_68772c0c_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("4623");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CFilter_vue_vue_type_style_index_0_id_68772c0c_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CFilter_vue_vue_type_style_index_0_id_68772c0c_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CFilter_vue_vue_type_style_index_0_id_68772c0c_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "0fc9":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("3a38");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "11e9":
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__("52a7");
var createDesc = __webpack_require__("4630");
var toIObject = __webpack_require__("6821");
var toPrimitive = __webpack_require__("6a99");
var has = __webpack_require__("69a8");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__("9e1e") ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),

/***/ "1222":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CCombobox_vue_vue_type_style_index_0_id_11f59c02_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("7163");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CCombobox_vue_vue_type_style_index_0_id_11f59c02_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CCombobox_vue_vue_type_style_index_0_id_11f59c02_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CCombobox_vue_vue_type_style_index_0_id_11f59c02_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "1495":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var anObject = __webpack_require__("cb7c");
var getKeys = __webpack_require__("0d58");

module.exports = __webpack_require__("9e1e") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "1654":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__("71c1")(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__("30f1")(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),

/***/ "1691":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "1928":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "1af6":
/***/ (function(module, exports, __webpack_require__) {

// 22.1.2.2 / 15.4.3.2 Array.isArray(arg)
var $export = __webpack_require__("63b6");

$export($export.S, 'Array', { isArray: __webpack_require__("9003") });


/***/ }),

/***/ "1bc3":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("f772");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "1c4c":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ctx = __webpack_require__("9b43");
var $export = __webpack_require__("5ca1");
var toObject = __webpack_require__("4bf8");
var call = __webpack_require__("1fa8");
var isArrayIter = __webpack_require__("33a4");
var toLength = __webpack_require__("9def");
var createProperty = __webpack_require__("f1ae");
var getIterFn = __webpack_require__("27ee");

$export($export.S + $export.F * !__webpack_require__("5cc5")(function (iter) { Array.from(iter); }), 'Array', {
  // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
  from: function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
    var O = toObject(arrayLike);
    var C = typeof this == 'function' ? this : Array;
    var aLen = arguments.length;
    var mapfn = aLen > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    var index = 0;
    var iterFn = getIterFn(O);
    var length, result, step, iterator;
    if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
    // if object isn't iterable or it's array with default iterator - use simple case
    if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
      for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
        createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
      }
    } else {
      length = toLength(O.length);
      for (result = new C(length); length > index; index++) {
        createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
      }
    }
    result.length = index;
    return result;
  }
});


/***/ }),

/***/ "1c76":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlay_vue_vue_type_style_index_0_id_7be65390_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("51c4");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlay_vue_vue_type_style_index_0_id_7be65390_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlay_vue_vue_type_style_index_0_id_7be65390_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlay_vue_vue_type_style_index_0_id_7be65390_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "1ec9":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("f772");
var document = __webpack_require__("e53d").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "1fa8":
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__("cb7c");
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),

/***/ "20fd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $defineProperty = __webpack_require__("d9f6");
var createDesc = __webpack_require__("aebd");

module.exports = function (object, index, value) {
  if (index in object) $defineProperty.f(object, index, createDesc(0, value));
  else object[index] = value;
};


/***/ }),

/***/ "214f":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

__webpack_require__("b0c5");
var redefine = __webpack_require__("2aba");
var hide = __webpack_require__("32e9");
var fails = __webpack_require__("79e5");
var defined = __webpack_require__("be13");
var wks = __webpack_require__("2b4c");
var regexpExec = __webpack_require__("520a");

var SPECIES = wks('species');

var REPLACE_SUPPORTS_NAMED_GROUPS = !fails(function () {
  // #replace needs built-in support for named groups.
  // #match works fine because it just return the exec results, even if it has
  // a "grops" property.
  var re = /./;
  re.exec = function () {
    var result = [];
    result.groups = { a: '7' };
    return result;
  };
  return ''.replace(re, '$<a>') !== '7';
});

var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = (function () {
  // Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
  var re = /(?:)/;
  var originalExec = re.exec;
  re.exec = function () { return originalExec.apply(this, arguments); };
  var result = 'ab'.split(re);
  return result.length === 2 && result[0] === 'a' && result[1] === 'b';
})();

module.exports = function (KEY, length, exec) {
  var SYMBOL = wks(KEY);

  var DELEGATES_TO_SYMBOL = !fails(function () {
    // String methods call symbol-named RegEp methods
    var O = {};
    O[SYMBOL] = function () { return 7; };
    return ''[KEY](O) != 7;
  });

  var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL ? !fails(function () {
    // Symbol-named RegExp methods call .exec
    var execCalled = false;
    var re = /a/;
    re.exec = function () { execCalled = true; return null; };
    if (KEY === 'split') {
      // RegExp[@@split] doesn't call the regex's exec method, but first creates
      // a new one. We need to return the patched regex when creating the new one.
      re.constructor = {};
      re.constructor[SPECIES] = function () { return re; };
    }
    re[SYMBOL]('');
    return !execCalled;
  }) : undefined;

  if (
    !DELEGATES_TO_SYMBOL ||
    !DELEGATES_TO_EXEC ||
    (KEY === 'replace' && !REPLACE_SUPPORTS_NAMED_GROUPS) ||
    (KEY === 'split' && !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC)
  ) {
    var nativeRegExpMethod = /./[SYMBOL];
    var fns = exec(
      defined,
      SYMBOL,
      ''[KEY],
      function maybeCallNative(nativeMethod, regexp, str, arg2, forceStringMethod) {
        if (regexp.exec === regexpExec) {
          if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
            // The native String method already delegates to @@method (this
            // polyfilled function), leasing to infinite recursion.
            // We avoid it by directly calling the native @@method method.
            return { done: true, value: nativeRegExpMethod.call(regexp, str, arg2) };
          }
          return { done: true, value: nativeMethod.call(str, regexp, arg2) };
        }
        return { done: false };
      }
    );
    var strfn = fns[0];
    var rxfn = fns[1];

    redefine(String.prototype, KEY, strfn);
    hide(RegExp.prototype, SYMBOL, length == 2
      // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
      // 21.2.5.11 RegExp.prototype[@@split](string, limit)
      ? function (string, arg) { return rxfn.call(string, this, arg); }
      // 21.2.5.6 RegExp.prototype[@@match](string)
      // 21.2.5.9 RegExp.prototype[@@search](string)
      : function (string) { return rxfn.call(string, this); }
    );
  }
};


/***/ }),

/***/ "230e":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var document = __webpack_require__("7726").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "23c6":
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__("2d95");
var TAG = __webpack_require__("2b4c")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "241e":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__("25eb");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "25eb":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "2621":
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),

/***/ "27ee":
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__("23c6");
var ITERATOR = __webpack_require__("2b4c")('iterator');
var Iterators = __webpack_require__("84f2");
module.exports = __webpack_require__("8378").getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),

/***/ "28a5":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isRegExp = __webpack_require__("aae3");
var anObject = __webpack_require__("cb7c");
var speciesConstructor = __webpack_require__("ebd6");
var advanceStringIndex = __webpack_require__("0390");
var toLength = __webpack_require__("9def");
var callRegExpExec = __webpack_require__("5f1b");
var regexpExec = __webpack_require__("520a");
var fails = __webpack_require__("79e5");
var $min = Math.min;
var $push = [].push;
var $SPLIT = 'split';
var LENGTH = 'length';
var LAST_INDEX = 'lastIndex';
var MAX_UINT32 = 0xffffffff;

// babel-minify transpiles RegExp('x', 'y') -> /x/y and it causes SyntaxError
var SUPPORTS_Y = !fails(function () { RegExp(MAX_UINT32, 'y'); });

// @@split logic
__webpack_require__("214f")('split', 2, function (defined, SPLIT, $split, maybeCallNative) {
  var internalSplit;
  if (
    'abbc'[$SPLIT](/(b)*/)[1] == 'c' ||
    'test'[$SPLIT](/(?:)/, -1)[LENGTH] != 4 ||
    'ab'[$SPLIT](/(?:ab)*/)[LENGTH] != 2 ||
    '.'[$SPLIT](/(.?)(.?)/)[LENGTH] != 4 ||
    '.'[$SPLIT](/()()/)[LENGTH] > 1 ||
    ''[$SPLIT](/.?/)[LENGTH]
  ) {
    // based on es5-shim implementation, need to rework it
    internalSplit = function (separator, limit) {
      var string = String(this);
      if (separator === undefined && limit === 0) return [];
      // If `separator` is not a regex, use native split
      if (!isRegExp(separator)) return $split.call(string, separator, limit);
      var output = [];
      var flags = (separator.ignoreCase ? 'i' : '') +
                  (separator.multiline ? 'm' : '') +
                  (separator.unicode ? 'u' : '') +
                  (separator.sticky ? 'y' : '');
      var lastLastIndex = 0;
      var splitLimit = limit === undefined ? MAX_UINT32 : limit >>> 0;
      // Make `global` and avoid `lastIndex` issues by working with a copy
      var separatorCopy = new RegExp(separator.source, flags + 'g');
      var match, lastIndex, lastLength;
      while (match = regexpExec.call(separatorCopy, string)) {
        lastIndex = separatorCopy[LAST_INDEX];
        if (lastIndex > lastLastIndex) {
          output.push(string.slice(lastLastIndex, match.index));
          if (match[LENGTH] > 1 && match.index < string[LENGTH]) $push.apply(output, match.slice(1));
          lastLength = match[0][LENGTH];
          lastLastIndex = lastIndex;
          if (output[LENGTH] >= splitLimit) break;
        }
        if (separatorCopy[LAST_INDEX] === match.index) separatorCopy[LAST_INDEX]++; // Avoid an infinite loop
      }
      if (lastLastIndex === string[LENGTH]) {
        if (lastLength || !separatorCopy.test('')) output.push('');
      } else output.push(string.slice(lastLastIndex));
      return output[LENGTH] > splitLimit ? output.slice(0, splitLimit) : output;
    };
  // Chakra, V8
  } else if ('0'[$SPLIT](undefined, 0)[LENGTH]) {
    internalSplit = function (separator, limit) {
      return separator === undefined && limit === 0 ? [] : $split.call(this, separator, limit);
    };
  } else {
    internalSplit = $split;
  }

  return [
    // `String.prototype.split` method
    // https://tc39.github.io/ecma262/#sec-string.prototype.split
    function split(separator, limit) {
      var O = defined(this);
      var splitter = separator == undefined ? undefined : separator[SPLIT];
      return splitter !== undefined
        ? splitter.call(separator, O, limit)
        : internalSplit.call(String(O), separator, limit);
    },
    // `RegExp.prototype[@@split]` method
    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@split
    //
    // NOTE: This cannot be properly polyfilled in engines that don't support
    // the 'y' flag.
    function (regexp, limit) {
      var res = maybeCallNative(internalSplit, regexp, this, limit, internalSplit !== $split);
      if (res.done) return res.value;

      var rx = anObject(regexp);
      var S = String(this);
      var C = speciesConstructor(rx, RegExp);

      var unicodeMatching = rx.unicode;
      var flags = (rx.ignoreCase ? 'i' : '') +
                  (rx.multiline ? 'm' : '') +
                  (rx.unicode ? 'u' : '') +
                  (SUPPORTS_Y ? 'y' : 'g');

      // ^(? + rx + ) is needed, in combination with some S slicing, to
      // simulate the 'y' flag.
      var splitter = new C(SUPPORTS_Y ? rx : '^(?:' + rx.source + ')', flags);
      var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
      if (lim === 0) return [];
      if (S.length === 0) return callRegExpExec(splitter, S) === null ? [S] : [];
      var p = 0;
      var q = 0;
      var A = [];
      while (q < S.length) {
        splitter.lastIndex = SUPPORTS_Y ? q : 0;
        var z = callRegExpExec(splitter, SUPPORTS_Y ? S : S.slice(q));
        var e;
        if (
          z === null ||
          (e = $min(toLength(splitter.lastIndex + (SUPPORTS_Y ? 0 : q)), S.length)) === p
        ) {
          q = advanceStringIndex(S, q, unicodeMatching);
        } else {
          A.push(S.slice(p, q));
          if (A.length === lim) return A;
          for (var i = 1; i <= z.length - 1; i++) {
            A.push(z[i]);
            if (A.length === lim) return A;
          }
          q = p = e;
        }
      }
      A.push(S.slice(p));
      return A;
    }
  ];
});


/***/ }),

/***/ "294c":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "2aba":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var hide = __webpack_require__("32e9");
var has = __webpack_require__("69a8");
var SRC = __webpack_require__("ca5a")('src');
var $toString = __webpack_require__("fa5b");
var TO_STRING = 'toString';
var TPL = ('' + $toString).split(TO_STRING);

__webpack_require__("8378").inspectSource = function (it) {
  return $toString.call(it);
};

(module.exports = function (O, key, val, safe) {
  var isFunction = typeof val == 'function';
  if (isFunction) has(val, 'name') || hide(val, 'name', key);
  if (O[key] === val) return;
  if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
  if (O === global) {
    O[key] = val;
  } else if (!safe) {
    delete O[key];
    hide(O, key, val);
  } else if (O[key]) {
    O[key] = val;
  } else {
    hide(O, key, val);
  }
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, TO_STRING, function toString() {
  return typeof this == 'function' && this[SRC] || $toString.call(this);
});


/***/ }),

/***/ "2aeb":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("cb7c");
var dPs = __webpack_require__("1495");
var enumBugKeys = __webpack_require__("e11e");
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("230e")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("fab2").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "2b4c":
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__("5537")('wks');
var uid = __webpack_require__("ca5a");
var Symbol = __webpack_require__("7726").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "2bdc":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("fb6c");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "2d00":
/***/ (function(module, exports) {

module.exports = false;


/***/ }),

/***/ "2d95":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "2f21":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var fails = __webpack_require__("79e5");

module.exports = function (method, arg) {
  return !!method && fails(function () {
    // eslint-disable-next-line no-useless-call
    arg ? method.call(null, function () { /* empty */ }, 1) : method.call(null);
  });
};


/***/ }),

/***/ "2fdb":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// 21.1.3.7 String.prototype.includes(searchString, position = 0)

var $export = __webpack_require__("5ca1");
var context = __webpack_require__("d2c8");
var INCLUDES = 'includes';

$export($export.P + $export.F * __webpack_require__("5147")(INCLUDES), 'String', {
  includes: function includes(searchString /* , position = 0 */) {
    return !!~context(this, searchString, INCLUDES)
      .indexOf(searchString, arguments.length > 1 ? arguments[1] : undefined);
  }
});


/***/ }),

/***/ "30f1":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("b8e3");
var $export = __webpack_require__("63b6");
var redefine = __webpack_require__("9138");
var hide = __webpack_require__("35e8");
var Iterators = __webpack_require__("481b");
var $iterCreate = __webpack_require__("8f60");
var setToStringTag = __webpack_require__("45f2");
var getPrototypeOf = __webpack_require__("53e2");
var ITERATOR = __webpack_require__("5168")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "32a6":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__("241e");
var $keys = __webpack_require__("c3a1");

__webpack_require__("ce7e")('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),

/***/ "32e9":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var createDesc = __webpack_require__("4630");
module.exports = __webpack_require__("9e1e") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "32fc":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("e53d").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "335c":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("6b4c");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "33a4":
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__("84f2");
var ITERATOR = __webpack_require__("2b4c")('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),

/***/ "355d":
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "35e8":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("d9f6");
var createDesc = __webpack_require__("aebd");
module.exports = __webpack_require__("8e60") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "36c3":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("335c");
var defined = __webpack_require__("25eb");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "3702":
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__("481b");
var ITERATOR = __webpack_require__("5168")('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),

/***/ "38fd":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__("69a8");
var toObject = __webpack_require__("4bf8");
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "3a38":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "3a7f":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "40c3":
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__("6b4c");
var TAG = __webpack_require__("5168")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "419d":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CCard_vue_vue_type_style_index_0_id_1a43f82e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("478f");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CCard_vue_vue_type_style_index_0_id_1a43f82e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CCard_vue_vue_type_style_index_0_id_1a43f82e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CCard_vue_vue_type_style_index_0_id_1a43f82e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "41a0":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__("2aeb");
var descriptor = __webpack_require__("4630");
var setToStringTag = __webpack_require__("7f20");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__("32e9")(IteratorPrototype, __webpack_require__("2b4c")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "454f":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("46a7");
var $Object = __webpack_require__("584a").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "456d":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__("4bf8");
var $keys = __webpack_require__("0d58");

__webpack_require__("5eda")('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),

/***/ "4588":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "45f2":
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__("d9f6").f;
var has = __webpack_require__("07e3");
var TAG = __webpack_require__("5168")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "4623":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "4630":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "46a7":
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__("63b6");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__("8e60"), 'Object', { defineProperty: __webpack_require__("d9f6").f });


/***/ }),

/***/ "4730":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CToolbar_vue_vue_type_style_index_0_id_54c78777_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cd66");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CToolbar_vue_vue_type_style_index_0_id_54c78777_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CToolbar_vue_vue_type_style_index_0_id_54c78777_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CToolbar_vue_vue_type_style_index_0_id_54c78777_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "478f":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "47ee":
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__("c3a1");
var gOPS = __webpack_require__("9aa9");
var pIE = __webpack_require__("355d");
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),

/***/ "481b":
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ "4917":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var anObject = __webpack_require__("cb7c");
var toLength = __webpack_require__("9def");
var advanceStringIndex = __webpack_require__("0390");
var regExpExec = __webpack_require__("5f1b");

// @@match logic
__webpack_require__("214f")('match', 1, function (defined, MATCH, $match, maybeCallNative) {
  return [
    // `String.prototype.match` method
    // https://tc39.github.io/ecma262/#sec-string.prototype.match
    function match(regexp) {
      var O = defined(this);
      var fn = regexp == undefined ? undefined : regexp[MATCH];
      return fn !== undefined ? fn.call(regexp, O) : new RegExp(regexp)[MATCH](String(O));
    },
    // `RegExp.prototype[@@match]` method
    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@match
    function (regexp) {
      var res = maybeCallNative($match, regexp, this);
      if (res.done) return res.value;
      var rx = anObject(regexp);
      var S = String(this);
      if (!rx.global) return regExpExec(rx, S);
      var fullUnicode = rx.unicode;
      rx.lastIndex = 0;
      var A = [];
      var n = 0;
      var result;
      while ((result = regExpExec(rx, S)) !== null) {
        var matchStr = String(result[0]);
        A[n] = matchStr;
        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
        n++;
      }
      return n === 0 ? null : A;
    }
  ];
});


/***/ }),

/***/ "4972":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CMessage_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("6d51");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CMessage_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CMessage_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CMessage_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "4a59":
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__("9b43");
var call = __webpack_require__("1fa8");
var isArrayIter = __webpack_require__("33a4");
var anObject = __webpack_require__("cb7c");
var toLength = __webpack_require__("9def");
var getIterFn = __webpack_require__("27ee");
var BREAK = {};
var RETURN = {};
var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
  var iterFn = ITERATOR ? function () { return iterable; } : getIterFn(iterable);
  var f = ctx(fn, that, entries ? 2 : 1);
  var index = 0;
  var length, step, iterator, result;
  if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!');
  // fast case for arrays with default iterator
  if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
    result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
    if (result === BREAK || result === RETURN) return result;
  } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
    result = call(iterator, f, step.value, entries);
    if (result === BREAK || result === RETURN) return result;
  }
};
exports.BREAK = BREAK;
exports.RETURN = RETURN;


/***/ }),

/***/ "4bf8":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__("be13");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "4d5a":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Viewer_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("e601");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Viewer_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Viewer_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Viewer_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "4ee1":
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__("5168")('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),

/***/ "4f7f":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var strong = __webpack_require__("c26b");
var validate = __webpack_require__("b39a");
var SET = 'Set';

// 23.2 Set Objects
module.exports = __webpack_require__("e0b8")(SET, function (get) {
  return function Set() { return get(this, arguments.length > 0 ? arguments[0] : undefined); };
}, {
  // 23.2.3.1 Set.prototype.add(value)
  add: function add(value) {
    return strong.def(validate(this, SET), value = value === 0 ? 0 : value, value);
  }
}, strong);


/***/ }),

/***/ "50ed":
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "5147":
/***/ (function(module, exports, __webpack_require__) {

var MATCH = __webpack_require__("2b4c")('match');
module.exports = function (KEY) {
  var re = /./;
  try {
    '/./'[KEY](re);
  } catch (e) {
    try {
      re[MATCH] = false;
      return !'/./'[KEY](re);
    } catch (f) { /* empty */ }
  } return true;
};


/***/ }),

/***/ "5168":
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__("dbdb")('wks');
var uid = __webpack_require__("62a0");
var Symbol = __webpack_require__("e53d").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "51c4":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "520a":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var regexpFlags = __webpack_require__("0bfb");

var nativeExec = RegExp.prototype.exec;
// This always refers to the native implementation, because the
// String#replace polyfill uses ./fix-regexp-well-known-symbol-logic.js,
// which loads this file before patching the method.
var nativeReplace = String.prototype.replace;

var patchedExec = nativeExec;

var LAST_INDEX = 'lastIndex';

var UPDATES_LAST_INDEX_WRONG = (function () {
  var re1 = /a/,
      re2 = /b*/g;
  nativeExec.call(re1, 'a');
  nativeExec.call(re2, 'a');
  return re1[LAST_INDEX] !== 0 || re2[LAST_INDEX] !== 0;
})();

// nonparticipating capturing group, copied from es5-shim's String#split patch.
var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;

var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED;

if (PATCH) {
  patchedExec = function exec(str) {
    var re = this;
    var lastIndex, reCopy, match, i;

    if (NPCG_INCLUDED) {
      reCopy = new RegExp('^' + re.source + '$(?!\\s)', regexpFlags.call(re));
    }
    if (UPDATES_LAST_INDEX_WRONG) lastIndex = re[LAST_INDEX];

    match = nativeExec.call(re, str);

    if (UPDATES_LAST_INDEX_WRONG && match) {
      re[LAST_INDEX] = re.global ? match.index + match[0].length : lastIndex;
    }
    if (NPCG_INCLUDED && match && match.length > 1) {
      // Fix browsers whose `exec` methods don't consistently return `undefined`
      // for NPCG, like IE8. NOTE: This doesn' work for /(.?)?/
      // eslint-disable-next-line no-loop-func
      nativeReplace.call(match[0], reCopy, function () {
        for (i = 1; i < arguments.length - 2; i++) {
          if (arguments[i] === undefined) match[i] = undefined;
        }
      });
    }

    return match;
  };
}

module.exports = patchedExec;


/***/ }),

/***/ "52a7":
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "53c2":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "53e2":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__("07e3");
var toObject = __webpack_require__("241e");
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "549b":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ctx = __webpack_require__("d864");
var $export = __webpack_require__("63b6");
var toObject = __webpack_require__("241e");
var call = __webpack_require__("b0dc");
var isArrayIter = __webpack_require__("3702");
var toLength = __webpack_require__("b447");
var createProperty = __webpack_require__("20fd");
var getIterFn = __webpack_require__("7cd6");

$export($export.S + $export.F * !__webpack_require__("4ee1")(function (iter) { Array.from(iter); }), 'Array', {
  // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
  from: function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
    var O = toObject(arrayLike);
    var C = typeof this == 'function' ? this : Array;
    var aLen = arguments.length;
    var mapfn = aLen > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    var index = 0;
    var iterFn = getIterFn(O);
    var length, result, step, iterator;
    if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
    // if object isn't iterable or it's array with default iterator - use simple case
    if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
      for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
        createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
      }
    } else {
      length = toLength(O.length);
      for (result = new C(length); length > index; index++) {
        createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
      }
    }
    result.length = index;
    return result;
  }
});


/***/ }),

/***/ "54a1":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6c1c");
__webpack_require__("1654");
module.exports = __webpack_require__("95d5");


/***/ }),

/***/ "5537":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("8378");
var global = __webpack_require__("7726");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__("2d00") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "5559":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("dbdb")('keys');
var uid = __webpack_require__("62a0");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "55dd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $export = __webpack_require__("5ca1");
var aFunction = __webpack_require__("d8e8");
var toObject = __webpack_require__("4bf8");
var fails = __webpack_require__("79e5");
var $sort = [].sort;
var test = [1, 2, 3];

$export($export.P + $export.F * (fails(function () {
  // IE8-
  test.sort(undefined);
}) || !fails(function () {
  // V8 bug
  test.sort(null);
  // Old WebKit
}) || !__webpack_require__("2f21")($sort)), 'Array', {
  // 22.1.3.25 Array.prototype.sort(comparefn)
  sort: function sort(comparefn) {
    return comparefn === undefined
      ? $sort.call(toObject(this))
      : $sort.call(toObject(this), aFunction(comparefn));
  }
});


/***/ }),

/***/ "584a":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "5b4e":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("36c3");
var toLength = __webpack_require__("b447");
var toAbsoluteIndex = __webpack_require__("0fc9");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "5b5a":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CChip_vue_vue_type_style_index_0_id_4ac2cfc0_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("53c2");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CChip_vue_vue_type_style_index_0_id_4ac2cfc0_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CChip_vue_vue_type_style_index_0_id_4ac2cfc0_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CChip_vue_vue_type_style_index_0_id_4ac2cfc0_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "5ca1":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var core = __webpack_require__("8378");
var hide = __webpack_require__("32e9");
var redefine = __webpack_require__("2aba");
var ctx = __webpack_require__("9b43");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
  var key, own, out, exp;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    // export native or passed
    out = (own ? target : source)[key];
    // bind timers to global for call from export context
    exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // extend global
    if (target) redefine(target, key, out, type & $export.U);
    // export
    if (exports[key] != out) hide(exports, key, exp);
    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
  }
};
global.core = core;
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "5cc5":
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__("2b4c")('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),

/***/ "5d58":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("d8d6");

/***/ }),

/***/ "5dbc":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var setPrototypeOf = __webpack_require__("8b97").set;
module.exports = function (that, target, C) {
  var S = target.constructor;
  var P;
  if (S !== C && typeof S == 'function' && (P = S.prototype) !== C.prototype && isObject(P) && setPrototypeOf) {
    setPrototypeOf(that, P);
  } return that;
};


/***/ }),

/***/ "5df3":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__("02f4")(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__("01f9")(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),

/***/ "5eda":
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__("5ca1");
var core = __webpack_require__("8378");
var fails = __webpack_require__("79e5");
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),

/***/ "5f1b":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var classof = __webpack_require__("23c6");
var builtinExec = RegExp.prototype.exec;

 // `RegExpExec` abstract operation
// https://tc39.github.io/ecma262/#sec-regexpexec
module.exports = function (R, S) {
  var exec = R.exec;
  if (typeof exec === 'function') {
    var result = exec.call(R, S);
    if (typeof result !== 'object') {
      throw new TypeError('RegExp exec method returned something other than an Object or null');
    }
    return result;
  }
  if (classof(R) !== 'RegExp') {
    throw new TypeError('RegExp#exec called on incompatible receiver');
  }
  return builtinExec.call(R, S);
};


/***/ }),

/***/ "613b":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("5537")('keys');
var uid = __webpack_require__("ca5a");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "626a":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("2d95");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "62a0":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "63b6":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("e53d");
var core = __webpack_require__("584a");
var ctx = __webpack_require__("d864");
var hide = __webpack_require__("35e8");
var has = __webpack_require__("07e3");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "6706":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "6718":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("e53d");
var core = __webpack_require__("584a");
var LIBRARY = __webpack_require__("b8e3");
var wksExt = __webpack_require__("ccb9");
var defineProperty = __webpack_require__("d9f6").f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),

/***/ "6762":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// https://github.com/tc39/Array.prototype.includes
var $export = __webpack_require__("5ca1");
var $includes = __webpack_require__("c366")(true);

$export($export.P, 'Array', {
  includes: function includes(el /* , fromIndex = 0 */) {
    return $includes(this, el, arguments.length > 1 ? arguments[1] : undefined);
  }
});

__webpack_require__("9c6c")('includes');


/***/ }),

/***/ "67ab":
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__("ca5a")('meta');
var isObject = __webpack_require__("d3f4");
var has = __webpack_require__("69a8");
var setDesc = __webpack_require__("86cc").f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__("79e5")(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),

/***/ "67bb":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("f921");

/***/ }),

/***/ "6821":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("626a");
var defined = __webpack_require__("be13");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "6859":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("6706");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Box_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "69a8":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "69d3":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6718")('asyncIterator');


/***/ }),

/***/ "6a99":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("d3f4");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "6abf":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__("e6f3");
var hiddenKeys = __webpack_require__("1691").concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),

/***/ "6b4c":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "6c1c":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("c367");
var global = __webpack_require__("e53d");
var hide = __webpack_require__("35e8");
var Iterators = __webpack_require__("481b");
var TO_STRING_TAG = __webpack_require__("5168")('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),

/***/ "6d51":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "6df2":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "6e83":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CSuggestion_vue_vue_type_style_index_0_id_2a39c370_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("f43b");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CSuggestion_vue_vue_type_style_index_0_id_2a39c370_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CSuggestion_vue_vue_type_style_index_0_id_2a39c370_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CSuggestion_vue_vue_type_style_index_0_id_2a39c370_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "70f0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_List_vue_vue_type_style_index_0_id_57a3a2bb_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("9842");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_List_vue_vue_type_style_index_0_id_57a3a2bb_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_List_vue_vue_type_style_index_0_id_57a3a2bb_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_List_vue_vue_type_style_index_0_id_57a3a2bb_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "7163":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "71c1":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("3a38");
var defined = __webpack_require__("25eb");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "7333":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var DESCRIPTORS = __webpack_require__("9e1e");
var getKeys = __webpack_require__("0d58");
var gOPS = __webpack_require__("2621");
var pIE = __webpack_require__("52a7");
var toObject = __webpack_require__("4bf8");
var IObject = __webpack_require__("626a");
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__("79e5")(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) {
      key = keys[j++];
      if (!DESCRIPTORS || isEnum.call(S, key)) T[key] = S[key];
    }
  } return T;
} : $assign;


/***/ }),

/***/ "765d":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6718")('observable');


/***/ }),

/***/ "7726":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "774e":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("d2d5");

/***/ }),

/***/ "77f1":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("4588");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "794b":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("8e60") && !__webpack_require__("294c")(function () {
  return Object.defineProperty(__webpack_require__("1ec9")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "79aa":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "79e5":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "7a56":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__("7726");
var dP = __webpack_require__("86cc");
var DESCRIPTORS = __webpack_require__("9e1e");
var SPECIES = __webpack_require__("2b4c")('species');

module.exports = function (KEY) {
  var C = global[KEY];
  if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
    configurable: true,
    get: function () { return this; }
  });
};


/***/ }),

/***/ "7cd6":
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__("40c3");
var ITERATOR = __webpack_require__("5168")('iterator');
var Iterators = __webpack_require__("481b");
module.exports = __webpack_require__("584a").getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),

/***/ "7e90":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("d9f6");
var anObject = __webpack_require__("e4ae");
var getKeys = __webpack_require__("c3a1");

module.exports = __webpack_require__("8e60") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "7f20":
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__("86cc").f;
var has = __webpack_require__("69a8");
var TAG = __webpack_require__("2b4c")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "7f7f":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc").f;
var FProto = Function.prototype;
var nameRE = /^\s*function ([^ (]*)/;
var NAME = 'name';

// 19.2.4.2 name
NAME in FProto || __webpack_require__("9e1e") && dP(FProto, NAME, {
  configurable: true,
  get: function () {
    try {
      return ('' + this).match(nameRE)[1];
    } catch (e) {
      return '';
    }
  }
});


/***/ }),

/***/ "8378":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "8436":
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),

/***/ "84f2":
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ "85f2":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("454f");

/***/ }),

/***/ "86cc":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("cb7c");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var toPrimitive = __webpack_require__("6a99");
var dP = Object.defineProperty;

exports.f = __webpack_require__("9e1e") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "8a04":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "8aae":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("32a6");
module.exports = __webpack_require__("584a").Object.keys;


/***/ }),

/***/ "8b90":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CGroupname_vue_vue_type_style_index_0_id_3c87ecea_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("e2c0");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CGroupname_vue_vue_type_style_index_0_id_3c87ecea_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CGroupname_vue_vue_type_style_index_0_id_3c87ecea_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CGroupname_vue_vue_type_style_index_0_id_3c87ecea_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "8b97":
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__("d3f4");
var anObject = __webpack_require__("cb7c");
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__("9b43")(Function.call, __webpack_require__("11e9").f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),

/***/ "8ccc":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "8e60":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("294c")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "8e6e":
/***/ (function(module, exports, __webpack_require__) {

// https://github.com/tc39/proposal-object-getownpropertydescriptors
var $export = __webpack_require__("5ca1");
var ownKeys = __webpack_require__("990b");
var toIObject = __webpack_require__("6821");
var gOPD = __webpack_require__("11e9");
var createProperty = __webpack_require__("f1ae");

$export($export.S, 'Object', {
  getOwnPropertyDescriptors: function getOwnPropertyDescriptors(object) {
    var O = toIObject(object);
    var getDesc = gOPD.f;
    var keys = ownKeys(O);
    var result = {};
    var i = 0;
    var key, desc;
    while (keys.length > i) {
      desc = getDesc(O, key = keys[i++]);
      if (desc !== undefined) createProperty(result, key, desc);
    }
    return result;
  }
});


/***/ }),

/***/ "8f60":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__("a159");
var descriptor = __webpack_require__("aebd");
var setToStringTag = __webpack_require__("45f2");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__("35e8")(IteratorPrototype, __webpack_require__("5168")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "9003":
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__("6b4c");
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),

/***/ "9093":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__("ce10");
var hiddenKeys = __webpack_require__("e11e").concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),

/***/ "9138":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("35e8");


/***/ }),

/***/ "94a1":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Selector_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("1928");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Selector_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Selector_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Selector_vue_vue_type_style_index_0_lang_sass___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "95d5":
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__("40c3");
var ITERATOR = __webpack_require__("5168")('iterator');
var Iterators = __webpack_require__("481b");
module.exports = __webpack_require__("584a").isIterable = function (it) {
  var O = Object(it);
  return O[ITERATOR] !== undefined
    || '@@iterator' in O
    // eslint-disable-next-line no-prototype-builtins
    || Iterators.hasOwnProperty(classof(O));
};


/***/ }),

/***/ "9842":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "990b":
/***/ (function(module, exports, __webpack_require__) {

// all object keys, includes non-enumerable and symbols
var gOPN = __webpack_require__("9093");
var gOPS = __webpack_require__("2621");
var anObject = __webpack_require__("cb7c");
var Reflect = __webpack_require__("7726").Reflect;
module.exports = Reflect && Reflect.ownKeys || function ownKeys(it) {
  var keys = gOPN.f(anObject(it));
  var getSymbols = gOPS.f;
  return getSymbols ? keys.concat(getSymbols(it)) : keys;
};


/***/ }),

/***/ "9aa9":
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),

/***/ "9b43":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("d8e8");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "9c6c":
/***/ (function(module, exports, __webpack_require__) {

// 22.1.3.31 Array.prototype[@@unscopables]
var UNSCOPABLES = __webpack_require__("2b4c")('unscopables');
var ArrayProto = Array.prototype;
if (ArrayProto[UNSCOPABLES] == undefined) __webpack_require__("32e9")(ArrayProto, UNSCOPABLES, {});
module.exports = function (key) {
  ArrayProto[UNSCOPABLES][key] = true;
};


/***/ }),

/***/ "9def":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("4588");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "9e1e":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("79e5")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "9e57":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "9faa":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CAutocomplete_vue_vue_type_style_index_0_id_28ae3ced_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("9e57");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CAutocomplete_vue_vue_type_style_index_0_id_28ae3ced_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CAutocomplete_vue_vue_type_style_index_0_id_28ae3ced_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CAutocomplete_vue_vue_type_style_index_0_id_28ae3ced_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "a159":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("e4ae");
var dPs = __webpack_require__("7e90");
var enumBugKeys = __webpack_require__("1691");
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("1ec9")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("32fc").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "a4bb":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("8aae");

/***/ }),

/***/ "a5f9":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CDialog_vue_vue_type_style_index_0_id_0ddf918e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("6df2");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CDialog_vue_vue_type_style_index_0_id_0ddf918e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CDialog_vue_vue_type_style_index_0_id_0ddf918e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CDialog_vue_vue_type_style_index_0_id_0ddf918e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "a745":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("f410");

/***/ }),

/***/ "aa77":
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__("5ca1");
var defined = __webpack_require__("be13");
var fails = __webpack_require__("79e5");
var spaces = __webpack_require__("fdef");
var space = '[' + spaces + ']';
var non = '\u200b\u0085';
var ltrim = RegExp('^' + space + space + '*');
var rtrim = RegExp(space + space + '*$');

var exporter = function (KEY, exec, ALIAS) {
  var exp = {};
  var FORCE = fails(function () {
    return !!spaces[KEY]() || non[KEY]() != non;
  });
  var fn = exp[KEY] = FORCE ? exec(trim) : spaces[KEY];
  if (ALIAS) exp[ALIAS] = fn;
  $export($export.P + $export.F * FORCE, 'String', exp);
};

// 1 -> String#trimLeft
// 2 -> String#trimRight
// 3 -> String#trim
var trim = exporter.trim = function (string, TYPE) {
  string = String(defined(string));
  if (TYPE & 1) string = string.replace(ltrim, '');
  if (TYPE & 2) string = string.replace(rtrim, '');
  return string;
};

module.exports = exporter;


/***/ }),

/***/ "aae3":
/***/ (function(module, exports, __webpack_require__) {

// 7.2.8 IsRegExp(argument)
var isObject = __webpack_require__("d3f4");
var cof = __webpack_require__("2d95");
var MATCH = __webpack_require__("2b4c")('match');
module.exports = function (it) {
  var isRegExp;
  return isObject(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : cof(it) == 'RegExp');
};


/***/ }),

/***/ "ac6a":
/***/ (function(module, exports, __webpack_require__) {

var $iterators = __webpack_require__("cadf");
var getKeys = __webpack_require__("0d58");
var redefine = __webpack_require__("2aba");
var global = __webpack_require__("7726");
var hide = __webpack_require__("32e9");
var Iterators = __webpack_require__("84f2");
var wks = __webpack_require__("2b4c");
var ITERATOR = wks('iterator');
var TO_STRING_TAG = wks('toStringTag');
var ArrayValues = Iterators.Array;

var DOMIterables = {
  CSSRuleList: true, // TODO: Not spec compliant, should be false.
  CSSStyleDeclaration: false,
  CSSValueList: false,
  ClientRectList: false,
  DOMRectList: false,
  DOMStringList: false,
  DOMTokenList: true,
  DataTransferItemList: false,
  FileList: false,
  HTMLAllCollection: false,
  HTMLCollection: false,
  HTMLFormElement: false,
  HTMLSelectElement: false,
  MediaList: true, // TODO: Not spec compliant, should be false.
  MimeTypeArray: false,
  NamedNodeMap: false,
  NodeList: true,
  PaintRequestList: false,
  Plugin: false,
  PluginArray: false,
  SVGLengthList: false,
  SVGNumberList: false,
  SVGPathSegList: false,
  SVGPointList: false,
  SVGStringList: false,
  SVGTransformList: false,
  SourceBufferList: false,
  StyleSheetList: true, // TODO: Not spec compliant, should be false.
  TextTrackCueList: false,
  TextTrackList: false,
  TouchList: false
};

for (var collections = getKeys(DOMIterables), i = 0; i < collections.length; i++) {
  var NAME = collections[i];
  var explicit = DOMIterables[NAME];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  var key;
  if (proto) {
    if (!proto[ITERATOR]) hide(proto, ITERATOR, ArrayValues);
    if (!proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
    Iterators[NAME] = ArrayValues;
    if (explicit) for (key in $iterators) if (!proto[key]) redefine(proto, key, $iterators[key], true);
  }
}


/***/ }),

/***/ "aebd":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "b0c5":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var regexpExec = __webpack_require__("520a");
__webpack_require__("5ca1")({
  target: 'RegExp',
  proto: true,
  forced: regexpExec !== /./.exec
}, {
  exec: regexpExec
});


/***/ }),

/***/ "b0dc":
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__("e4ae");
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),

/***/ "b39a":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
module.exports = function (it, TYPE) {
  if (!isObject(it) || it._t !== TYPE) throw TypeError('Incompatible receiver, ' + TYPE + ' required!');
  return it;
};


/***/ }),

/***/ "b447":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("3a38");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "b8e3":
/***/ (function(module, exports) {

module.exports = true;


/***/ }),

/***/ "bb56":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CIcon_vue_vue_type_style_index_0_id_02ec376f_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3a7f");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CIcon_vue_vue_type_style_index_0_id_02ec376f_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CIcon_vue_vue_type_style_index_0_id_02ec376f_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CIcon_vue_vue_type_style_index_0_id_02ec376f_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "bda0":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "be13":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "bf0b":
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__("355d");
var createDesc = __webpack_require__("aebd");
var toIObject = __webpack_require__("36c3");
var toPrimitive = __webpack_require__("1bc3");
var has = __webpack_require__("07e3");
var IE8_DOM_DEFINE = __webpack_require__("794b");
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__("8e60") ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),

/***/ "c207":
/***/ (function(module, exports) {



/***/ }),

/***/ "c26b":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var dP = __webpack_require__("86cc").f;
var create = __webpack_require__("2aeb");
var redefineAll = __webpack_require__("dcbc");
var ctx = __webpack_require__("9b43");
var anInstance = __webpack_require__("f605");
var forOf = __webpack_require__("4a59");
var $iterDefine = __webpack_require__("01f9");
var step = __webpack_require__("d53b");
var setSpecies = __webpack_require__("7a56");
var DESCRIPTORS = __webpack_require__("9e1e");
var fastKey = __webpack_require__("67ab").fastKey;
var validate = __webpack_require__("b39a");
var SIZE = DESCRIPTORS ? '_s' : 'size';

var getEntry = function (that, key) {
  // fast case
  var index = fastKey(key);
  var entry;
  if (index !== 'F') return that._i[index];
  // frozen object case
  for (entry = that._f; entry; entry = entry.n) {
    if (entry.k == key) return entry;
  }
};

module.exports = {
  getConstructor: function (wrapper, NAME, IS_MAP, ADDER) {
    var C = wrapper(function (that, iterable) {
      anInstance(that, C, NAME, '_i');
      that._t = NAME;         // collection type
      that._i = create(null); // index
      that._f = undefined;    // first entry
      that._l = undefined;    // last entry
      that[SIZE] = 0;         // size
      if (iterable != undefined) forOf(iterable, IS_MAP, that[ADDER], that);
    });
    redefineAll(C.prototype, {
      // 23.1.3.1 Map.prototype.clear()
      // 23.2.3.2 Set.prototype.clear()
      clear: function clear() {
        for (var that = validate(this, NAME), data = that._i, entry = that._f; entry; entry = entry.n) {
          entry.r = true;
          if (entry.p) entry.p = entry.p.n = undefined;
          delete data[entry.i];
        }
        that._f = that._l = undefined;
        that[SIZE] = 0;
      },
      // 23.1.3.3 Map.prototype.delete(key)
      // 23.2.3.4 Set.prototype.delete(value)
      'delete': function (key) {
        var that = validate(this, NAME);
        var entry = getEntry(that, key);
        if (entry) {
          var next = entry.n;
          var prev = entry.p;
          delete that._i[entry.i];
          entry.r = true;
          if (prev) prev.n = next;
          if (next) next.p = prev;
          if (that._f == entry) that._f = next;
          if (that._l == entry) that._l = prev;
          that[SIZE]--;
        } return !!entry;
      },
      // 23.2.3.6 Set.prototype.forEach(callbackfn, thisArg = undefined)
      // 23.1.3.5 Map.prototype.forEach(callbackfn, thisArg = undefined)
      forEach: function forEach(callbackfn /* , that = undefined */) {
        validate(this, NAME);
        var f = ctx(callbackfn, arguments.length > 1 ? arguments[1] : undefined, 3);
        var entry;
        while (entry = entry ? entry.n : this._f) {
          f(entry.v, entry.k, this);
          // revert to the last existing entry
          while (entry && entry.r) entry = entry.p;
        }
      },
      // 23.1.3.7 Map.prototype.has(key)
      // 23.2.3.7 Set.prototype.has(value)
      has: function has(key) {
        return !!getEntry(validate(this, NAME), key);
      }
    });
    if (DESCRIPTORS) dP(C.prototype, 'size', {
      get: function () {
        return validate(this, NAME)[SIZE];
      }
    });
    return C;
  },
  def: function (that, key, value) {
    var entry = getEntry(that, key);
    var prev, index;
    // change existing entry
    if (entry) {
      entry.v = value;
    // create new entry
    } else {
      that._l = entry = {
        i: index = fastKey(key, true), // <- index
        k: key,                        // <- key
        v: value,                      // <- value
        p: prev = that._l,             // <- previous entry
        n: undefined,                  // <- next entry
        r: false                       // <- removed
      };
      if (!that._f) that._f = entry;
      if (prev) prev.n = entry;
      that[SIZE]++;
      // add to index
      if (index !== 'F') that._i[index] = entry;
    } return that;
  },
  getEntry: getEntry,
  setStrong: function (C, NAME, IS_MAP) {
    // add .keys, .values, .entries, [@@iterator]
    // 23.1.3.4, 23.1.3.8, 23.1.3.11, 23.1.3.12, 23.2.3.5, 23.2.3.8, 23.2.3.10, 23.2.3.11
    $iterDefine(C, NAME, function (iterated, kind) {
      this._t = validate(iterated, NAME); // target
      this._k = kind;                     // kind
      this._l = undefined;                // previous
    }, function () {
      var that = this;
      var kind = that._k;
      var entry = that._l;
      // revert to the last existing entry
      while (entry && entry.r) entry = entry.p;
      // get next entry
      if (!that._t || !(that._l = entry = entry ? entry.n : that._t._f)) {
        // or finish the iteration
        that._t = undefined;
        return step(1);
      }
      // return step by kind
      if (kind == 'keys') return step(0, entry.k);
      if (kind == 'values') return step(0, entry.v);
      return step(0, [entry.k, entry.v]);
    }, IS_MAP ? 'entries' : 'values', !IS_MAP, true);

    // add [@@species], 23.1.2.2, 23.2.2.2
    setSpecies(NAME);
  }
};


/***/ }),

/***/ "c366":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("6821");
var toLength = __webpack_require__("9def");
var toAbsoluteIndex = __webpack_require__("77f1");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "c367":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__("8436");
var step = __webpack_require__("50ed");
var Iterators = __webpack_require__("481b");
var toIObject = __webpack_require__("36c3");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__("30f1")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "c3a1":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("e6f3");
var enumBugKeys = __webpack_require__("1691");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "c5f6":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__("7726");
var has = __webpack_require__("69a8");
var cof = __webpack_require__("2d95");
var inheritIfRequired = __webpack_require__("5dbc");
var toPrimitive = __webpack_require__("6a99");
var fails = __webpack_require__("79e5");
var gOPN = __webpack_require__("9093").f;
var gOPD = __webpack_require__("11e9").f;
var dP = __webpack_require__("86cc").f;
var $trim = __webpack_require__("aa77").trim;
var NUMBER = 'Number';
var $Number = global[NUMBER];
var Base = $Number;
var proto = $Number.prototype;
// Opera ~12 has broken Object#toString
var BROKEN_COF = cof(__webpack_require__("2aeb")(proto)) == NUMBER;
var TRIM = 'trim' in String.prototype;

// 7.1.3 ToNumber(argument)
var toNumber = function (argument) {
  var it = toPrimitive(argument, false);
  if (typeof it == 'string' && it.length > 2) {
    it = TRIM ? it.trim() : $trim(it, 3);
    var first = it.charCodeAt(0);
    var third, radix, maxCode;
    if (first === 43 || first === 45) {
      third = it.charCodeAt(2);
      if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
    } else if (first === 48) {
      switch (it.charCodeAt(1)) {
        case 66: case 98: radix = 2; maxCode = 49; break; // fast equal /^0b[01]+$/i
        case 79: case 111: radix = 8; maxCode = 55; break; // fast equal /^0o[0-7]+$/i
        default: return +it;
      }
      for (var digits = it.slice(2), i = 0, l = digits.length, code; i < l; i++) {
        code = digits.charCodeAt(i);
        // parseInt parses a string to a first unavailable symbol
        // but ToNumber should return NaN if a string contains unavailable symbols
        if (code < 48 || code > maxCode) return NaN;
      } return parseInt(digits, radix);
    }
  } return +it;
};

if (!$Number(' 0o1') || !$Number('0b1') || $Number('+0x1')) {
  $Number = function Number(value) {
    var it = arguments.length < 1 ? 0 : value;
    var that = this;
    return that instanceof $Number
      // check on 1..constructor(foo) case
      && (BROKEN_COF ? fails(function () { proto.valueOf.call(that); }) : cof(that) != NUMBER)
        ? inheritIfRequired(new Base(toNumber(it)), that, $Number) : toNumber(it);
  };
  for (var keys = __webpack_require__("9e1e") ? gOPN(Base) : (
    // ES3:
    'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' +
    // ES6 (in case, if modules with ES6 Number statics required before):
    'EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,' +
    'MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger'
  ).split(','), j = 0, key; keys.length > j; j++) {
    if (has(Base, key = keys[j]) && !has($Number, key)) {
      dP($Number, key, gOPD(Base, key));
    }
  }
  $Number.prototype = proto;
  proto.constructor = $Number;
  __webpack_require__("2aba")(global, NUMBER, $Number);
}


/***/ }),

/***/ "c69a":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("9e1e") && !__webpack_require__("79e5")(function () {
  return Object.defineProperty(__webpack_require__("230e")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "c6f0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CText_vue_vue_type_style_index_0_id_5fa5e14a_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("d415");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CText_vue_vue_type_style_index_0_id_5fa5e14a_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CText_vue_vue_type_style_index_0_id_5fa5e14a_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CText_vue_vue_type_style_index_0_id_5fa5e14a_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "c8bb":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("54a1");

/***/ }),

/***/ "ca5a":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "cadf":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__("9c6c");
var step = __webpack_require__("d53b");
var Iterators = __webpack_require__("84f2");
var toIObject = __webpack_require__("6821");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__("01f9")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "cb7c":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "ccb9":
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__("5168");


/***/ }),

/***/ "cd66":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "cd8d":
/***/ (function(module) {

module.exports = JSON.parse("{\"autocomplete\":true,\"selector\":true,\"unitsLimit\":3,\"floating\":true}");

/***/ }),

/***/ "ce10":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("69a8");
var toIObject = __webpack_require__("6821");
var arrayIndexOf = __webpack_require__("c366")(false);
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "ce7e":
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__("63b6");
var core = __webpack_require__("584a");
var fails = __webpack_require__("294c");
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),

/***/ "d2c8":
/***/ (function(module, exports, __webpack_require__) {

// helper for String#{startsWith, endsWith, includes}
var isRegExp = __webpack_require__("aae3");
var defined = __webpack_require__("be13");

module.exports = function (that, searchString, NAME) {
  if (isRegExp(searchString)) throw TypeError('String#' + NAME + " doesn't accept regex!");
  return String(defined(that));
};


/***/ }),

/***/ "d2d5":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1654");
__webpack_require__("549b");
module.exports = __webpack_require__("584a").Array.from;


/***/ }),

/***/ "d3f4":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "d415":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "d53b":
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "d864":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("79aa");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "d8d6":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1654");
__webpack_require__("6c1c");
module.exports = __webpack_require__("ccb9").f('iterator');


/***/ }),

/***/ "d8e8":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "d98b":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CLoading_vue_vue_type_style_index_0_id_2947297e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("8a04");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CLoading_vue_vue_type_style_index_0_id_2947297e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CLoading_vue_vue_type_style_index_0_id_2947297e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CLoading_vue_vue_type_style_index_0_id_2947297e_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "d9f6":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("e4ae");
var IE8_DOM_DEFINE = __webpack_require__("794b");
var toPrimitive = __webpack_require__("1bc3");
var dP = Object.defineProperty;

exports.f = __webpack_require__("8e60") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "dbdb":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("584a");
var global = __webpack_require__("e53d");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__("b8e3") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "dcbc":
/***/ (function(module, exports, __webpack_require__) {

var redefine = __webpack_require__("2aba");
module.exports = function (target, src, safe) {
  for (var key in src) redefine(target, key, src[key], safe);
  return target;
};


/***/ }),

/***/ "ddcf":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_id_4ac51c12_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("8ccc");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_id_4ac51c12_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_id_4ac51c12_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_id_4ac51c12_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "e0b8":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__("7726");
var $export = __webpack_require__("5ca1");
var redefine = __webpack_require__("2aba");
var redefineAll = __webpack_require__("dcbc");
var meta = __webpack_require__("67ab");
var forOf = __webpack_require__("4a59");
var anInstance = __webpack_require__("f605");
var isObject = __webpack_require__("d3f4");
var fails = __webpack_require__("79e5");
var $iterDetect = __webpack_require__("5cc5");
var setToStringTag = __webpack_require__("7f20");
var inheritIfRequired = __webpack_require__("5dbc");

module.exports = function (NAME, wrapper, methods, common, IS_MAP, IS_WEAK) {
  var Base = global[NAME];
  var C = Base;
  var ADDER = IS_MAP ? 'set' : 'add';
  var proto = C && C.prototype;
  var O = {};
  var fixMethod = function (KEY) {
    var fn = proto[KEY];
    redefine(proto, KEY,
      KEY == 'delete' ? function (a) {
        return IS_WEAK && !isObject(a) ? false : fn.call(this, a === 0 ? 0 : a);
      } : KEY == 'has' ? function has(a) {
        return IS_WEAK && !isObject(a) ? false : fn.call(this, a === 0 ? 0 : a);
      } : KEY == 'get' ? function get(a) {
        return IS_WEAK && !isObject(a) ? undefined : fn.call(this, a === 0 ? 0 : a);
      } : KEY == 'add' ? function add(a) { fn.call(this, a === 0 ? 0 : a); return this; }
        : function set(a, b) { fn.call(this, a === 0 ? 0 : a, b); return this; }
    );
  };
  if (typeof C != 'function' || !(IS_WEAK || proto.forEach && !fails(function () {
    new C().entries().next();
  }))) {
    // create collection constructor
    C = common.getConstructor(wrapper, NAME, IS_MAP, ADDER);
    redefineAll(C.prototype, methods);
    meta.NEED = true;
  } else {
    var instance = new C();
    // early implementations not supports chaining
    var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) != instance;
    // V8 ~  Chromium 40- weak-collections throws on primitives, but should return false
    var THROWS_ON_PRIMITIVES = fails(function () { instance.has(1); });
    // most early implementations doesn't supports iterables, most modern - not close it correctly
    var ACCEPT_ITERABLES = $iterDetect(function (iter) { new C(iter); }); // eslint-disable-line no-new
    // for early implementations -0 and +0 not the same
    var BUGGY_ZERO = !IS_WEAK && fails(function () {
      // V8 ~ Chromium 42- fails only with 5+ elements
      var $instance = new C();
      var index = 5;
      while (index--) $instance[ADDER](index, index);
      return !$instance.has(-0);
    });
    if (!ACCEPT_ITERABLES) {
      C = wrapper(function (target, iterable) {
        anInstance(target, C, NAME);
        var that = inheritIfRequired(new Base(), target, C);
        if (iterable != undefined) forOf(iterable, IS_MAP, that[ADDER], that);
        return that;
      });
      C.prototype = proto;
      proto.constructor = C;
    }
    if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
      fixMethod('delete');
      fixMethod('has');
      IS_MAP && fixMethod('get');
    }
    if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER);
    // weak collections should not contains .clear method
    if (IS_WEAK && proto.clear) delete proto.clear;
  }

  setToStringTag(C, NAME);

  O[NAME] = C;
  $export($export.G + $export.W + $export.F * (C != Base), O);

  if (!IS_WEAK) common.setStrong(C, NAME, IS_MAP);

  return C;
};


/***/ }),

/***/ "e11e":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "e265":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("ed33");

/***/ }),

/***/ "e2c0":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "e4ae":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("f772");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "e53d":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "e601":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "e6f3":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("07e3");
var toIObject = __webpack_require__("36c3");
var arrayIndexOf = __webpack_require__("5b4e")(false);
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "ebd6":
/***/ (function(module, exports, __webpack_require__) {

// 7.3.20 SpeciesConstructor(O, defaultConstructor)
var anObject = __webpack_require__("cb7c");
var aFunction = __webpack_require__("d8e8");
var SPECIES = __webpack_require__("2b4c")('species');
module.exports = function (O, D) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
};


/***/ }),

/***/ "ebfd":
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__("62a0")('meta');
var isObject = __webpack_require__("f772");
var has = __webpack_require__("07e3");
var setDesc = __webpack_require__("d9f6").f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__("294c")(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),

/***/ "ed33":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("014b");
module.exports = __webpack_require__("584a").Object.getOwnPropertySymbols;


/***/ }),

/***/ "f17c":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "f1ae":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $defineProperty = __webpack_require__("86cc");
var createDesc = __webpack_require__("4630");

module.exports = function (object, index, value) {
  if (index in object) $defineProperty.f(object, index, createDesc(0, value));
  else object[index] = value;
};


/***/ }),

/***/ "f400":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var strong = __webpack_require__("c26b");
var validate = __webpack_require__("b39a");
var MAP = 'Map';

// 23.1 Map Objects
module.exports = __webpack_require__("e0b8")(MAP, function (get) {
  return function Map() { return get(this, arguments.length > 0 ? arguments[0] : undefined); };
}, {
  // 23.1.3.6 Map.prototype.get(key)
  get: function get(key) {
    var entry = strong.getEntry(validate(this, MAP), key);
    return entry && entry.v;
  },
  // 23.1.3.9 Map.prototype.set(key, value)
  set: function set(key, value) {
    return strong.def(validate(this, MAP), key === 0 ? 0 : key, value);
  }
}, strong, true);


/***/ }),

/***/ "f410":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1af6");
module.exports = __webpack_require__("584a").Array.isArray;


/***/ }),

/***/ "f43b":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "f605":
/***/ (function(module, exports) {

module.exports = function (it, Constructor, name, forbiddenField) {
  if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
    throw TypeError(name + ': incorrect invocation!');
  } return it;
};


/***/ }),

/***/ "f6fd":
/***/ (function(module, exports) {

// document.currentScript polyfill by Adam Miller

// MIT license

(function(document){
  var currentScript = "currentScript",
      scripts = document.getElementsByTagName('script'); // Live NodeList collection

  // If browser needs currentScript polyfill, add get currentScript() to the document object
  if (!(currentScript in document)) {
    Object.defineProperty(document, currentScript, {
      get: function(){

        // IE 6-10 supports script readyState
        // IE 10+ support stack trace
        try { throw new Error(); }
        catch (err) {

          // Find the second match for the "at" string to get file src url from stack.
          // Specifically works with the format of stack traces in IE.
          var i, res = ((/.*at [^\(]*\((.*):.+:.+\)$/ig).exec(err.stack) || [false])[1];

          // For all scripts on the page, if src matches or if ready state is interactive, return the script tag
          for(i in scripts){
            if(scripts[i].src == res || scripts[i].readyState == "interactive"){
              return scripts[i];
            }
          }

          // If no match, return null
          return null;
        }
      }
    });
  }
})(document);


/***/ }),

/***/ "f736":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Free_vue_vue_type_style_index_0_id_3cdd7e46_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("bda0");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Free_vue_vue_type_style_index_0_id_3cdd7e46_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Free_vue_vue_type_style_index_0_id_3cdd7e46_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_9_oneOf_1_0_node_modules_css_loader_index_js_ref_9_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_9_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Free_vue_vue_type_style_index_0_id_3cdd7e46_lang_sass_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "f751":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__("5ca1");

$export($export.S + $export.F, 'Object', { assign: __webpack_require__("7333") });


/***/ }),

/***/ "f772":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "f921":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("014b");
__webpack_require__("c207");
__webpack_require__("69d3");
__webpack_require__("765d");
module.exports = __webpack_require__("584a").Symbol;


/***/ }),

/***/ "fa5b":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("5537")('native-function-to-string', Function.toString);


/***/ }),

/***/ "fab2":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("7726").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  if (true) {
    __webpack_require__("f6fd")
  }

  var setPublicPath_i
  if ((setPublicPath_i = window.document.currentScript) && (setPublicPath_i = setPublicPath_i.src.match(/(.+\/)[^/]+\.js(\?.*)?$/))) {
    __webpack_require__.p = setPublicPath_i[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.regexp.split.js
var es6_regexp_split = __webpack_require__("28a5");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es7.array.includes.js
var es7_array_includes = __webpack_require__("6762");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.string.includes.js
var es6_string_includes = __webpack_require__("2fdb");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.function.name.js
var es6_function_name = __webpack_require__("7f7f");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/array/is-array.js
var is_array = __webpack_require__("a745");
var is_array_default = /*#__PURE__*/__webpack_require__.n(is_array);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/arrayLikeToArray.js
function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/arrayWithoutHoles.js


function _arrayWithoutHoles(arr) {
  if (is_array_default()(arr)) return _arrayLikeToArray(arr);
}
// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/array/from.js
var from = __webpack_require__("774e");
var from_default = /*#__PURE__*/__webpack_require__.n(from);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/is-iterable.js
var is_iterable = __webpack_require__("c8bb");
var is_iterable_default = /*#__PURE__*/__webpack_require__.n(is_iterable);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/symbol.js
var symbol = __webpack_require__("67bb");
var symbol_default = /*#__PURE__*/__webpack_require__.n(symbol);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/iterableToArray.js



function _iterableToArray(iter) {
  if (typeof symbol_default.a !== "undefined" && is_iterable_default()(Object(iter))) return from_default()(iter);
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/unsupportedIterableToArray.js


function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return from_default()(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/nonIterableSpread.js
function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/toConsumableArray.js




function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}
// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom.iterable.js
var web_dom_iterable = __webpack_require__("ac6a");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Main.vue?vue&type=template&id=f5c2c68a&lang=pug&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.$colvis.isInit())?_c('section',{staticClass:"col-input-main",class:{ 'c-dark': _vm.$colvis.getOptions().theme === 'dark', 'c-light': _vm.$colvis.getOptions().theme === 'light' }},[_c('col-input-selector',{directives:[{name:"show",rawName:"v-show",value:(_vm.annotating || !!_vm.replyTo),expression:"annotating || !!replyTo"}],attrs:{"init":_vm.$colvis.isInit(),"offsetTop":_vm.offsetTop,"free":_vm.free,"selectedSubjects":_vm.selectedSubjects,"selectedComplements":_vm.selectedComplements,"step":_vm.step},on:{"subjectSelection":function($event){return _vm.selectElementsBySelector($event)},"complementSelection":function($event){return _vm.selectElementsBySelector($event, 'complement')}}}),_c('col-input-overlay',{attrs:{"annotating":_vm.annotating || !!_vm.replyTo,"previousTags":_vm.previousTags,"previousUnits":_vm.previousUnits,"selectedSubjects":_vm.selectedSubjects,"selectedComplements":_vm.selectedComplements,"selectionMethods":_vm.selectionMethods,"loading":_vm.loading,"free":_vm.free,"replyTo":_vm.replyTo},on:{"close":function($event){return _vm.$emit('close')},"stepChanged":function($event){_vm.step = $event},"boxSubjectSelection":function($event){return _vm.selectElementsByBox($event)},"boxComplementSelection":function($event){return _vm.selectElementsByBox($event, 'complement')},"submittingAnnotation":_vm.submit,"free":function($event){_vm.free = $event},"stopReplying":function($event){return _vm.$emit('stopReplying')}}})],1):_vm._e()}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Input/Main.vue?vue&type=template&id=f5c2c68a&lang=pug&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.object.assign.js
var es6_object_assign = __webpack_require__("f751");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Selector.vue?vue&type=template&id=63b44dfe&lang=pug&
var Selectorvue_type_template_id_63b44dfe_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.$colvis.hasSelector())?_c('svg',{ref:"selection",staticClass:"col-input-selector",class:{ active: [1,3].includes(_vm.step) && !_vm.free, selecting: _vm.selecting && !_vm.selectionAdd, adding: _vm.selecting && _vm.selectionAdd },style:({ top: (_vm.compTop + "px"), left: (_vm.compLeft + "px"), width: (_vm.width + "px"), height: (_vm.height + "px") }),on:{"mousedown":_vm.start,"mousemove":_vm.setRect,"mouseup":_vm.sendCoordinates}},[_c('rect',{staticClass:"col-input-selector__selection",attrs:{"x":_vm.selectionX,"y":_vm.selectionY,"width":_vm.selectionWidth,"height":_vm.selectionHeight}}),_vm._l((_vm.pathPool),function(el,i){return _c('path',{key:i,staticClass:"col-input-selector__selected",class:{ active: el.step === _vm.step },attrs:{"transform":_vm.getTransform(el),"d":el.domElement.attributes.d.value}})}),_vm._l((_vm.circlePool),function(el,i){return _c('circle',{key:i,staticClass:"col-input-selector__selected",class:{ active: el.step === _vm.step },attrs:{"transform":_vm.getTransform(el),"cx":el.domElement.cx.animVal.value,"cy":el.domElement.cy.animVal.value,"r":el.domElement.r.animVal.value}})}),_vm._l((_vm.rectPool),function(el,i){return _c('rect',{key:i,staticClass:"col-input-selector__selected",class:{ active: el.step === _vm.step },attrs:{"transform":_vm.getTransform(el),"x":el.domElement.attributes.x.value,"y":el.domElement.attributes.y.value,"width":el.domElement.attributes.width.value,"height":el.domElement.attributes.height.value}})})],2):_vm._e()}
var Selectorvue_type_template_id_63b44dfe_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Input/Selector.vue?vue&type=template&id=63b44dfe&lang=pug&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.array.iterator.js
var es6_array_iterator = __webpack_require__("cadf");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.object.keys.js
var es6_object_keys = __webpack_require__("456d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.number.constructor.js
var es6_number_constructor = __webpack_require__("c5f6");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Selector.vue?vue&type=script&lang=js&





//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/**
 * Create a new Selector.
 * Selectors are the visual interface that allows analysts to select data units from their visualizations.
 * @extends Vue
 */
/* harmony default export */ var Selectorvue_type_script_lang_js_ = ({
  props: {
    /** Whether the visualization has been initialized */
    init: {
      type: Boolean,
      required: true
    },

    /** The list of selected subjects */
    selectedSubjects: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The list of selected complements */
    selectedComplements: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The current step of the annotation */
    step: {
      type: Number,
      required: true
    },

    /** Whether a free annotation is being taken */
    free: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    /** The top position of the Selector, in pixels */
    var top = 0;
    /** The left position of the Selector, in pixels */

    var left = 0;
    /** The width of the Selector, in pixels */

    var width = 0;
    /** The height of the Selector, in pixels */

    var height = 0;
    /** Some parent elements of the Selector might be in Position relative  */

    var offsetTop = 0;
    var offsetLeft = 0;
    /** Whether the user is currently selecting something in the Selector */

    var selecting = false;
    /** The 'x' attribute of the rectangular selection */

    var selectionX = 0;
    /** The 'y' attribute of the rectangular selection */

    var selectionY = 0;
    /** The 'width' attribute of the rectangular selection */

    var selectionWidth = 0;
    /** The 'height' attribute of the rectangular selection */

    var selectionHeight = 0;
    /** The original 'x' attribute of the rectangular selection. Needed to understand if going backwards. */

    var selectionXOrigin = 0;
    /** The original 'y' attribute of the rectangular selection. Needed to understand if going upwards. */

    var selectionYOrigin = 0;
    /** Whether the selection is in "add" or "replace" (default) mode */

    var selectionAdd = false;
    /** List of event listeners to remove once the component is destroyed */

    var eventListeners = [];
    /** ResizeObserver to make sure that the selector matches the SVG it overlays */

    var resizeObserver = null;
    return {
      top: top,
      left: left,
      width: width,
      height: height,
      offsetTop: offsetTop,
      offsetLeft: offsetLeft,
      selecting: selecting,
      selectionX: selectionX,
      selectionY: selectionY,
      selectionWidth: selectionWidth,
      selectionHeight: selectionHeight,
      selectionXOrigin: selectionXOrigin,
      selectionYOrigin: selectionYOrigin,
      eventListeners: eventListeners,
      resizeObserver: resizeObserver
    };
  },
  computed: {
    /** @returns the list of selected DataBinders, either as subjects or complements. */
    pool: function pool() {
      return this.selectedSubjects.map(function (s) {
        s.step = 1;
        return s;
      }).concat(this.selectedComplements.map(function (c) {
        c.step = 3;
        return c;
      })); // return this.step === 1 ? this.selectedSubjects.map(s => { s.step = 1; return s; }) : this.selectedComplements.map(c => { c.step = 3; return c });
    },

    /** @returns the list of paths within the selected DataBinders. */
    pathPool: function pathPool() {
      return this.pool.filter(function (binder) {
        return binder.domElement.tagName === 'path';
      });
    },

    /** @returns the list of circles within the selected DataBinders. */
    circlePool: function circlePool() {
      return this.pool.filter(function (binder) {
        return binder.domElement.tagName === 'circle';
      });
    },

    /** @returns the list of rectangles within the selected DataBinders. */
    rectPool: function rectPool() {
      return this.pool.filter(function (binder) {
        return binder.domElement.tagName === 'rect';
      });
    },
    compLeft: function compLeft() {
      return this.left - this.offsetLeft;
    },
    compTop: function compTop() {
      return this.top - this.offsetTop;
    }
  },
  methods: {
    /**
     * Return the translation value of a given element, based on its transformation matrix.
     * It allows to restore the element's precise position despite potential transformations
     * implied by parents.
     * @param {DataBinder} element the element whose translate value is required
     */
    getTransform: function getTransform(element) {
      if (!(element.domElement instanceof SVGGraphicsElement)) throw new Error('The selected element is not a SVG element');
      var matrix = element.domElement.getCTM();
      return matrix ? "translate(".concat(matrix.e, ", ").concat(matrix.f, ")") : "";
    },

    /**
     * Get the position of the container of the visualization.
     * Setting { top, left, width, height } properties of the instance.
     */
    getPosition: function getPosition() {
      var _this = this;

      function position() {
        var target = document.querySelector(this.$colvis.getSpecs().visualization.container);

        if (!target) {
          throw new Error('Could not locate a target container for the Selector. Please make sure the specifications is set correctly.');
        }

        var offsetTop = 0;
        var offsetLeft = 0;
        var offsetter = this.$refs.selection.parentElement;

        while (offsetter.offsetParent) {
          offsetTop += offsetter.offsetParent.offsetTop;
          offsetLeft += offsetter.offsetParent.offsetLeft;
          offsetter = offsetter.offsetParent;
        }

        var _target$getBoundingCl = target.getBoundingClientRect(),
            top = _target$getBoundingCl.top,
            left = _target$getBoundingCl.left,
            width = _target$getBoundingCl.width,
            height = _target$getBoundingCl.height;

        Object.assign(this.$data, {
          top: top,
          left: left,
          width: width,
          height: height,
          offsetTop: offsetTop,
          offsetLeft: offsetLeft
        });
      }

      position.call(this);

      if (this.height <= 0) {
        console.warn('Could not attach Selector. Will try again in 1 second.');
        var interval = window.setInterval(function (e) {
          position.call(_this);

          if (_this.height > 0) {
            console.log('Selector attached');
            clearInterval(interval);
          } else console.warn('Could not attach Selector. Will try again in 1 second.');
        }, 1000);
      }
    },

    /**
     * Startup function that tracks the move of the mouse over the selector's area.
     * Called on mousedown.
     * Setting { selecting, selectionXOrigin, selectionYOrigin } properties of the instance.
     * @param {MouseEvent} event the mousedown event
     */
    start: function start(event) {
      this.selecting = true;
      if (event.ctrlKey || event.shiftKey) this.selectionAdd = true;else this.selectionAdd = false;
      var boundingBox = this.$refs.selection.getBoundingClientRect();
      this.selectionXOrigin = this.selectionX = event.clientX - boundingBox.left;
      this.selectionYOrigin = this.selectionY = event.clientY - boundingBox.top;
    },
    changeMode: function changeMode(event) {
      if (event.type === 'keyup') {
        if (event.key === 'Control') this.selectionAdd = false;
      } else {
        if (event.key === 'Control') this.selectionAdd = true;else this.selectionAdd = false;
      }
    },

    /**
     * Compute the coordinates of the selection rectangle.
     * Function called while moving across the selector's area.
     * Setting
     * { selectionX, selectonWidth, selectionXOrigin, selectionY, selectionHeight, selectionYOrigin }
     * properties of the instance.
     * @param {MouseEvent} event the mousemove event
     */
    setRect: function setRect(event) {
      if (!this.selecting) return;
      var boundingBox = this.$refs.selection.getBoundingClientRect();
      var x = event.clientX - boundingBox.left;
      var y = event.clientY - boundingBox.top;
      if (x > this.selectionXOrigin) this.selectionWidth = x - this.selectionXOrigin;else {
        this.selectionX = x;
        this.selectionWidth = this.selectionXOrigin - x;
      }
      if (y > this.selectionYOrigin) this.selectionHeight = y - this.selectionYOrigin;else {
        this.selectionY = y;
        this.selectionHeight = this.selectionYOrigin - y;
      }
    },

    /**
     * Reset the selection rectangle's values.
     * Setting { selecting, selectionX, selectionY, selectionWidth, selectionHeight } properties of the instance.
     */
    reset: function reset() {
      this.selecting = false;
      this.selectionX = 0;
      this.selectionY = 0;
      this.selectionWidth = 0;
      this.selectionHeight = 0;
    },

    /**
     * Send the coordinates of the rectangle to parents.
     */
    sendCoordinates: function sendCoordinates() {
      var base = this.$refs.selection.getBoundingClientRect();
      this.$emit("".concat(this.step === 1 ? 'subject' : 'complement', "Selection"), {
        coordinates: {
          x: this.selectionX + base.left,
          y: this.selectionY + base.top,
          width: this.selectionWidth,
          height: this.selectionHeight
        },
        mode: {
          add: this.selectionAdd
        }
      });
      this.reset();
    }
  },
  watch: {
    init: function init() {
      if (this.$colvis.isInit()) this.getPosition();
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    this.getPosition();
    addEventListener('resize', this.getPosition);
    this.eventListeners.push({
      resize: this.getPosition
    });
    addEventListener('keydown', this.changeMode);
    this.eventListeners.push({
      keydown: this.changeMode
    });
    addEventListener('keyup', this.changeMode);
    this.eventListeners.push({
      keyup: this.changeMode
    });
    /**
     * We resize the selector when the selector's parent's element is resized. We cannot observe
     * SVGs, since there are multiple issues with SVG and bounding boxes.
     */

    this.resizeObserver = new ResizeObserver(function (e) {
      console.log('triggering resize');

      _this2.getPosition();
    });
    this.resizeObserver.observe(this.$refs.selection.parentElement);
  },
  destroyed: function destroyed() {
    this.eventListeners.forEach(function (e) {
      Object.keys(e).forEach(function (key) {
        removeEventListener(key, e[key]);
      });
    });
    this.resizeObserver.disconnect();
  }
});
// CONCATENATED MODULE: ./src/components/Input/Selector.vue?vue&type=script&lang=js&
 /* harmony default export */ var Input_Selectorvue_type_script_lang_js_ = (Selectorvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Input/Selector.vue?vue&type=style&index=0&lang=sass&
var Selectorvue_type_style_index_0_lang_sass_ = __webpack_require__("94a1");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/components/Input/Selector.vue






/* normalize component */

var component = normalizeComponent(
  Input_Selectorvue_type_script_lang_js_,
  Selectorvue_type_template_id_63b44dfe_lang_pug_render,
  Selectorvue_type_template_id_63b44dfe_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var Selector = (component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Overlay.vue?vue&type=template&id=7be65390&scoped=true&lang=pug&
var Overlayvue_type_template_id_7be65390_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('aside',{ref:"overlay",staticClass:"col-input-overlay",on:{"mousemove":_vm.move}},[_c('nav',{ref:"box",staticClass:"col-input-overlay__box",class:{ annotating: _vm.annotating, floating: _vm.$colvis.getOptions().floating },style:({ top: _vm.topPx, left: _vm.leftPx })},[_c('header',{staticClass:"toolbar",on:{"mousedown":_vm.startMoving,"mousemove":_vm.move,"mouseup":_vm.stopMoving}},[_c('h3',{staticClass:"header"},[_vm._v("Taking an annotation")]),_c('button',{attrs:{"icon":""},on:{"mousedown":function($event){$event.stopPropagation();return _vm.$emit('close')}}},[_vm._v("Close")])]),_c('c-loading',{directives:[{name:"show",rawName:"v-show",value:(_vm.loading),expression:"loading"}]}),_c('nav',{staticClass:"col-input-overlay__box__nav"},[_c('label',[_vm._v("Structured annotation"),_c('input',{attrs:{"type":"checkbox"},domProps:{"checked":!_vm.free},on:{"input":function($event){return _vm.$emit('free', !_vm.free)}}})]),(_vm.replyTo)?_c('div',{staticClass:"reply"},[_c('span',[_vm._v("You are replying to an annotation")]),_c('small',{staticClass:"clear",on:{"click":function($event){return _vm.$emit('stopReplying')}}},[_vm._v("Clear")]),_c('div',{staticClass:"reply__details"},[_c('div',{staticClass:"reply__details__text"},[_c('c-text',{attrs:{"annotation":_vm.replyTo,"clickEvent":"selectUnit"},on:{"selectUnit":_vm.selectUnit}})],1)])]):_vm._e()]),(!_vm.free)?_c('col-input-box',{attrs:{"previousTags":_vm.previousTags,"previousUnits":_vm.previousUnits,"unitToRestore":_vm.unitToRestore,"selectedComplements":_vm.selectedComplements,"selectedSubjects":_vm.selectedSubjects,"selectionMethods":_vm.selectionMethods,"replyTo":_vm.replyTo,"replyType":_vm.replyTo ? _vm.replyType : null},on:{"stepChanged":function($event){return _vm.$emit('stepChanged', $event)},"boxSubjectSelection":function($event){return _vm.$emit('boxSubjectSelection', $event)},"boxComplementSelection":function($event){return _vm.$emit('boxComplementSelection', $event)},"submittingAnnotation":function($event){return _vm.$emit('submittingAnnotation', $event)},"unitRestored":function($event){_vm.unitToRestore = null}}}):_c('col-free-box',{attrs:{"previousTags":_vm.previousTags,"replyTo":_vm.replyTo,"replyType":_vm.replyTo ? _vm.replyType : null},on:{"submittingAnnotation":function($event){return _vm.$emit('submittingAnnotation', $event)}}})],1)])}
var Overlayvue_type_template_id_7be65390_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Input/Overlay.vue?vue&type=template&id=7be65390&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Box.vue?vue&type=template&id=7fc82d01&lang=pug&
var Boxvue_type_template_id_7fc82d01_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('form',{ref:"boxForm",on:{"submit":function($event){$event.preventDefault();return _vm.submit($event)}}},[_c('c-message',{model:{value:(_vm.colvisEvent),callback:function ($$v) {_vm.colvisEvent=$$v},expression:"colvisEvent"}}),(_vm.structured)?_c('div',{staticClass:"stepper",model:{value:(_vm.step),callback:function ($$v) {_vm.step=$$v},expression:"step"}},[_c('div',{staticClass:"stepper__step",class:{ active: _vm.step === 1 },on:{"click":function($event){_vm.step = 1}}},[_c('header',{staticClass:"stepper__step__head actionable"},[_c('span',[_vm._v("① What are you talking about?")]),_c('c-icon',{directives:[{name:"show",rawName:"v-show",value:(_vm.selectedSubjects.length),expression:"selectedSubjects.length"}],attrs:{"type":"success","message":"Step completed"}},[_vm._v("✅️")]),_c('c-icon',{directives:[{name:"show",rawName:"v-show",value:(!_vm.selectedSubjects.length && _vm.step > 1),expression:"!selectedSubjects.length && step > 1"}],attrs:{"type":"error","message":"Mandatory step"}},[_vm._v("⚠️")])],1),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.step === 1),expression:"step === 1"}],staticClass:"stepper__step__content"},[(_vm.$colvis.hasSelector())?_c('small',[_vm._v("You can select data from the selector box over your visualization.")]):_vm._e(),(_vm.$colvis.hasAutocomplete())?_c('c-autocomplete',{attrs:{"id":"subject","label":"Subjects","items":_vm.$colvis.getAnnotables(),"value":_vm.selectedSubjects},on:{"input":function($event){return _vm.$emit('boxSubjectSelection', $event)}}}):_vm._e(),_c('transition',{attrs:{"name":"slide-down-fade"}},[(_vm.selectedSubjects.length > 1)?_c('c-groupname',{attrs:{"id":"subject-name","selection":_vm.selectedSubjects},model:{value:(_vm.subjectName),callback:function ($$v) {_vm.subjectName=$$v},expression:"subjectName"}}):_vm._e()],1),(_vm.subjectSuggestions.length)?_c('c-suggestion',{attrs:{"suggestions":_vm.subjectSuggestions},on:{"choose":function($event){return _vm.chooseSuggestion($event, 'subject')}}}):_vm._e()],1)]),_c('div',{staticClass:"stepper__step",class:{ active: _vm.step === 2 },on:{"click":function($event){_vm.step = 2}}},[_c('header',{staticClass:"stepper__step__head actionable"},[_c('span',[_vm._v("② What makes the subjects relevant?")]),_c('c-icon',{directives:[{name:"show",rawName:"v-show",value:(_vm.reason.verb || _vm.reason.details),expression:"reason.verb || reason.details"}],attrs:{"type":"success","message":"Step completed"}},[_vm._v("✅️")]),_c('c-icon',{directives:[{name:"show",rawName:"v-show",value:(!_vm.reason.verb && !_vm.reason.details && _vm.step > 2),expression:"!reason.verb && !reason.details && step > 2"}],attrs:{"type":"error","message":"Mandatory step"}},[_vm._v("⚠️")])],1),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.step === 2),expression:"step === 2"}],staticClass:"stepper__step__content"},[_c('c-dialog',{attrs:{"entities":_vm.$colvis.getEntities(),"subject":_vm.selectedSubjects,"complement":_vm.selectedComplements,"combinations":_vm.$colvis.getSpecs().combinations,"isGenerality":_vm.isGenerality},on:{"selection":function($event){_vm.reason = $event}}})],1)]),(!_vm.isGenerality)?_c('div',{staticClass:"stepper__step",class:{ active: _vm.step === 3 },on:{"click":function($event){_vm.step = 3}}},[_c('header',{staticClass:"stepper__step__head actionable"},[_c('span',[_vm._v("③ Do you compare subjects to other data?")]),_c('c-icon',{directives:[{name:"show",rawName:"v-show",value:(_vm.selectedComplements.length),expression:"selectedComplements.length"}],attrs:{"type":"success","message":"Step completed"}},[_vm._v("✅️")])],1),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.step === 3),expression:"step === 3"}],staticClass:"stepper__step__content"},[(_vm.$colvis.hasSelector())?_c('small',[_vm._v("You can select data from the selector box over your visualization.")]):_vm._e(),(_vm.$colvis.hasAutocomplete())?_c('c-autocomplete',{attrs:{"id":"complement","label":"Complements","items":_vm.$colvis.getAnnotables(),"value":_vm.selectedComplements},on:{"input":function($event){return _vm.$emit('boxComplementSelection', $event)}}}):_vm._e(),_c('transition',{attrs:{"name":"slide-down-fade"}},[(_vm.selectedComplements.length > 1)?_c('c-groupname',{attrs:{"id":"complement-name","selection":_vm.selectedComplements},model:{value:(_vm.complementName),callback:function ($$v) {_vm.complementName=$$v},expression:"complementName"}}):_vm._e()],1),(_vm.complementSuggestions.length)?_c('c-suggestion',{attrs:{"suggestions":_vm.complementSuggestions},on:{"choose":function($event){return _vm.chooseSuggestion($event, 'complement')}}}):_vm._e()],1)]):_vm._e(),_c('div',{staticClass:"stepper__step",class:{ active: _vm.step === 4 },on:{"click":function($event){_vm.step = 4}}},[_c('header',{staticClass:"stepper__step__head actionable"},[_c('span',[_vm._v("④ What do you infer from this observation?")]),_c('c-icon',{directives:[{name:"show",rawName:"v-show",value:(_vm.conclusion),expression:"conclusion"}],attrs:{"type":"success","message":"Step completed"}},[_vm._v("✅️")])],1),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.step === 4),expression:"step === 4"}],staticClass:"stepper__step__content"},[_c('textarea',{directives:[{name:"model",rawName:"v-model",value:(_vm.conclusion),expression:"conclusion"}],staticClass:"textarea",attrs:{"label":"Conclusion","placeholder":"It seems that..."},domProps:{"value":(_vm.conclusion)},on:{"input":function($event){if($event.target.composing){ return; }_vm.conclusion=$event.target.value}}}),_c('c-combobox',{attrs:{"label":"Tags","placeholder":"myItems, generalObservation, etc...","hint":"You can tag your annotation to make it easier to retrieve. To use several tags, separate them by typing space, enter or comma.","items":_vm.previousTags},model:{value:(_vm.tags),callback:function ($$v) {_vm.tags=$$v},expression:"tags"}})],1)])]):_vm._e(),_c('div',{staticClass:"form-submit"},[_c('button',{directives:[{name:"show",rawName:"v-show",value:(_vm.theAnnotation),expression:"theAnnotation"}],staticClass:"btn",on:{"click":function($event){$event.preventDefault();_vm.preview = !_vm.preview}}},[_vm._v(_vm._s(_vm.preview ? 'Hide preview' : 'Preview'))]),_c('button',{staticClass:"btn btn-main",attrs:{"type":"submit"}},[_vm._v("Submit")])]),_c('div',{staticClass:"stepper"},[(_vm.theAnnotation && _vm.preview)?_c('div',{staticClass:"stepper__step preview"},[_vm._m(0),(_vm.theAnnotation)?_c('div',{staticClass:"stepper__step__content"},[_c('c-text',{attrs:{"annotation":_vm.theAnnotation}})],1):_vm._e()]):_vm._e()])],1)}
var Boxvue_type_template_id_7fc82d01_lang_pug_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('header',{staticClass:"stepper__step__head"},[_c('span',[_vm._v("Preview")]),_c('small',[_vm._v("how your annotation looks so far")])])}]


// CONCATENATED MODULE: ./src/components/Input/Box.vue?vue&type=template&id=7fc82d01&lang=pug&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es7.object.get-own-property-descriptors.js
var es7_object_get_own_property_descriptors = __webpack_require__("8e6e");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/symbol/iterator.js
var iterator = __webpack_require__("5d58");
var iterator_default = /*#__PURE__*/__webpack_require__.n(iterator);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/typeof.js


function typeof_typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof symbol_default.a === "function" && typeof iterator_default.a === "symbol") {
    typeof_typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    typeof_typeof = function _typeof(obj) {
      return obj && typeof symbol_default.a === "function" && obj.constructor === symbol_default.a && obj !== symbol_default.a.prototype ? "symbol" : typeof obj;
    };
  }

  return typeof_typeof(obj);
}
// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("e265");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var keys = __webpack_require__("a4bb");
var keys_default = /*#__PURE__*/__webpack_require__.n(keys);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutPropertiesLoose.js

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};

  var sourceKeys = keys_default()(source);

  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutProperties.js


function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = _objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (get_own_property_symbols_default.a) {
    var sourceSymbolKeys = get_own_property_symbols_default()(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}
// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("85f2");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js


function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;

    define_property_default()(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js

function _defineProperty(obj, key, value) {
  if (key in obj) {
    define_property_default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}
// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.array.from.js
var es6_array_from = __webpack_require__("1c4c");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.string.iterator.js
var es6_string_iterator = __webpack_require__("5df3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.map.js
var es6_map = __webpack_require__("f400");

// CONCATENATED MODULE: ./src/assets/utils/index.js











/**
 * Check if a given element has a parent with the same __data__ property.
 * @param el
 */

function hasSelectableParent(el) {
  var method = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'vega';
  if (!el.parentElement || !el.parentElement.__data__) return false;
  if (method === 'vega') return strfy(el.parentElement.__data__.datum) === strfy(el.__data__.datum);else return strfy(el.parentElement.__data__) === strfy(el.__data__);
}
/**
 * Take a selection and return a duplicate-free version
 * @param {DataBinder[]} selection
 * @returns {DataBinder[]} duplicate-free version of the selection 
 */


function enforceUniqueId(selection) {
  var m = new Map(selection.map(function (s) {
    return [s.id, s];
  }));
  var a = Array.from(m).map(function (a) {
    return a[1];
  });
  return a;
}
/**
 * Ensure a DataBinder is not already present within a selection
 * @param {DataBinder} el 
 * @param {DataBinder[]} selection 
 * @returns {Boolean} whether it is not present
 */

function ensureNotPresent(el, selection) {
  var ids = selection.map(function (e) {
    return e.id;
  });
  return !ids.includes(el.id);
}
/**
 * Stringify an object, ignoring all circular references.
 * @param obj object to stringify
 */

function strfy(obj) {
  var cache = [];
  return JSON.stringify(obj, function (key, value) {
    if (typeof_typeof(value) === 'object' && value !== null) {
      if (cache.indexOf(value) !== -1) return;
      cache.push(value);
    }

    return value;
  });
}
/**
 * Return the unique identifier of an entity
 * @param {Object} options
 * @param {DataBinder} options.entity
 * @param {String[]} options.fields
 * @param {String|Boolean} options.defaultValue
 * @returns {String} the id of the identity 
 */

function getEntityId(_ref) {
  var entity = _ref.entity,
      fields = _ref.fields,
      _ref$defaultValue = _ref.defaultValue,
      defaultValue = _ref$defaultValue === void 0 ? false : _ref$defaultValue;

  if (!fields) {
    if (!defaultValue) throw new Error('Please provide either a list of fields or a default value');else return defaultValue;
  } else {
    var values = fields.map(function (f) {
      if (!f) return;
      var steps = f.split('.');
      if (steps.length === 1) return entity[f];else {
        return steps.reduce(function (prev, cur) {
          return prev[cur];
        }, entity);
      }
    });
    return values.join('-');
  }
}
/**
 * Return the title of an entity, to display in the annotation
 * @param {Object} options
 * @param {DataBinder} options.entity
 * @param {String[]} options.fields
 * @param {String|Boolean} options.defaultValue
 * @returns {String|null} the title of the identity 
 */

function getEntityTitle(_ref2) {
  var entity = _ref2.entity,
      fields = _ref2.fields,
      _ref2$defaultValue = _ref2.defaultValue,
      defaultValue = _ref2$defaultValue === void 0 ? false : _ref2$defaultValue;

  if (!fields) {
    return defaultValue || null;
  } else {
    var values = fields.map(function (f) {
      if (!f) return;
      var steps = f.split('.');
      if (steps.length === 1) return entity[f];else {
        return steps.reduce(function (prev, cur) {
          return prev[cur];
        }, entity);
      }
    });
    return values.join('-');
  }
}
/**
 * Return an array of DataBinder to be used by the AnnotationBox.
 * If using Vega, containerSelector should be a selector for the element where Vega 
 * creates the SVG. If using D3, containerSelector should be the actual selector of the SVG.
 * @param {string} containerSelector the selector used to identify where to look for data. Example: '.svgContainer'
 * @param {GetDataFromContainerOptions} options misc options
 */

function getDataFromContainer(containerSelector, options) {
  var method = options.method || 'vega';
  var filter = options.filter || null;
  var natureId = options.natureId || null;
  var natureMarkType = options.natureMarkType || null;
  var selector = options.selector || '*';
  var allResults = options.allResults || false;
  var data = [];
  var idFields = options.id instanceof Array ? options.id : [options.id];
  var titleFields;

  if (options.title) {
    titleFields = options.title instanceof Array ? options.title : [options.title];
  }

  if (method === 'vega') {
    var d = Array.from(document.querySelectorAll("".concat(containerSelector, " svg ").concat(selector))).filter(function (el) {
      return el.__data__ && (!natureMarkType || el.__data__.shape === natureMarkType || el.__data__.mark.marktype === natureMarkType) && el.__data__.datum && (!filter || el.__data__.datum[filter]) && (allResults || !hasSelectableParent(el));
    }).map(function (el, index) {
      return new DataBinder_DataBinder({
        id: getEntityId({
          entity: el.__data__.datum,
          fields: idFields,
          defaultValue: index
        }),
        title: getEntityTitle({
          entity: el.__data__.datum,
          fields: titleFields,
          defaultValue: index
        }),
        natureId: natureId,
        obj: el.__data__.datum,
        domElement: el
      });
    });
    data.push.apply(data, _toConsumableArray(d));
  } else if (method === 'd3') {
    var _d = Array.from(document.querySelectorAll("".concat(containerSelector, " ").concat(selector))).filter(function (el) {
      return el.__data__ && (!natureMarkType || el.tagName === natureMarkType) && (!filter || el.__data__[filter]) && (allResults || !hasSelectableParent(el, 'd3'));
    }).map(function (el, index) {
      return new DataBinder_DataBinder({
        id: getEntityId({
          entity: el.__data__,
          fields: idFields,
          defaultValue: index
        }),
        title: getEntityTitle({
          entity: el.__data__,
          fields: titleFields,
          defaultValue: index
        }),
        natureId: natureId,
        obj: el.__data__,
        domElement: el
      });
    });

    data.push.apply(data, _toConsumableArray(_d));
  }

  return data;
}
/**
 * Check if an element is being crossed by the rectangular selection.
 * @param {ClientRect} elBoundingRect the ClientRect of the element being checked
 * @param {SelectionCoordinates} coordinates the coordinates of the selection
 * @returns {Boolean} whether the rectangle is selected or not
 */

function checkIfSelected(elBoundingRect, coordinates) {
  var match = true; // X-axis

  if (elBoundingRect.left > coordinates.x + coordinates.width) match = false;
  if (elBoundingRect.left + elBoundingRect.width < coordinates.x) match = false; // Y-axis

  if (elBoundingRect.top > coordinates.y + coordinates.height) match = false;
  if (elBoundingRect.top + elBoundingRect.height < coordinates.y) match = false;
  return match;
}
/**
 * Hash a string
 * courtesy of https://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
 * @param {String} str the String to hash
 * @returns {String} the hash 
 */

function hashCode(str) {
  var hash = 0;
  var i;
  var chr;
  if (str.length === 0) return hash;

  for (i = 0; i < str.length; i++) {
    chr = str.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0;
  }

  return hash;
}
;
// CONCATENATED MODULE: ./src/models/RawAnnotation.js



/**
 * An instance of RawAnnotation contains the values of the form
 * submitted by the analyst. It is basically an handy way to refer
 * to the raw data submitted, before Colvis classifies the annotation.
 * 
 * A RawAnnotation is composed of an observation and an optional 
 * meaning. The observation is itself subdivided into three: subjects,
 * reason, complements. An additional set of free tags can be 
 * attributed to the RawAnnotation.
 * 
 * The observation is the visual-focused or data-focused feature that
 * the analyst made during the sense making process. The meaning
 * is a tentative explanation for that feature.
 * @class
 */

var RawAnnotation_RawAnnotation =
/**
 * The subjects of the observation
 * @type {DataBinder[]}
 * @public
 */

/**
 * The name of the subjects
 * @type {string}
 * @public
 */

/**
 * The complements of the observation
 * @type {DataBinder[]|null}
 * @public
 */

/**
 * The name of the complement
 * @type {string}
 * @public
 */

/**
 * The "reason" of the observation.
 * @type {Reason}
 * @public
 */

/**
 * The meaning
 * @type {string}
 * @public
 */

/**
 * The tags of the annotation
 * @type {string[]}
 * @public
 */

/**
 * SelectionMethod
 * @type {('combobox', 'selector')[]} 
 */

/**
 * Textual representation of the annotation
 */

/**
 * The state of the visualization
 */

/**
 * the annotation being replied to
 */

/**
 * the type of reply, CommentSpace-like
 */

/**
 * Constructor method of the RawAnnotation class
 * @param {Object} raw - The raw values of the form
 * @param {DataBinder[]} raw.subjects - The subjects of the annotation
 * @param {DataBinder[]} raw.complements - The complements of the annotation 
 * @param {Object} raw.reason - The reason 
 * @param {string} raw.meaning - The meaning 
 * @param {string[]} raw.tags - The tags of the annotation
 * @param {Object} raw.state - The state of the visualization
 * @param {('combobox'|'selector')[]} raw.selectionMethods - the methods used
 */
function RawAnnotation(_ref) {
  var subjects = _ref.subjects,
      subjectName = _ref.subjectName,
      complements = _ref.complements,
      complementName = _ref.complementName,
      reason = _ref.reason,
      meaning = _ref.meaning,
      tags = _ref.tags,
      selectionMethods = _ref.selectionMethods,
      specs = _ref.specs,
      ci = _ref.ci,
      state = _ref.state,
      replyTo = _ref.replyTo,
      replyType = _ref.replyType;

  _classCallCheck(this, RawAnnotation);

  _defineProperty(this, "subjects", void 0);

  _defineProperty(this, "subjectName", void 0);

  _defineProperty(this, "complements", void 0);

  _defineProperty(this, "complementName", void 0);

  _defineProperty(this, "reason", void 0);

  _defineProperty(this, "meaning", void 0);

  _defineProperty(this, "tags", void 0);

  _defineProperty(this, "selectionMethods", []);

  _defineProperty(this, "text", void 0);

  _defineProperty(this, "state", void 0);

  _defineProperty(this, "replyTo", void 0);

  _defineProperty(this, "replyType", void 0);

  this.subjects = subjects;
  this.subjectName = subjectName || (this.subjects.length === 1 ? this.subjects[0].title : null);
  this.complements = complements;
  this.complementName = complementName || (this.complements.length === 1 ? this.complements[0].title : null);
  this.reason = reason;
  this.meaning = meaning;
  this.tags = tags;
  this.selectionMethods = selectionMethods;
  this.state = state;
  this.replyTo = replyTo;
  this.replyType = replyType;
  var subList = ci.getFilteredList(subjects, specs);
  var comList = ci.getFilteredList(complements, specs);

  if (!this.subjectName && this.subjects.length > 1) {
    var tSubList = subList.displayed.map(function (s) {
      return ci.getEntityTitle(s, specs);
    });
    if (subList.hidden.length) tSubList = tSubList.concat(["and ".concat(subList.hidden.length, " others")]);
    this.subjectName = tSubList.join(', ');
  }

  if (!this.complementName && this.complements.length > 1) {
    var tComList = comList.displayed.map(function (c) {
      return ci.getEntityTitle(c, specs);
    });
    if (comList.hidden.length) tComList = tComList.concat(["and ".concat(comList.hidden.length, " others")]);
    this.complementName = tComList.join(', ');
  }

  if (this.subjectName && (reason.verb || reason.details)) {
    this.text = this.subjectName;
    this.text += " ".concat(reason.verb.text);
    if (this.complementName) this.text += " ".concat(this.complementName);
    if (reason.details) this.text += " ".concat(reason.details);
    this.text += '.';
    if (meaning) this.text += " ".concat(meaning, ".");
  }
};


// CONCATENATED MODULE: ./src/models/Annotation/DataUnit.js




/**
 * A Data Unit is a "piece" of the overall data that the analyst
 * speaks about in an annotation. For more information about Data
 * Units, please refer to Colvis' documentation.
 * @class
 */
var DataUnit_DataUnit =
/**
 * The role of the Data Unit
 * @type {('complement'|'subject'|'explanation')}
 * @public
 */

/**
 * The id corresponding to the DataBinder's.
 * @type {string}
 * @public
 */

/**
 * The id of nature of the Data Unit.
 * @type {string}
 * @public
 */

/**
 * Whether the data unit is aggregated or single.
 * @type {boolean}
 * @public
 */

/**
 * Single units composing the aggregated data unit,
 * if aggregated. 
 * @type {DataUnit[]}
 * @public
 */

/**
 * The title of the Data Unit.
 * @type {string|null}
 * @public 
 */

/**
 * Constructor method for the Data Unit class
 * @param {Object} unit - The Data Unit
 * @param {('complement'|'subject'|'explanation')} role - the role
 * @param {string} id - an id corresponding to a DataBinder's
 * @param {string} natureId - the nature
 */
function DataUnit(_ref) {
  var _this$units;

  var role = _ref.role,
      id = _ref.id,
      natureId = _ref.natureId,
      aggregated = _ref.aggregated,
      units = _ref.units,
      title = _ref.title;

  _classCallCheck(this, DataUnit);

  _defineProperty(this, "role", void 0);

  _defineProperty(this, "id", void 0);

  _defineProperty(this, "natureId", void 0);

  _defineProperty(this, "aggregated", false);

  _defineProperty(this, "units", []);

  _defineProperty(this, "title", null);

  this.role = role;
  this.id = id;
  this.natureId = natureId;
  this.aggregated = aggregated ? true : false;
  if (units) (_this$units = this.units).push.apply(_this$units, _toConsumableArray(units));
  if (title) this.title = title;
};


// CONCATENATED MODULE: ./src/models/Annotation/Loi.js



/**
 * Loi - or Level of Interpretation in the classification system - is the depth 
 * of observations made by the analyst, as observable in the annotation.
 * We distinguish three possible levels:
 *  - Visual
 *  - Data
 *  - Meaning
 * For more about Levels of Interpretation, please refer to the Colvis' documentation.
 * @class
 */
var Loi_Loi =
/** 
 * Visual value of the level of interpretation. 
 * @type {(boolean}
 * @public
 */

/** 
 * Data value of the level of interpretation. 
 * @type {boolean}
 * @public
 */

/** 
 * Meaning value of the level of interpretation. 
 * @type {string|(false)}
 * @public
 */

/**
 * Constructor method of the Pattern class. 
 * @param {Object} loiValue - The pattern value
 * @param {boolean} loiValue.visual - The visual value 
 * @param {boolean} loiValue.data - The data value 
 * @param {string|(false)} loiValue.meaning - The meaning value 
 */
function Loi(_ref) {
  var visual = _ref.visual,
      data = _ref.data,
      meaning = _ref.meaning;

  _classCallCheck(this, Loi);

  _defineProperty(this, "visual", void 0);

  _defineProperty(this, "data", void 0);

  _defineProperty(this, "meaning", void 0);

  this.visual = visual;
  this.data = data;
  this.meaning = meaning;
};


// CONCATENATED MODULE: ./src/models/Annotation/Pattern.js



/**
 * Pattern - or Detected Pattern in the classification system - is a 
 * type of observations made by the analyst in the annotation.
 * We distinguish three possible patterns:
 *  - Singularity, either explicit or implicit
 *  - Duality
 *  - Generality
 * For more about Detected Patterns, please refer to the Colvis' documentation.
 * @class
 */
var Pattern_Pattern =
/** 
 * Singularity value of the detected pattern. "true" value is considered
 * as explicit by default.
 * @type {('explicit'|'implicit')|boolean}
 * @public
 */

/** 
 * Duality value of the detected pattern. 
 * @type {boolean}
 * @public
 */

/** 
 * Generality value of the detected pattern. 
 * @type {boolean}
 * @public
 */

/**
 * Constructor method of the Pattern class. 
 * @param {Object} patternValue - The pattern value
 * @param {('explicit'|'implicit')|boolean} patternValue.singularity - The singularity value 
 * @param {boolean} patternValue.duality - The duality value 
 * @param {boolean} patternValue.generality - The generality value 
 */
function Pattern(_ref) {
  var subjects = _ref.subjects,
      complements = _ref.complements,
      reason = _ref.reason,
      specs = _ref.specs;

  _classCallCheck(this, Pattern);

  _defineProperty(this, "singularity", void 0);

  _defineProperty(this, "duality", void 0);

  _defineProperty(this, "generality", void 0);

  var nature = specs.natures.filter(function (n) {
    return n.id === subjects[0].natureId;
  })[0];
  var ratioSubjectToTotal = subjects.length / nature.list.length;

  if (reason.verb.id === 'sim') {
    this.singularity = false;
    this.duality = true;
    this.generality = false;
  } else {
    if (complements.length && (subjects.length / complements.length < .5 || subjects.length / complements.length >= 2)) {
      this.singularity = 'explicit';
    } else if (!complements.length) {
      this.singularity = 'implicit';
    } else {
      this.singularity = false;
    }

    this.duality = !!complements.length && subjects.length / complements.length >= .5 && subjects.length / complements.length < 2;
    this.generality = ratioSubjectToTotal > .8;
  }
};


// CONCATENATED MODULE: ./src/models/Annotation/NotableFeature.js



/** 
 * A NotableFeature explains why the subject data unit is worth
 * annotating. It consists of a textual verb, and two sets of 
 * features: data-related features and visual-related features.
 * 
 * Data-related features 
 * 
 * Visual-related features are insights about graphical 
 * points of interest: position of the subject, color of
 * the subject, etc.
 */
var NotableFeature_NotableFeature =
/**
 * Sentence used to make the annotation.
 * @type {('stands out'|'share similar'|'is similar to')}
 * @public
 */

/**
 * @type {DataFeature[]}
 * @public
 */

/**
 * @type {VisualFeature[]}
 * @public
 */

/**
 * @param {Object} settings
 * @param {Object[]} settings.dataFeatures
 * @param {Object[]} settings.visualFeatures 
 */
function NotableFeature(_ref) {
  var verb = _ref.verb,
      dataFeatures = _ref.dataFeatures,
      visualFeatures = _ref.visualFeatures;

  _classCallCheck(this, NotableFeature);

  _defineProperty(this, "verb", void 0);

  _defineProperty(this, "dataFeatures", []);

  _defineProperty(this, "visualFeatures", []);

  this.verb = verb;
  this.dataFeatures = dataFeatures.map(function (f) {
    return new NotableFeature_DataFeature(f);
  });
  this.visualFeatures = visualFeatures.map(function (f) {
    return new NotableFeature_VisualFeature(f);
  });
};
/**
 * A DataFeature is an insight about the product of
 * a combination between two data units (the subject and the
 * explanatory data units). 
 * 
 * Example: a product of Jean Valjean and Chapter 3 is
 * the frequency of appearance of the said character
 * for the said chapter. An annotation's DataFeature
 * could be high frequency of appearance for Jean Valjean
 * and Chapter 3.
 */



var NotableFeature_DataFeature =
/**
 * Subject data unit
 * @type {string}
 * @public
 */

/**
 * Explanatory data unit
 * @type {string}
 * @public
 */

/**
 * The Identifier of the product you speak about
 * @type {string}
 */

/**
 * The description of the product.
 * "low" and "high" are extrema. "unusual" denotes
 * an outlier.
 * 
 * While seemingly less interesting,
 * "average" can be used in conjuction with
 * another feature to point out a singularity.
 * Example: "this character has an average level
 * of intellect but a high social skill",
 * probably implying that most characters
 * have similarly high intellect and social
 * skill. 
 * @type {('low'|'average'|'high'|'unusual')}
 */
function DataFeature(_ref2) {
  var subject = _ref2.subject,
      explanation = _ref2.explanation,
      product = _ref2.product,
      value = _ref2.value;

  _classCallCheck(this, DataFeature);

  _defineProperty(this, "subjectId", void 0);

  _defineProperty(this, "explanationId", void 0);

  _defineProperty(this, "productId", void 0);

  _defineProperty(this, "value", void 0);

  this.subjectId = subject.id;
  this.explanationId = explanation[0].id;
  this.productId = product[0].id;
  this.value = value;
};
/**
 * A VisualFeature is an insight about the visual
 * elements of the visualization, without refering
 * the data. It thus refers to the traditional 
 * visual channels used to encode data.
 */

var NotableFeature_VisualFeature = function VisualFeature() {
  _classCallCheck(this, VisualFeature);

  _defineProperty(this, "value", void 0);
};
// CONCATENATED MODULE: ./src/models/Annotation/index.js












function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }







/**
 * The concept of Annotation is at the heart of Colvis. An annotation 
 * embodies one or several insights fostered by the visualizations and 
 * left by an analyst, either for herself (to support the sense-making 
 * process) or for others (to support collaborative analysis of the 
 * data visualization).
 * 
 * Please refer to the Colvis' documentation for more about Annotations.
 */

var Annotation_Annotation = /*#__PURE__*/function () {
  /**
   * The list of data units that this annotation speaks of
   * @type {DataUnit[]}
   * @public
   */

  /**
   * The Level of Interpretation of the annotation
   * @type {Loi}
   * @public
   */

  /**
   * The Detected Patterns of the annotation
   * @type {Pattern}
   * @public
   */

  /**
   * The raw values from the form
   * @type {RawAnnotation}
   * @public
   */

  /**
   * List of tags
   * @type {String[]}
   * @public
   */

  /**
   * Type of reply, if any
   * @type {Boolean}
   * @public
   */

  /**
   * Meta information regarding the annotation
   * @type {Meta}
   * @public
   */

  /**
   * 
   * @param {Object} settings
   * @param {RawAnnotation} rawAnnotation - The Raw Annotation from which the annotation is computed 
   * @param {Specs} specs - specifications of this visualization
   */
  function Annotation(_ref) {
    var _this = this;

    var rawAnnotation = _ref.rawAnnotation,
        specs = _ref.specs;

    _classCallCheck(this, Annotation);

    _defineProperty(this, "dataUnits", []);

    _defineProperty(this, "loi", void 0);

    _defineProperty(this, "patterns", void 0);

    _defineProperty(this, "rawAnnotation", void 0);

    _defineProperty(this, "tags", void 0);

    _defineProperty(this, "replyType", void 0);

    _defineProperty(this, "meta", void 0);

    // Process all subject data units
    rawAnnotation.subjects.forEach(function (subject) {
      _this.dataUnits.push(new DataUnit_DataUnit({
        role: 'subject',
        id: subject.id,
        natureId: subject.natureId,
        title: subject.title
      }));
    });
    rawAnnotation.complements.forEach(function (complement) {
      _this.dataUnits.push(new DataUnit_DataUnit({
        role: 'complement',
        id: complement.id,
        natureId: complement.natureId,
        title: complement.title
      }));
    });
    var subjects = this.dataUnits.filter(function (d) {
      return d.role === 'subject';
    });
    var complements = this.dataUnits.filter(function (d) {
      return d.role === 'complement';
    });
    var aggregatedSubjectDataUnit;
    var aggregatedComplementDataUnit;

    if (subjects.length > 1) {
      aggregatedSubjectDataUnit = new DataUnit_DataUnit({
        role: 'subject',
        id: hashCode(strfy(subjects.map(function (s) {
          return s.id;
        })) + rawAnnotation.subjectName),
        natureId: subjects[0].natureId,
        title: rawAnnotation.subjectName,
        units: subjects,
        aggregated: true
      });
    }

    if (complements.length > 1) {
      aggregatedComplementDataUnit = new DataUnit_DataUnit({
        role: 'complement',
        id: hashCode(strfy(complements.map(function (c) {
          return c.id;
        })) + rawAnnotation.complementName),
        natureId: complements[0].natureId,
        title: rawAnnotation.complementName,
        units: complements,
        aggregated: true
      });
    }

    if (aggregatedSubjectDataUnit || aggregatedComplementDataUnit) {
      if (aggregatedSubjectDataUnit) {
        this.dataUnits = this.dataUnits.filter(function (d) {
          return d.role !== 'subject';
        });
        this.dataUnits.push(aggregatedSubjectDataUnit);
      }

      if (aggregatedComplementDataUnit) {
        this.dataUnits = this.dataUnits.filter(function (d) {
          return d.role !== 'complement';
        });
        this.dataUnits.push(aggregatedComplementDataUnit);
      }
    }
    /*this.notableFeatures = new NotableFeature(rawAnnotation.reason);
     this.notableFeatures.dataFeatures.forEach(feat => {
        if (feat.explanationId) {
            const explanation = [].concat(...specs.natures.map(n => n.list)).filter(e => e.id === feat.explanationId)[0];
            this.dataUnits.push(new DataUnit({ role: 'explanation', id: explanation.id, natureId: explanation.natureId }));
        }
    });*/


    this.loi = new Loi_Loi({
      visual: rawAnnotation.selectionMethods.includes('selector'),
      data: rawAnnotation.selectionMethods.includes('combobox'),
      meaning: !!rawAnnotation.meaning
    });
    this.patterns = new Pattern_Pattern({
      subjects: rawAnnotation.subjects,
      complements: rawAnnotation.complements,
      reason: rawAnnotation.reason,
      specs: specs
    });
    this.rawAnnotation = _objectSpread({}, rawAnnotation);
    this.rawAnnotation.subjects = this.rawAnnotation.subjects.map(function (s) {
      var domElement = s.domElement,
          subject = _objectWithoutProperties(s, ["domElement"]);

      return subject;
    });
    this.rawAnnotation.complements = this.rawAnnotation.complements.map(function (c) {
      var domElement = c.domElement,
          complement = _objectWithoutProperties(c, ["domElement"]);

      return complement;
    });
    this.tags = this.rawAnnotation.tags;
    this.replyType = this.rawAnnotation.replyType;
    this.replyTo = this.rawAnnotation.replyTo;
  }
  /**
   * Recursively remove all null properties on an object. 
   * Used to save memory upon saving the annotation.
   * @param {Object} obj the object to clean
   */


  _createClass(Annotation, [{
    key: "deleteNullsProps",
    value: function deleteNullsProps() {
      var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this;
      var filter = arguments.length > 1 ? arguments[1] : undefined;

      for (var prop in obj) {
        if (obj[prop] === null && prop !== filter) {
          delete obj[prop];
        } else if (typeof_typeof(obj[prop]) === 'object') {
          this.deleteNullsProps(this[prop]);
        }
      }
    }
    /**
     * 
     * @param {Object} ci - Colvis instance
     */

  }, {
    key: "setMeta",
    value: function setMeta(ci) {
      this.meta = new Annotation_Meta(ci);
    }
  }]);

  return Annotation;
}();
/**
 * Meta information regarding the annotation
 */



var Annotation_Meta =
/** time at which the annotation is created. Useful to keep track of the succession of annotations */

/** hashes of the specs and binders */

/** state of the visualization */
function Meta(ci) {
  _classCallCheck(this, Meta);

  _defineProperty(this, "timestamp", void 0);

  _defineProperty(this, "hashes", void 0);

  _defineProperty(this, "state", void 0);

  this.timestamp = Date.now();
  this.state = ci.getVizState();
  this.hashes = {
    specs: ci.getSpecsHash(),
    entities: ci.getEntitiesHash()
  };
};
// CONCATENATED MODULE: ./src/models/DataBinder.js



/**
 * An object that links data and dom elements to which the data are bound.
 * @class
 */
var DataBinder_DataBinder =
/**
 * The unique identifier of the object, used to retrieve it
 * across Colvis.
 * @type {string}
 * @public
 */

/**
 * The title of the object, to be displayed for human-readable text
 * @type {string}
 * @public
 */

/**
 * The data object itself
 * @type {Object}
 * @public
 */

/**
 * The dom element created with the data.
 * @type {HTMLElement}
 * @public
 */

/**
 * The ID of the Nature that the DataBinder belongs to
 * @param {string} natureId 
 * @public
 */

/**
 * Creates a DataBinder
 * @param {Object} settings - the settings of the DataBinder
 * @param {string} settings.id - the identifier of the object, usually an id
 * @param {Object} settings.obj an object containing the data
 * @param {Object} settings.natureId the identifier of the Nature
 * @param {HTMLElement} settings.domElement the dom element created with the data
 */
function DataBinder(_ref) {
  var id = _ref.id,
      title = _ref.title,
      obj = _ref.obj,
      natureId = _ref.natureId,
      domElement = _ref.domElement;

  _classCallCheck(this, DataBinder);

  _defineProperty(this, "id", void 0);

  _defineProperty(this, "title", void 0);

  _defineProperty(this, "obj", void 0);

  _defineProperty(this, "domElement", void 0);

  _defineProperty(this, "natureId", void 0);

  this.id = id;
  this.title = title;
  this.obj = obj;
  this.natureId = natureId;
  this.domElement = domElement;
};


// CONCATENATED MODULE: ./src/models/Specs/Combination.js



/**
 * A Combination encompasses all possible forms that
 * a combination between two Natures can take. It is
 * thus defined by two Natures and "products" (see dedicated
 * class below).: several
 * annotables properties that are expressed as JavaScript's 
 * primitives. Most properties are Numbers, that can be 
 * rougly described as "low", "average" or "high". 
 * There is one generic annotable, "count", that represents 
 * the number of connections between a given data 
 * unit of a Nature and all other data units of the 
 * combined Nature.
 * 
 * For example: the combination between the "Character"
 * and the "Chapter" Natures produce two annotables:
 * "count" represents the number of chapters in which
 * a character appears and "frequency of apparition" is 
 * a Number that can be assessed as "low", "average" or "high".
 */
var Combination_Combination =
/**
 * First nature of the combination
 * @type {Nature}
 * @public
 */

/**
 * Second nature of the combination
 * @type {Nature}
 * @public
 */

/**
 * The possible results of the combinations.
 * @type {(('count')|Product)[]}
 * @public
 */

/**
 * Constructor method for the Combination class
 * @param {Object} settings
 * @param {Nature} first
 * @param {Nature} second
 * @param {(('count')|Product)[]} product
 */
function Combination(_ref) {
  var first = _ref.first,
      second = _ref.second,
      products = _ref.products;

  _classCallCheck(this, Combination);

  _defineProperty(this, "first", void 0);

  _defineProperty(this, "second", void 0);

  _defineProperty(this, "products", void 0);

  this.first = first;
  this.second = second;
  this.products = products;
};
/**
 * A Product is a property that belongs to a 
 * Combination and can be annotated. It is 
 * expressed as a type, a range and a type of
 * range.
 */



var Combination_Product =
/**
 * The Identifier of the Product
 * @type {string}
 * @public
 */

/**
 * Type of the Product. Must
 * be a Javascript's global
 * Object representing a primitive.
 * Example: String
 * 
 * @type {String|Boolean|Number}
 * @public
 */

/**
 * The type of range to use.
 * @type {('ordinal'|'linear')}
 * @public
 */

/**
 * The range itself.
 * @type {Any[]}
 * @public
 */

/**
 * Constructor method for the Product class
 * @param {Object} settings 
 * @param {Object} type 
 * @param {('ordinal'|'linear')} rangeType
 * @param {Any[]} range
 */
function Product(_ref2) {
  var type = _ref2.type,
      rangeType = _ref2.rangeType,
      range = _ref2.range;

  _classCallCheck(this, Product);

  _defineProperty(this, "id", void 0);

  _defineProperty(this, "type", void 0);

  _defineProperty(this, "rangeType", void 0);

  _defineProperty(this, "range", void 0);

  this.type = type;
  this.rangeType = rangeType;
  this.range = range;
};
// CONCATENATED MODULE: ./src/models/Specs/Nature.js



/**
 * A Nature is a "type of entities" that can be found in data
 * visualization.
 * Providing a list of possible nature allows us to know what can
 * be annotated and what are the relations between each nature
 * within a dataset. When two data units of different nature are
 * used within an annotation, we expect the combination to be
 * the reason why the subject data unit is being annotated. For
 * more information, please refer to the Combination and the Reason 
 * models, or check Colvis' documentation.
 * 
 * Example: "Jean Valjean appears more than other characters at 
 * chapter 3". "Jean Valjean" and "other characters" both belong 
 * to the same nature, "character", while "chapter 3" belongs
 * to the "chapter" nature. "Jean Valjean" is the subject,
 * "other characters" are a comparison mean (complement),
 * and the combination of the subject and "chapter 3", "number of appearance",
 * is implicitly the reason why Jean Valjean is being annotated.
 * Possible combinations are limitless, and must be explicitely
 * defined in the Colvis specifications.
 */
var Nature_Nature =
/**
 * The identifier of the nature. 
 * Example: "character", "chapter", etc.
 * @type {string}
 * @public
 */

/**
 * Whether this nature can be annotated.
 * @type {Annotable|(false)}
 * @public
 */

/**
 * The list of entities belonging to this nature.
 * @type {Object[]}
 */

/**
 * Constructor method for the Nature class
 * @param {Object} settings - the settings
 * @param {string|(false)} annotable - a CSS selector string to select the annotable elements, or false for not annotable
 * @param {Object[]} list - the list of entities
 */
function Nature(_ref) {
  var id = _ref.id,
      annotable = _ref.annotable,
      list = _ref.list;

  _classCallCheck(this, Nature);

  _defineProperty(this, "id", void 0);

  _defineProperty(this, "annotable", void 0);

  _defineProperty(this, "list", []);

  this.id = id;
  this.annotable = annotable ? new Nature_Annotable({
    selector: annotable
  }) : false;
  this.list = list;
};
/**
 * Annotable is an (overengineered) class to determine whether
 * a Nature can be annotated and, if so, what is the CSS selector
 * to search the relevant DOM Elements (those created by this nature).
 */



var Nature_Annotable =
/**
 * The CSS selector to find the annotable elements
 * belonging to the parent nature.
 * @type {string}
 * @public
 */

/**
 * Constructor method for the Annotable class
 * @param {Object} settings - the settings
 * @param {string} selector - the CSS selector to select the annotable elements
 */
function Annotable(_ref2) {
  var selector = _ref2.selector;

  _classCallCheck(this, Annotable);

  _defineProperty(this, "selector", void 0);

  this.selector = selector;
};
// CONCATENATED MODULE: ./src/models/Specs/index.js


/* harmony default export */ var Specs = ({
  Combination: Combination_Combination,
  Nature: Nature_Nature
});
// CONCATENATED MODULE: ./src/models/index.js
/**
 * All classes (models) that implement Colvis
 * are exported throught this file. These models
 * are direct implementation of a theoritical
 * framework and might seem somewhat overengineered.
 * 
 * For more information about these models, please
 * refer the Colvis' documentation.
 */





// CONCATENATED MODULE: ./src/models/ColvisMessage.js



var ColvisMessage_ColvisMessage = function ColvisMessage(type, content) {
  _classCallCheck(this, ColvisMessage);

  _defineProperty(this, "content", void 0);

  _defineProperty(this, "type", void 0);

  this.type = type;
  this.content = content;
};


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CAutocomplete.vue?vue&type=template&id=28ae3ced&scoped=true&lang=pug&
var CAutocompletevue_type_template_id_28ae3ced_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('fieldset',{on:{"click":function($event){$event.stopPropagation();_vm.listModal = true}}},[_c('header',{staticClass:"legend"},[(_vm.label)?_c('label',{attrs:{"for":_vm.id}},[_vm._v(_vm._s(_vm.label))]):_vm._e(),(_vm.filteredSelection.displayed.length)?_c('small',{staticClass:"clear-selection",on:{"click":function($event){return _vm.$emit('input', [])}}},[_vm._v("x Clear selection")]):_vm._e()]),_c('div',{ref:"field",staticClass:"field",on:{"click":_vm.focus}},[_c('transition-group',{attrs:{"name":"fade"}},[_vm._l((_vm.filteredSelection.displayed),function(element){return _c('c-chip',{key:element.id,attrs:{"text":_vm.$colvis.getEntityTitle(element),"closeable":""},on:{"close":function($event){return _vm.toggleItem(element)}}})}),(_vm.filteredSelection.hidden.length)?_c('c-chip',{key:"colvisOthers",attrs:{"text":("..." + (_vm.filteredSelection.hidden.length) + " others")}}):_vm._e()],2),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.searchString),expression:"searchString"}],ref:"fieldInput",attrs:{"id":_vm.id,"type":"text","placeholder":"search for an entry","autofill":"off","autocomplete":"off"},domProps:{"value":(_vm.searchString)},on:{"keydown":[function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"backspace",undefined,$event.key,undefined)){ return null; }return _vm.checkEntry($event)},function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"enter",13,$event.key,"Enter")){ return null; }$event.preventDefault();return _vm.checkEntry($event)}],"input":function($event){if($event.target.composing){ return; }_vm.searchString=$event.target.value}}})],1),(_vm.listModal && !_vm.$colvis.getOptions().autocomplete.group)?_c('ul',{ref:"list",staticClass:"list",style:({ width: ((_vm.$refs.field.clientWidth) + "px") })},_vm._l((_vm.filteredItems),function(item,index){return _c('li',{key:item.id,class:{ first: index === 0, selected: _vm.indexOfSelected(item) !== -1 },attrs:{"title":_vm.$colvis.getEntityTitle(item)},on:{"click":function($event){return _vm.toggleItem(item)}}},[_vm._v(_vm._s(_vm.$colvis.getEntityTitle(item)))])}),0):_vm._e(),_c('transition',{attrs:{"name":"slide-down-fade"}},[(_vm.listModal && _vm.$colvis.getOptions().autocomplete.group)?_c('ul',{ref:"listStructured",staticClass:"list--structured",style:({ width: ((_vm.$refs.field.clientWidth) + "px") })},_vm._l((_vm.filteredItems),function(item,index){return _c('li',{key:item.id,class:{ first: index === 0, selected: _vm.indexOfSelected(item) !== -1 },attrs:{"title":_vm.$colvis.getEntityTitle(item)}},[_c('i',[_vm._v("close")]),_c('span',[_vm._v(_vm._s(_vm.$colvis.getEntityTitle(item)))]),_c('ul',{staticClass:"list__secondlevel"},_vm._l((item.natures),function(nature){return _c('li',{key:nature.name},[_c('i',[_vm._v("close")]),_c('span',[_vm._v(_vm._s(nature.name))]),_c('ul',{staticClass:"list__thirdlevel"},_vm._l((nature.list),function(item){return _c('li',{key:item.id,class:{ selected: _vm.indexOfSelected(item) !== -1 },on:{"click":function($event){return _vm.toggleItem(item)}}},[_vm._v(_vm._s(_vm.$colvis.getEntityTitle(item))+" ")])}),0)])}),0)])}),0):_vm._e()])],1)}
var CAutocompletevue_type_template_id_28ae3ced_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_inputs/CAutocomplete.vue?vue&type=template&id=28ae3ced&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CChip.vue?vue&type=template&id=4ac2cfc0&scoped=true&lang=pug&
var CChipvue_type_template_id_4ac2cfc0_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',{staticClass:"chip",class:{ inactive: _vm.inactive, small: _vm.small },on:{"mouseover":function($event){return _vm.$emit('mouseover', $event)},"mouseout":function($event){return _vm.$emit('mouseout', $event)},"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_vm._t("prepend"),(_vm.text)?_c('span',[_vm._v(_vm._s(_vm.text))]):_vm._e(),_vm._t("default"),_vm._t("append"),(_vm.closeable)?_c('small',{staticClass:"chip__button",on:{"click":function($event){return _vm.$emit('close')}}},[_vm._v("x")]):_vm._e()],2)}
var CChipvue_type_template_id_4ac2cfc0_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/CChip.vue?vue&type=template&id=4ac2cfc0&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CChip.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
/* harmony default export */ var CChipvue_type_script_lang_js_ = ({
  props: {
    text: {
      type: String
    },
    closeable: {
      type: Boolean,
      default: false
    },
    inactive: {
      type: Boolean,
      default: false
    },
    small: {
      type: Boolean,
      default: false
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/CChip.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_CChipvue_type_script_lang_js_ = (CChipvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_generic/CChip.vue?vue&type=style&index=0&id=4ac2cfc0&lang=sass&scoped=true&
var CChipvue_type_style_index_0_id_4ac2cfc0_lang_sass_scoped_true_ = __webpack_require__("5b5a");

// CONCATENATED MODULE: ./src/components/_generic/CChip.vue






/* normalize component */

var CChip_component = normalizeComponent(
  _generic_CChipvue_type_script_lang_js_,
  CChipvue_type_template_id_4ac2cfc0_scoped_true_lang_pug_render,
  CChipvue_type_template_id_4ac2cfc0_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "4ac2cfc0",
  null
  
)

/* harmony default export */ var CChip = (CChip_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CAutocomplete.vue?vue&type=script&lang=js&




//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var CAutocompletevue_type_script_lang_js_ = ({
  components: {
    CChip: CChip
  },
  props: {
    items: {
      type: Array,
      required: true
    },
    id: {
      type: String
    },
    value: {
      type: Array
    },
    label: {
      type: String
    }
  },
  data: function data() {
    return {
      listModal: false,
      selected: [],
      searchString: null,
      eventListeners: []
    };
  },
  computed: {
    filteredItems: function filteredItems() {
      var _this = this;

      var items;
      var g = this.$colvis.getOptions().autocomplete.group;
      if (!this.searchString) items = this.items;else {
        items = this.items.filter(function (item) {
          return "".concat(_this.$colvis.getEntityTitle(item)).toLowerCase().indexOf(_this.searchString.toLowerCase()) !== -1;
        });
      }

      if (!g) {
        return items;
      } else {
        var _ref;

        var entities = this.$colvis.getEntities();
        var firstLevel = entities.filter(function (item) {
          return item.natureId === g;
        });
        var combinations = this.$colvis.getCombinationsWhereParent(g);
        combinations.forEach(function (c) {
          c.productsItems = c.products.map(function (p) {
            return {
              id: p,
              combination: c
            };
          });
        });

        var products = (_ref = []).concat.apply(_ref, _toConsumableArray(combinations.map(function (c) {
          return c.productsItems;
        })));

        firstLevel.forEach(function (l) {
          l.natures = _toConsumableArray(products.map(function (p) {
            var n = _this.$colvis.getNature(p.id);

            var field = p.combination.groupedSelection[l.natureId];
            var list = n.list.filter(function (e) {
              return e.obj[field] === l.id;
            });
            return {
              id: p.id,
              name: _this.$colvis.getNatureName(p.id),
              list: list
            };
          }));
        });
        return firstLevel;
      }
    },
    filteredSelection: function filteredSelection() {
      return this.$colvis.getFilteredList(this.value);
    }
  },
  methods: {
    toggleItem: function toggleItem(item) {
      var selected = this.indexOfSelected(item);

      if (selected === -1) {
        this.$emit('input', [item].concat(this.value));
      } else {
        var items = _toConsumableArray(this.value);

        items.splice(selected, 1);
        this.$emit('input', items);
      }
    },
    indexOfSelected: function indexOfSelected(item) {
      var items = this.value.map(function (i) {
        return i.id;
      });
      return items.indexOf(item.id);
    },
    focus: function focus() {
      this.$refs.fieldInput.focus();
    },
    checkEntry: function checkEntry(event) {
      if (event.key === 'Enter') {
        if (this.filteredItems.length) this.toggleItem(this.filteredItems[0]);
        this.searchString = '';
      } else if (event.key === 'Backspace') {
        if (!this.searchString) this.$emit('input', this.value.slice(0, -1));
      }
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    var fn = function fn(c) {
      function checkEqual(ref, target) {
        if (ref === target) return true;
        if (!target.parentNode) return false;
        return checkEqual(ref, target.parentNode);
      }

      if (_this2.listModal && !checkEqual(_this2.$refs.list, c.target)) {
        _this2.listModal = false;
      }
    };

    addEventListener('click', fn);
    this.eventListeners.push({
      click: fn
    });
  },
  destroyed: function destroyed() {
    this.eventListeners.forEach(function (e) {
      Object.keys(e).forEach(function (key) {
        removeEventListener(key, e[key]);
      });
    });
  }
});
// CONCATENATED MODULE: ./src/components/_inputs/CAutocomplete.vue?vue&type=script&lang=js&
 /* harmony default export */ var _inputs_CAutocompletevue_type_script_lang_js_ = (CAutocompletevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_inputs/CAutocomplete.vue?vue&type=style&index=0&id=28ae3ced&lang=sass&scoped=true&
var CAutocompletevue_type_style_index_0_id_28ae3ced_lang_sass_scoped_true_ = __webpack_require__("9faa");

// CONCATENATED MODULE: ./src/components/_inputs/CAutocomplete.vue






/* normalize component */

var CAutocomplete_component = normalizeComponent(
  _inputs_CAutocompletevue_type_script_lang_js_,
  CAutocompletevue_type_template_id_28ae3ced_scoped_true_lang_pug_render,
  CAutocompletevue_type_template_id_28ae3ced_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "28ae3ced",
  null
  
)

/* harmony default export */ var CAutocomplete = (CAutocomplete_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CSuggestion.vue?vue&type=template&id=2a39c370&scoped=true&lang=pug&
var CSuggestionvue_type_template_id_2a39c370_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('fieldset',[_c('legend',[_vm._v("Suggestions")]),_c('ul',{staticClass:"suggestions"},_vm._l((_vm.suggestions),function(suggestion){return _c('li',{key:suggestion.unit.title,staticClass:"suggestions__item",on:{"click":function($event){return _vm.$emit('choose', suggestion.unit)}}},[_vm._v(_vm._s(suggestion.unit.title)+" "),(suggestion.text)?_c('span',[_vm._v("(textual)")]):_vm._e(),_c('span',[_vm._v("(match: "+_vm._s(Math.ceil(suggestion.matchRatio * 100))+"%)")])])}),0)])}
var CSuggestionvue_type_template_id_2a39c370_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_inputs/CSuggestion.vue?vue&type=template&id=2a39c370&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CSuggestion.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
/* harmony default export */ var CSuggestionvue_type_script_lang_js_ = ({
  props: {
    suggestions: {
      type: Array,
      required: true
    }
  }
});
// CONCATENATED MODULE: ./src/components/_inputs/CSuggestion.vue?vue&type=script&lang=js&
 /* harmony default export */ var _inputs_CSuggestionvue_type_script_lang_js_ = (CSuggestionvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_inputs/CSuggestion.vue?vue&type=style&index=0&id=2a39c370&lang=sass&scoped=true&
var CSuggestionvue_type_style_index_0_id_2a39c370_lang_sass_scoped_true_ = __webpack_require__("6e83");

// CONCATENATED MODULE: ./src/components/_inputs/CSuggestion.vue






/* normalize component */

var CSuggestion_component = normalizeComponent(
  _inputs_CSuggestionvue_type_script_lang_js_,
  CSuggestionvue_type_template_id_2a39c370_scoped_true_lang_pug_render,
  CSuggestionvue_type_template_id_2a39c370_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "2a39c370",
  null
  
)

/* harmony default export */ var CSuggestion = (CSuggestion_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CCombobox.vue?vue&type=template&id=11f59c02&scoped=true&lang=pug&
var CComboboxvue_type_template_id_11f59c02_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('fieldset',{staticClass:"combobox"},[_c('header',{staticClass:"legend"},[(_vm.label)?_c('label',{attrs:{"for":(_vm.label + "Field")}},[_vm._v(_vm._s(_vm.label))]):_vm._e(),(_vm.tags.length)?_c('small',{staticClass:"clear-selection",on:{"click":function($event){return _vm.$emit('input', [])}}},[_vm._v("x Clear selection")]):_vm._e()]),_c('div',{ref:"field",staticClass:"field",on:{"click":_vm.focus}},[_vm._l((_vm.distinctTags),function(tag){return _c('c-chip',{key:tag,attrs:{"text":tag,"closeable":""},on:{"close":function($event){return _vm.removeTag(tag)}}})}),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.incomingTag),expression:"incomingTag"}],staticClass:"combobox__input",attrs:{"id":(_vm.label + "Field"),"type":"text","label":"Tags","list":(_vm.label + "List"),"placeholder":_vm.placeholder},domProps:{"value":(_vm.incomingTag)},on:{"keydown":[function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"backspace",undefined,$event.key,undefined)){ return null; }return _vm.checkTag($event)},function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"enter",13,$event.key,"Enter")){ return null; }$event.preventDefault();return _vm.checkTag($event)}],"input":[function($event){if($event.target.composing){ return; }_vm.incomingTag=$event.target.value},_vm.checkTag],"focus":function($event){_vm.hintVisible = true},"blur":function($event){_vm.hintVisible = false}}})],2),_c('transition',{attrs:{"name":"slide-fade-down"}},[_c('footer',{directives:[{name:"show",rawName:"v-show",value:(_vm.hintVisible),expression:"hintVisible"}]},[_c('small',{staticClass:"hint"},[_vm._v(_vm._s(_vm.hint))])])]),_c('datalist',{attrs:{"id":(_vm.label + "List")}},_vm._l((_vm.items),function(item){return _c('option',{key:item},[_vm._v(_vm._s(item))])}),0)],1)}
var CComboboxvue_type_template_id_11f59c02_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_inputs/CCombobox.vue?vue&type=template&id=11f59c02&scoped=true&lang=pug&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.set.js
var es6_set = __webpack_require__("4f7f");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CCombobox.vue?vue&type=script&lang=js&







//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var CComboboxvue_type_script_lang_js_ = ({
  components: {
    CChip: CChip
  },
  props: {
    label: {
      type: String
    },
    hint: {
      type: String
    },
    placeholder: {
      type: String
    },
    value: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    items: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      tags: [],
      hintVisible: false
    };
  },
  mounted: function mounted() {
    if (this.value.length) this.tags = this.value;
  },
  computed: {
    distinctTags: function distinctTags() {
      return Array.from(new Set(this.tags));
    }
  },
  methods: {
    checkTag: function checkTag(event) {
      var _this = this;

      var push = function push(tag) {
        if (tag || _this.incomingTag) _this.tags.push(tag || _this.incomingTag);
        _this.incomingTag = null;
      };

      if (event.type === 'keydown') {
        if (event.key === 'Enter') push();else if (event.key === 'Backspace' && !this.incomingTag) this.tags.pop();
      } else if (event.type === 'input') {
        if (event.inputType === 'insertReplacementText') push(event.data);else if ([' ', ','].includes(event.data)) push(this.incomingTag.substr(0, this.incomingTag.length - 1));
      }
    },
    removeTag: function removeTag(tag) {
      var index = this.tags.indexOf(tag);
      this.tags.splice(index, 1);
    },
    focus: function focus() {
      document.querySelector("#".concat(this.label, "Field")).focus();
    }
  },
  watch: {
    distinctTags: function distinctTags() {
      this.$emit('input', this.distinctTags);
    },
    value: function value() {
      if (JSON.stringify(this.tags) !== JSON.stringify(this.value)) {
        console.log('synching tags');
        this.tags = this.value;
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/_inputs/CCombobox.vue?vue&type=script&lang=js&
 /* harmony default export */ var _inputs_CComboboxvue_type_script_lang_js_ = (CComboboxvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_inputs/CCombobox.vue?vue&type=style&index=0&id=11f59c02&lang=sass&scoped=true&
var CComboboxvue_type_style_index_0_id_11f59c02_lang_sass_scoped_true_ = __webpack_require__("1222");

// CONCATENATED MODULE: ./src/components/_inputs/CCombobox.vue






/* normalize component */

var CCombobox_component = normalizeComponent(
  _inputs_CComboboxvue_type_script_lang_js_,
  CComboboxvue_type_template_id_11f59c02_scoped_true_lang_pug_render,
  CComboboxvue_type_template_id_11f59c02_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "11f59c02",
  null
  
)

/* harmony default export */ var CCombobox = (CCombobox_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CDialog.vue?vue&type=template&id=0ddf918e&scoped=true&lang=pug&
var CDialogvue_type_template_id_0ddf918e_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.isGenerality)?_c('div',[_c('div',{staticClass:"feature"},[_c('textarea',{directives:[{name:"model",rawName:"v-model",value:(_vm.details),expression:"details"}],staticClass:"textarea",attrs:{"label":"Details","placeholder":"Describe the general trend"},domProps:{"value":(_vm.details)},on:{"input":function($event){if($event.target.composing){ return; }_vm.details=$event.target.value}}})])]):_c('div',[_vm._l((_vm.verbs),function(verb){return _c('p',{key:verb.id,staticClass:"verb",class:{ selected: verb === _vm.selectedVerb },on:{"click":function($event){return _vm.selectVerb(verb)}}},[_vm._v(_vm._s(_vm.getVerbText(verb)))])}),(_vm.selectedVerb)?_c('div',{staticClass:"feature"},[_c('textarea',{directives:[{name:"model",rawName:"v-model",value:(_vm.details),expression:"details"}],staticClass:"textarea",attrs:{"label":"Details","placeholder":"i.e.: because of its position / because of high education"},domProps:{"value":(_vm.details)},on:{"input":function($event){if($event.target.composing){ return; }_vm.details=$event.target.value}}})]):_vm._e()],2)}
var CDialogvue_type_template_id_0ddf918e_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_inputs/CDialog.vue?vue&type=template&id=0ddf918e&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CDialog.vue?vue&type=script&lang=js&










function CDialogvue_type_script_lang_js_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function CDialogvue_type_script_lang_js_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { CDialogvue_type_script_lang_js_ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { CDialogvue_type_script_lang_js_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

//
//
//
//
//
//
//
//
//
//
//v-for="combination in availableCombinations" :key="combination.first+combination.second")


var CDialogvue_type_script_lang_js_RawDataFeature = function RawDataFeature(_ref) {
  var subject = _ref.subject,
      explanation = _ref.explanation,
      product = _ref.product,
      value = _ref.value;

  _classCallCheck(this, RawDataFeature);

  _defineProperty(this, "subject", void 0);

  _defineProperty(this, "explanation", void 0);

  _defineProperty(this, "product", void 0);

  _defineProperty(this, "value", void 0);

  this.subject = subject;
  this.explanation = explanation || [];
  this.product = product || [];
  this.value = value || null;
};

/* harmony default export */ var CDialogvue_type_script_lang_js_ = ({
  components: {
    CAutocomplete: CAutocomplete
  },
  props: {
    /**
     * @type {DataBinder[]}
     */
    entities: {
      type: Array,
      required: true
    },
    subject: {
      type: Array,
      required: true
    },
    complement: {
      type: Array
    },
    combinations: {
      type: Array
    },
    isGenerality: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      selectedVerb: null,
      verbs: [{
        id: 'diff',
        singular: 'stands out',
        plural: 'stand out',
        comparison: 'from'
      }, {
        id: 'sim',
        singular: 'is similar',
        plural: 'are similar',
        comparison: 'to'
      }],
      details: ''
    };
  },
  computed: {
    availableCombinations: function availableCombinations() {
      var sn = this.subject[0].natureId;
      return this.combinations.filter(function (c) {
        return c.first === sn || c.second === sn;
      });
    },
    availableProducts: function availableProducts() {
      var _ref2, _ref3;

      return (_ref2 = []).concat.apply(_ref2, _toConsumableArray((_ref3 = []).concat.apply(_ref3, _toConsumableArray(this.availableCombinations.map(function (c) {
        return c.products;
      })))));
    },
    availableExplanations: function availableExplanations() {
      var _this = this;

      return this.entities.filter(function (e) {
        var combIds = _this.combinations.map(function (c) {
          return c.first + c.second;
        });

        var currentComb = [e.natureId + _this.subject.natureId, _this.subject.natureId + e.natureId];
        return combIds.includes(currentComb[0]) || combIds.includes(currentComb[1]);
      });
    },
    currentVerbs: function currentVerbs() {}
  },
  methods: {
    getVerbText: function getVerbText(verb) {
      var n = this.subject.length > 1 ? 'plural' : 'singular';
      var c = this.complement && this.complement.length > 0 ? " ".concat(verb.comparison) : '';
      return "".concat(verb[n]).concat(c);
    },
    selectVerb: function selectVerb(verb) {
      if (verb === this.selectedVerb) this.selectedVerb = null;else this.selectedVerb = verb;
    },
    selectValue: function selectValue(feature, value) {
      if (value === feature.value) feature.value = null;else feature.value = value;
    },
    sendSelection: function sendSelection() {
      var verb = CDialogvue_type_script_lang_js_objectSpread({}, this.selectedVerb);

      verb.text = this.getVerbText(verb);
      this.$emit('selection', {
        verb: verb,
        details: this.details,
        dataFeatures: this.dataFeatures,
        visualFeatures: this.visualFeatures
      });
    },
    addFeature: function addFeature() {
      this.dataFeatures.push(new CDialogvue_type_script_lang_js_RawDataFeature({
        subject: this.subject
      }));
    },
    getEntriesForProduct: function getEntriesForProduct(product) {
      // Find the combination and take the second nature
      var natureId = this.availableCombinations.filter(function (c) {
        return c.products.filter(function (p) {
          return p.id === product.id;
        })[0];
      })[0].second; // Find all entries belonging to this nature

      return this.entities.filter(function (e) {
        return e.natureId === natureId;
      });
    }
  },
  watch: {
    selectedVerb: function selectedVerb() {
      this.sendSelection();
    },
    details: function details() {
      this.sendSelection();
    },
    selectedAttribute: function selectedAttribute() {
      this.sendSelection();
    }
  }
});
// CONCATENATED MODULE: ./src/components/_inputs/CDialog.vue?vue&type=script&lang=js&
 /* harmony default export */ var _inputs_CDialogvue_type_script_lang_js_ = (CDialogvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_inputs/CDialog.vue?vue&type=style&index=0&id=0ddf918e&lang=sass&scoped=true&
var CDialogvue_type_style_index_0_id_0ddf918e_lang_sass_scoped_true_ = __webpack_require__("a5f9");

// CONCATENATED MODULE: ./src/components/_inputs/CDialog.vue






/* normalize component */

var CDialog_component = normalizeComponent(
  _inputs_CDialogvue_type_script_lang_js_,
  CDialogvue_type_template_id_0ddf918e_scoped_true_lang_pug_render,
  CDialogvue_type_template_id_0ddf918e_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "0ddf918e",
  null
  
)

/* harmony default export */ var CDialog = (CDialog_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CGroupname.vue?vue&type=template&id=3c87ecea&scoped=true&lang=pug&
var CGroupnamevue_type_template_id_3c87ecea_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('fieldset',[_c('label',{attrs:{"for":_vm.id}},[_vm._v("Name the group")]),_c('input',{staticClass:"field",attrs:{"id":_vm.id,"type":"text","placeholder":"Optionally type a name for the group..."},domProps:{"value":_vm.currentName},on:{"input":function($event){_vm.groupName = $event.target.value}}})])}
var CGroupnamevue_type_template_id_3c87ecea_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_inputs/CGroupname.vue?vue&type=template&id=3c87ecea&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_inputs/CGroupname.vue?vue&type=script&lang=js&
//
//
//
//
//
//
/* harmony default export */ var CGroupnamevue_type_script_lang_js_ = ({
  props: {
    selection: {
      type: Array,
      required: true
    },
    id: {
      type: String
    },
    value: {
      type: String
    }
  },
  data: function data() {
    return {
      groupName: ''
    };
  },
  computed: {
    suggestedName: function suggestedName() {
      var current = this.selection.length;
      var total = this.$colvis.getEntities().length;
      var natureName = this.$colvis.getNatureName(this.selection[0].natureId, true);
      if (current === total) return "All ".concat(natureName);else if (current > total * .8) return "Most ".concat(natureName);
      return null;
    },
    currentName: function currentName() {
      if (this.groupName) return this.groupName;else if (this.suggestedName) return this.suggestedName;else return null;
    }
  },
  mounted: function mounted() {
    if (this.value) this.groupName = this.value;
    this.$emit('input', this.currentName);
  },
  watch: {
    currentName: function currentName() {
      this.$emit('input', this.currentName);
    },
    suggestedName: function suggestedName() {
      this.$emit('input', this.currentName);
    },
    value: function value() {
      if (this.groupName !== this.value) {
        this.groupName = this.value;
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/_inputs/CGroupname.vue?vue&type=script&lang=js&
 /* harmony default export */ var _inputs_CGroupnamevue_type_script_lang_js_ = (CGroupnamevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_inputs/CGroupname.vue?vue&type=style&index=0&id=3c87ecea&lang=sass&scoped=true&
var CGroupnamevue_type_style_index_0_id_3c87ecea_lang_sass_scoped_true_ = __webpack_require__("8b90");

// CONCATENATED MODULE: ./src/components/_inputs/CGroupname.vue






/* normalize component */

var CGroupname_component = normalizeComponent(
  _inputs_CGroupnamevue_type_script_lang_js_,
  CGroupnamevue_type_template_id_3c87ecea_scoped_true_lang_pug_render,
  CGroupnamevue_type_template_id_3c87ecea_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "3c87ecea",
  null
  
)

/* harmony default export */ var CGroupname = (CGroupname_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CMessage.vue?vue&type=template&id=3092c226&lang=pug&
var CMessagevue_type_template_id_3092c226_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('transition',{attrs:{"name":"fade"}},[(_vm.value)?_c('aside',{staticClass:"colvis-message",class:_vm.messageClass},[_c('header',{staticClass:"colvis-message__header"},[_c('span',[_vm._v(_vm._s(_vm.value.type))]),_c('span',{staticClass:"colvis-message__header__close",on:{"click":_vm.dismiss}},[_vm._v("close message")])]),_c('div',{staticClass:"colvis-message__main"},[_c('p',[_vm._v(_vm._s(_vm.value.content))])])]):_vm._e()])}
var CMessagevue_type_template_id_3092c226_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/CMessage.vue?vue&type=template&id=3092c226&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CMessage.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
var classes = {
  error: 'colvis-message--error',
  warning: 'colvis-message--warning',
  success: 'colvis-message--success',
  default: ''
};
/* harmony default export */ var CMessagevue_type_script_lang_js_ = ({
  props: {
    value: {
      type: Object
    }
  },
  computed: {
    messageClass: function messageClass() {
      var c = classes[this.value.type];
      if (!c) return classes.default;else return c;
    }
  },
  methods: {
    dismiss: function dismiss() {
      this.$emit('input', null);
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/CMessage.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_CMessagevue_type_script_lang_js_ = (CMessagevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_generic/CMessage.vue?vue&type=style&index=0&lang=sass&
var CMessagevue_type_style_index_0_lang_sass_ = __webpack_require__("4972");

// CONCATENATED MODULE: ./src/components/_generic/CMessage.vue






/* normalize component */

var CMessage_component = normalizeComponent(
  _generic_CMessagevue_type_script_lang_js_,
  CMessagevue_type_template_id_3092c226_lang_pug_render,
  CMessagevue_type_template_id_3092c226_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var CMessage = (CMessage_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CIcon.vue?vue&type=template&id=02ec376f&scoped=true&lang=pug&
var CIconvue_type_template_id_02ec376f_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('i',{staticClass:"colvis-icon",class:_vm.iconClass},[_vm._t("default",[_vm._v("Icon")]),(_vm.message)?_c('small',{staticClass:"colvis-icon__message"},[_vm._v(_vm._s(_vm.message))]):_vm._e()],2)}
var CIconvue_type_template_id_02ec376f_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/CIcon.vue?vue&type=template&id=02ec376f&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CIcon.vue?vue&type=script&lang=js&
//
//
//
//
//
//
var CIconvue_type_script_lang_js_classes = {
  error: 'colvis-color--error',
  warning: 'colvis-color--warning',
  success: 'colvis-color--success',
  default: 'colvis-color'
};
/* harmony default export */ var CIconvue_type_script_lang_js_ = ({
  props: {
    message: {
      type: String
    },
    type: {
      type: String
    }
  },
  computed: {
    iconClass: function iconClass() {
      var c = CIconvue_type_script_lang_js_classes[this.type];
      if (!c) return CIconvue_type_script_lang_js_classes.default;else return c;
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/CIcon.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_CIconvue_type_script_lang_js_ = (CIconvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_generic/CIcon.vue?vue&type=style&index=0&id=02ec376f&lang=sass&scoped=true&
var CIconvue_type_style_index_0_id_02ec376f_lang_sass_scoped_true_ = __webpack_require__("bb56");

// CONCATENATED MODULE: ./src/components/_generic/CIcon.vue






/* normalize component */

var CIcon_component = normalizeComponent(
  _generic_CIconvue_type_script_lang_js_,
  CIconvue_type_template_id_02ec376f_scoped_true_lang_pug_render,
  CIconvue_type_template_id_02ec376f_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "02ec376f",
  null
  
)

/* harmony default export */ var CIcon = (CIcon_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/CText.vue?vue&type=template&id=5fa5e14a&scoped=true&lang=pug&
var CTextvue_type_template_id_5fa5e14a_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.structured)?_c('div',{staticClass:"text"},[_c('span',{staticClass:"text__unit"},[_c('c-chip',{attrs:{"text":_vm.subject.title,"inactive":!_vm.subject.units.length && !_vm.clickEvent},on:{"mouseover":function($event){_vm.activeAnnotation ? null : _vm.$emit('highlight', _vm.subject)},"mouseout":function($event){_vm.activeAnnotation ? null : _vm.$emit('highlight')},"click":function($event){$event.stopPropagation();return _vm.clickUnit('subject', _vm.subject)}},scopedSlots:_vm._u([(_vm.filter)?{key:"prepend",fn:function(){return [_c('sort-icon',{on:{"click":function($event){$event.stopPropagation();return _vm.$emit('unit', _vm.subject)}}})]},proxy:true}:null],null,true)}),(_vm.subject.units.length && _vm.agDetails)?_c('div',{staticClass:"text__unit__ag",class:{ active: _vm.subjectVisible }},_vm._l((_vm.subject.units),function(unit){return _c('c-chip',{attrs:{"text":unit.title,"inactive":""},on:{"mouseover":function($event){_vm.activeAnnotation ? null : _vm.$emit('highlight', unit)},"mouseout":function($event){_vm.activeAnnotation ? null : _vm.$emit('highlight')}}})}),1):_vm._e()],1),_c('span',{staticClass:"text__verb"},[_vm._v(_vm._s(_vm.verb))]),(_vm.complement)?_c('span',{staticClass:"text__unit"},[_c('c-chip',{attrs:{"text":_vm.complement.title,"inactive":!_vm.complement.units.length && !_vm.clickEvent},on:{"mouseover":function($event){_vm.activeAnnotation ? null : _vm.$emit('highlight', _vm.complement)},"mouseout":function($event){_vm.activeAnnotation ? null : _vm.$emit('highlight')},"click":function($event){$event.stopPropagation();return _vm.clickUnit('complement', _vm.complement)}},scopedSlots:_vm._u([(_vm.filter)?{key:"prepend",fn:function(){return [_c('sort-icon',{on:{"click":function($event){$event.stopPropagation();return _vm.$emit('unit', _vm.complement)}}},[_vm._v("filter")])]},proxy:true}:null],null,true)}),(_vm.complement.units.length && _vm.agDetails)?_c('div',{staticClass:"text__unit__ag",class:{ active: _vm.complementVisible }},_vm._l((_vm.complement.units),function(unit){return _c('c-chip',{attrs:{"text":unit.title,"inactive":""},on:{"mouseover":function($event){_vm.activeAnnotation ? null : _vm.$emit('highlight', unit)},"mouseout":function($event){_vm.activeAnnotation ? null : _vm.$emit('highlight')}}})}),1):_vm._e()],1):_vm._e(),(_vm.details)?_c('span',{staticClass:"text__details"},[_vm._v(_vm._s(_vm.details))]):_vm._e(),(_vm.meaning)?_c('span',{staticClass:"text__meaning"},[_vm._v(_vm._s(_vm.meaning))]):_vm._e()]):_c('div',{staticClass:"text"},[_vm._v(_vm._s(_vm.text))])}
var CTextvue_type_template_id_5fa5e14a_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Retrieval/CText.vue?vue&type=template&id=5fa5e14a&scoped=true&lang=pug&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.regexp.match.js
var es6_regexp_match = __webpack_require__("4917");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/SortIcon.vue?vue&type=template&id=04e7b7de&lang=pug&
var SortIconvue_type_template_id_04e7b7de_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ 'margin-right': '1rem', 'vertical-align': 'middle', fill: _vm.color }),attrs:{"width":_vm.size,"height":_vm.size,"viewBox":"0 0 24 24"},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)},"mouseover":function($event){$event.stopPropagation();return _vm.$emit('mouseover', $event)}}},[_c('path',{attrs:{"d":"M19 7H22L18 3L14 7H17V21H19M5 7C5 8.1 5.9 9 7 9C8.1 9 9 8.1 9 7C9 5.9 8.1 5 7 5C5.9 5 5 5.9 5 7M7 3C9.21 3 11 4.79 11 7C11 9.21 9.21 11 7 11C4.79 11 3 9.21 3 7C3 4.79 4.79 3 7 3M7 13C4.79 13 3 14.79 3 17C3 19.21 4.79 21 7 21C9.21 21 11 19.21 11 17C11 14.79 9.21 13 7 13Z"}})])}
var SortIconvue_type_template_id_04e7b7de_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/SortIcon.vue?vue&type=template&id=04e7b7de&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/SortIcon.vue?vue&type=script&lang=js&

//
//
//
//
//
/* harmony default export */ var SortIconvue_type_script_lang_js_ = ({
  props: {
    color: {
      type: String,
      default: 'white'
    },
    size: {
      type: Number,
      default: 24
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/SortIcon.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_SortIconvue_type_script_lang_js_ = (SortIconvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/SortIcon.vue





/* normalize component */

var SortIcon_component = normalizeComponent(
  _generic_SortIconvue_type_script_lang_js_,
  SortIconvue_type_template_id_04e7b7de_lang_pug_render,
  SortIconvue_type_template_id_04e7b7de_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var SortIcon = (SortIcon_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/CText.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var CTextvue_type_script_lang_js_ = ({
  components: {
    CChip: CChip,
    SortIcon: SortIcon
  },
  props: {
    annotation: {
      type: Object,
      required: true
    },
    activeAnnotation: {
      type: Object
    },
    filter: {
      type: Boolean,
      default: false
    },
    clickEvent: {
      type: String
    },
    agDetails: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      subjectVisible: false,
      complementVisible: false
    };
  },
  computed: {
    structured: function structured() {
      return !!this.annotation.patterns;
    },
    text: function text() {
      return this.annotation.text;
    },
    subject: function subject() {
      return this.annotation.dataUnits.filter(function (u) {
        return u.role === 'subject';
      })[0];
    },
    verb: function verb() {
      var _this$subject$units;

      var n = ((_this$subject$units = this.subject.units) === null || _this$subject$units === void 0 ? void 0 : _this$subject$units.length) > 1 ? 'plural' : 'singular';
      var verb = this.annotation.rawAnnotation.reason.verb[n];
      if (this.complement) verb += " ".concat(this.annotation.rawAnnotation.reason.verb.comparison);else if (!this.details) verb += ".";
      return " ".concat(verb);
    },
    complement: function complement() {
      return this.annotation.dataUnits.filter(function (u) {
        return u.role === 'complement';
      })[0];
    },
    details: function details() {
      var d = this.annotation.rawAnnotation.reason.details;
      if (!d) return;
      d = d.substr(-1).match(/^[.,:!? ]/) ? d : "".concat(d, ".");
      return " ".concat(d);
    },
    meaning: function meaning() {
      var m = this.annotation.rawAnnotation.meaning;
      if (!m) return;
      m = m.substr(-1).match(/^[.,:!? ]/) ? m : "".concat(m, ".");
      return " ".concat(m);
    }
  },
  methods: {
    clickUnit: function clickUnit(role, unit) {
      if (this.clickEvent) this.$emit(this.clickEvent, unit);else if (role === 'subject') this.subjectVisible = !this.subjectVisible;else this.complementVisible = !this.complementVisible;
    }
  },
  watch: {
    subjectVisible: function subjectVisible() {
      if (this.subjectVisible) this.complementVisible = false;
    },
    complementVisible: function complementVisible() {
      if (this.complementVisible) this.subjectVisible = false;
    }
  }
});
// CONCATENATED MODULE: ./src/components/Retrieval/CText.vue?vue&type=script&lang=js&
 /* harmony default export */ var Retrieval_CTextvue_type_script_lang_js_ = (CTextvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Retrieval/CText.vue?vue&type=style&index=0&id=5fa5e14a&lang=sass&scoped=true&
var CTextvue_type_style_index_0_id_5fa5e14a_lang_sass_scoped_true_ = __webpack_require__("c6f0");

// CONCATENATED MODULE: ./src/components/Retrieval/CText.vue






/* normalize component */

var CText_component = normalizeComponent(
  Retrieval_CTextvue_type_script_lang_js_,
  CTextvue_type_template_id_5fa5e14a_scoped_true_lang_pug_render,
  CTextvue_type_template_id_5fa5e14a_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "5fa5e14a",
  null
  
)

/* harmony default export */ var CText = (CText_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Box.vue?vue&type=script&lang=js&




//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//











function initialState() {
  /** Whether the current annotation should be structured */
  var structured = true;
  /** Current step of the annotation process */

  var step = 1;
  /** The conclusion / free annotation */

  var conclusion = '';
  /** The verb (will be replaced) */

  var reason = {
    verb: '',
    details: ''
  };
  /** Tags associated with the annotation */

  var tags = [];
  /** @type Boolean Colvis message */

  var colvisEvent = null;
  var subjectName = null;
  var complementName = null;
  var subjectSuggestions = [];
  var complementSuggestions = [];
  var preview = false;
  return {
    structured: structured,
    step: step,
    conclusion: conclusion,
    reason: reason,
    tags: tags,
    colvisEvent: colvisEvent,
    subjectName: subjectName,
    complementName: complementName,
    subjectSuggestions: subjectSuggestions,
    complementSuggestions: complementSuggestions,
    preview: preview
  };
}
/**
 * Create a new Box.
 * Boxes allow analysts to enter their annotation.
 */


/* harmony default export */ var Boxvue_type_script_lang_js_ = ({
  components: {
    CAutocomplete: CAutocomplete,
    CCombobox: CCombobox,
    CSuggestion: CSuggestion,
    CGroupname: CGroupname,
    CDialog: CDialog,
    CMessage: CMessage,
    CIcon: CIcon,
    CText: CText
  },
  props: {
    /** The list of selected subjects */
    selectedSubjects: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The list of selected components */
    selectedComplements: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The list of existing units in the backend's database */
    previousUnits: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The list of existing tags in the backend's database */
    previousTags: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    selectionMethods: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    replyTo: {
      type: Object
    },
    replyType: {
      type: String
    },
    unitToRestore: {
      type: Object
    }
  },
  data: function data() {
    return initialState();
  },
  computed: {
    /** @returns {RawAnnotation} the raw, temporary annotation */
    theRawAnnotation: function theRawAnnotation() {
      var selectedSubjects = this.selectedSubjects,
          selectedComplements = this.selectedComplements,
          reason = this.reason,
          conclusion = this.conclusion,
          tags = this.tags,
          selectionMethods = this.selectionMethods,
          subjectName = this.subjectName,
          complementName = this.complementName,
          replyTo = this.replyTo,
          replyType = this.replyType;
      return new RawAnnotation_RawAnnotation({
        subjects: selectedSubjects,
        complements: selectedComplements,
        meaning: conclusion,
        specs: this.$colvis.getSpecs(),
        ci: this.$colvis,
        reason: reason,
        selectionMethods: selectionMethods,
        tags: tags,
        subjectName: subjectName,
        complementName: complementName,
        replyTo: replyTo === null || replyTo === void 0 ? void 0 : replyTo.id,
        replyType: replyType
      });
    },

    /** @returns {Annotation|Boolean} the annotation */
    theAnnotation: function theAnnotation() {
      if (this.selectedSubjects.length && (this.reason.verb || this.reason.details)) {
        return new Annotation_Annotation({
          rawAnnotation: this.theRawAnnotation,
          specs: this.$colvis.getSpecs()
        });
      } else return null;
    },

    /**
     * Whether the annotation being currently written is a Generality. 
     * @returns {Boolean}
     */
    isGenerality: function isGenerality() {
      return this.selectedSubjects.length / this.$colvis.getEntities().length > 0.8;
    }
  },
  methods: {
    submit: function submit() {
      if (this.theAnnotation) {
        this.theAnnotation.setMeta(this.$colvis); // this.theAnnotation.deleteNullsProps();

        this.$emit('submittingAnnotation', this.theAnnotation);
        Object.assign(this.$data, initialState());
      } else {
        this.colvisEvent = new ColvisMessage_ColvisMessage('error', 'Please make sure you fill at least all mandatory steps (1 and 2) before submitting the annotation.');
      }
    },
    checkSelection: function checkSelection(pool, suggestions) {
      var _this = this;

      suggestions.splice(0, suggestions.length);
      this.previousUnits.forEach(function (u) {
        if (!u.aggregated || !u.title || !u.units) return;
        var prevIds = u.units.map(function (s) {
          return s.id;
        });
        var curIds = pool.map(function (s) {
          return s.id;
        });
        var matches = prevIds.reduce(function (p, c) {
          return p + curIds.includes(c);
        }, 0);
        var matchRatio = matches / Math.max(prevIds.length, curIds.length);

        if (matchRatio === 1 && u.title == _this.subjectName) {
          console.log('similar data unit');
        } else if (matchRatio > .8) {
          suggestions.push({
            unit: u,
            matchRatio: matchRatio
          });
        }
      });
    },
    checkName: function checkName(name, pool, suggestions) {
      var _this2 = this;

      suggestions.splice(0, suggestions.length);
      this.previousUnits.forEach(function (u) {
        if (!u.aggregated || !u.title || !u.units) return;

        if (u.title.toLowerCase().includes(name.toLowerCase())) {
          var prevIds = u.units.map(function (s) {
            return s.id;
          });
          var curIds = pool.map(function (s) {
            return s.id;
          });
          var matches = prevIds.reduce(function (p, c) {
            return p + curIds.includes(c);
          }, 0);
          var matchRatio = matches / Math.max(prevIds.length, curIds.length);

          if (matchRatio === 1 && u.title == _this2.subjectName) {
            console.log('similar data unit');
          } else suggestions.push({
            unit: u,
            text: true,
            matchRatio: matchRatio
          });
        }
      });
    },
    chooseSuggestion: function chooseSuggestion(unit, pool) {
      if (pool === 'subject') {
        this.$emit('boxSubjectSelection', unit.units);
        this.subjectName = unit.title;
        this.subjectSuggestions = [];
      } else {
        this.$emit('boxComplementSelection', unit.units);
        this.complementName = unit.title;
        this.complementSuggestions = [];
      }
    }
  },
  watch: {
    step: function step() {
      this.$emit('stepChanged', this.step);
    },
    selectedSubjects: function selectedSubjects() {
      this.checkSelection(this.selectedSubjects, this.subjectSuggestions);
    },
    subjectName: function subjectName() {
      if (this.subjectName && this.subjectName.length > 3) {
        this.checkName(this.subjectName, this.selectedSubjects, this.subjectSuggestions);
      } else this.subjectSuggestions = [];
    },
    selectedComplements: function selectedComplements() {
      this.checkSelection(this.selectedComplements, this.complementSuggestions);
    },
    complementName: function complementName() {
      if (this.complementName && this.complementName.length > 3) {
        this.checkName(this.complementName, this.selectedComplements, this.complementSuggestions);
      } else this.complementSuggestions = [];
    },
    unitToRestore: function unitToRestore() {
      if (this.unitToRestore && (this.step === 1 || this.step === 3)) {
        var ids = this.unitToRestore.aggregated ? this.unitToRestore.units.map(function (u) {
          return u.id;
        }) : [this.unitToRestore.id];
        var items = this.$colvis.getAnnotables();

        if (this.step === 1) {
          this.$emit('boxSubjectSelection', items.filter(function (i) {
            return ids.includes(i.id);
          }));

          if (this.unitToRestore.aggregated) {
            this.subjectName = this.unitToRestore.title;
          }
        } else if (this.step === 3) {
          this.$emit('boxComplementSelection', items.filter(function (i) {
            return ids.includes(i.id);
          }));
          if (this.unitToRestore.aggregated) this.complementName = this.unitToRestore.title;
        }

        ;
      }

      this.$emit('unitRestored');
    }
  }
});
// CONCATENATED MODULE: ./src/components/Input/Box.vue?vue&type=script&lang=js&
 /* harmony default export */ var Input_Boxvue_type_script_lang_js_ = (Boxvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Input/Box.vue?vue&type=style&index=0&lang=sass&
var Boxvue_type_style_index_0_lang_sass_ = __webpack_require__("6859");

// CONCATENATED MODULE: ./src/components/Input/Box.vue






/* normalize component */

var Box_component = normalizeComponent(
  Input_Boxvue_type_script_lang_js_,
  Boxvue_type_template_id_7fc82d01_lang_pug_render,
  Boxvue_type_template_id_7fc82d01_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var Box = (Box_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Free.vue?vue&type=template&id=3cdd7e46&scoped=true&lang=pug&
var Freevue_type_template_id_3cdd7e46_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('form',{ref:"freeForm",on:{"submit":function($event){$event.preventDefault();return _vm.submit($event)}}},[_c('div',{staticClass:"free"},[_c('div',{staticClass:"free__main"},[_vm._m(0),_c('div',{staticClass:"free__main__content"},[_c('textarea',{directives:[{name:"model",rawName:"v-model",value:(_vm.annotation),expression:"annotation"}],staticClass:"textarea",attrs:{"label":"Annotation","placeholder":"It seems that..."},domProps:{"value":(_vm.annotation)},on:{"input":function($event){if($event.target.composing){ return; }_vm.annotation=$event.target.value}}}),_c('c-combobox',{attrs:{"label":"Tags","placeholder":"myItems, generalObservation, etc...","hint":"You can tag your annotation to make it easier to retrieve. To use several tags, separate them by typing space, enter or comma.","items":_vm.previousTags},model:{value:(_vm.tags),callback:function ($$v) {_vm.tags=$$v},expression:"tags"}})],1)])]),_vm._m(1)])}
var Freevue_type_template_id_3cdd7e46_scoped_true_lang_pug_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('header',{staticClass:"free__main__head actionable"},[_c('span',[_vm._v("Annotate")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-submit"},[_c('button',{staticClass:"btn btn-main",attrs:{"type":"submit"}},[_vm._v("Submit")])])}]


// CONCATENATED MODULE: ./src/components/Input/Free.vue?vue&type=template&id=3cdd7e46&scoped=true&lang=pug&

// CONCATENATED MODULE: ./src/models/Annotation/Free.js



/**
 * A free annotation, without structure beside simple tags.
 * @class
 */

var Free_FreeAnnotation =
/**
 * The content of the annotation
 * @type String
 * @public
 */

/**
 * The list of tags
 * @type String[]
 * @public
 */

/**
 * Type of reply, if any
 * @type {Boolean}
 * @public
 */

/**
 * replyTo
 * @public
 */

/**
 * Meta information regarding the annotation
 * @type {Meta}
 * @public
 */
function FreeAnnotation(_ref) {
  var annotation = _ref.annotation,
      tags = _ref.tags,
      replyTo = _ref.replyTo,
      replyType = _ref.replyType,
      ci = _ref.ci;

  _classCallCheck(this, FreeAnnotation);

  _defineProperty(this, "annotation", void 0);

  _defineProperty(this, "tags", []);

  _defineProperty(this, "replyType", void 0);

  _defineProperty(this, "replyTo", void 0);

  _defineProperty(this, "meta", void 0);

  this.annotation = annotation;
  this.tags = tags;
  this.replyType = replyType;
  this.replyTo = replyTo;
  this.meta = new Annotation_Meta(ci);
};


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Free.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
//
//
//
//




function Freevue_type_script_lang_js_initialState() {
  var colvisEvent = null;
  var annotation = null;
  var tags = [];
  return {
    colvisEvent: colvisEvent,
    annotation: annotation,
    tags: tags
  };
}

/* harmony default export */ var Freevue_type_script_lang_js_ = ({
  components: {
    CMessage: ColvisMessage_ColvisMessage,
    CCombobox: CCombobox
  },
  props: {
    /** The list of existing tags in the backend's database */
    previousTags: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    replyTo: {
      type: Object
    },
    replyType: {
      type: String
    }
  },
  data: function data() {
    return new Freevue_type_script_lang_js_initialState();
  },
  methods: {
    submit: function submit() {
      var annotation = new Free_FreeAnnotation({
        annotation: this.annotation,
        tags: this.tags,
        replyTo: this.replyTo,
        replyType: this.replyType,
        ci: this.$colvis
      });
      this.$emit('submittingAnnotation', annotation);
      Object.assign(this.$data, Freevue_type_script_lang_js_initialState());
    }
  }
});
// CONCATENATED MODULE: ./src/components/Input/Free.vue?vue&type=script&lang=js&
 /* harmony default export */ var Input_Freevue_type_script_lang_js_ = (Freevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Input/Free.vue?vue&type=style&index=0&id=3cdd7e46&lang=sass&scoped=true&
var Freevue_type_style_index_0_id_3cdd7e46_lang_sass_scoped_true_ = __webpack_require__("f736");

// CONCATENATED MODULE: ./src/components/Input/Free.vue






/* normalize component */

var Free_component = normalizeComponent(
  Input_Freevue_type_script_lang_js_,
  Freevue_type_template_id_3cdd7e46_scoped_true_lang_pug_render,
  Freevue_type_template_id_3cdd7e46_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "3cdd7e46",
  null
  
)

/* harmony default export */ var Free = (Free_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CLoading.vue?vue&type=template&id=2947297e&scoped=true&lang=pug&
var CLoadingvue_type_template_id_2947297e_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _vm._m(0)}
var CLoadingvue_type_template_id_2947297e_scoped_true_lang_pug_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"loading"},[_c('div',{staticClass:"loading__bar"})])}]


// CONCATENATED MODULE: ./src/components/_generic/CLoading.vue?vue&type=template&id=2947297e&scoped=true&lang=pug&

// EXTERNAL MODULE: ./src/components/_generic/CLoading.vue?vue&type=style&index=0&id=2947297e&lang=sass&scoped=true&
var CLoadingvue_type_style_index_0_id_2947297e_lang_sass_scoped_true_ = __webpack_require__("d98b");

// CONCATENATED MODULE: ./src/components/_generic/CLoading.vue

var script = {}



/* normalize component */

var CLoading_component = normalizeComponent(
  script,
  CLoadingvue_type_template_id_2947297e_scoped_true_lang_pug_render,
  CLoadingvue_type_template_id_2947297e_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "2947297e",
  null
  
)

/* harmony default export */ var CLoading = (CLoading_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Overlay.vue?vue&type=script&lang=js&




//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/**
 * Create a new Overlay.
 * An overlay includes the annotation box and places it within a navigation box.
 * The box is detached from the Overlay so that developers can use it outside of the context
 * of the box, should it be needed.
 */

/* harmony default export */ var Overlayvue_type_script_lang_js_ = ({
  components: {
    ColInputBox: Box,
    CText: CText,
    ColFreeBox: Free,
    CLoading: CLoading
  },
  props: {
    /** Whether the user is annotating the visualization */
    annotating: {
      type: Boolean,
      default: false
    },

    /** Whether to use the dark theme for the Vuetify's components */
    dark: {
      type: Boolean,
      default: false
    },

    /** The list of selected subjects */
    selectedSubjects: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The list of selected components */
    selectedComplements: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The list of existing units in the backend's database */
    previousUnits: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The list of existing tags in the backend's database */
    previousTags: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    loading: {
      type: Boolean,
      default: false
    },
    selectionMethods: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    free: {
      type: Boolean,
      default: false
    },
    replyTo: {
      type: Object
    }
  },
  data: function data() {
    return {
      top: 0,
      left: 0,
      offsetTop: 0,
      offsetLeft: 0,
      maxVertical: 0,
      maxHorizontal: 0,
      moving: false,
      eventListeners: [],
      replyType: 'comment',
      structured: true,
      unitToRestore: null
    };
  },
  computed: {
    topPx: function topPx() {
      if (!this.$colvis.getOptions().floating) return '0';
      var intendedTop = this.top + this.offsetTop;
      var top = intendedTop > 0 ? intendedTop : 0;
      top = top < this.maxVertical ? top : this.maxVertical;
      return "".concat(top, "px");
    },
    leftPx: function leftPx() {
      if (!this.$colvis.getOptions().floating) return 'calc(100% - 30rem)';
      var intendedLeft = this.left + this.offsetLeft;
      var left = intendedLeft > 0 ? intendedLeft : 0;
      left = left < this.maxHorizontal ? left : this.maxHorizontal;
      return "".concat(left, "px");
    }
  },
  methods: {
    startMoving: function startMoving(e) {
      this.moving = true;

      var _e$target$getBounding = e.target.getBoundingClientRect(),
          top = _e$target$getBounding.top,
          left = _e$target$getBounding.left;

      this.top = e.clientY;
      this.left = e.clientX;
      this.offsetTop = top - e.clientY;
      this.offsetLeft = left - e.clientX;
    },
    move: function move(e) {
      if (!this.moving) return;
      this.top = e.clientY;
      this.left = e.clientX;
    },
    stopMoving: function stopMoving() {
      this.moving = false;
      console.log('debug: not moving anymore');
    },
    selectUnit: function selectUnit(unit) {
      this.unitToRestore = unit;
    }
  },
  mounted: function mounted() {
    var offsetTop = 0;
    var offsetLeft = 0;
    var offsetter = this.$refs.overlay.parentElement;
    this.maxVertical = document.body.clientHeight - this.$refs.box.clientHeight;
    this.maxHorizontal = document.body.clientWidth - this.$refs.box.clientWidth;

    while (offsetter.offsetParent) {
      offsetTop += offsetter.offsetParent.offsetTop;
      offsetLeft += offsetter.offsetParent.offsetLeft;
      offsetter = offsetter.offsetParent;
    }

    Object.assign(this.$data, {
      offsetTop: offsetTop,
      offsetLeft: offsetLeft
    });
    document.body.addEventListener('mouseleave', this.stopMoving);
    this.eventListeners.push({
      mouseleave: this.stopMoving
    });
  },
  destroyed: function destroyed() {
    this.eventListeners.forEach(function (e) {
      Object.keys(e).forEach(function (key) {
        document.body.removeEventListener(key, e[key]);
      });
    });
  },
  watch: {
    annotating: function annotating() {
      this.maxVertical = document.body.clientHeight - this.$refs.box.clientHeight;
      this.maxHorizontal = document.body.clientWidth - this.$refs.box.clientWidth;
    }
  }
});
// CONCATENATED MODULE: ./src/components/Input/Overlay.vue?vue&type=script&lang=js&
 /* harmony default export */ var Input_Overlayvue_type_script_lang_js_ = (Overlayvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Input/Overlay.vue?vue&type=style&index=0&id=7be65390&lang=sass&scoped=true&
var Overlayvue_type_style_index_0_id_7be65390_lang_sass_scoped_true_ = __webpack_require__("1c76");

// CONCATENATED MODULE: ./src/components/Input/Overlay.vue






/* normalize component */

var Overlay_component = normalizeComponent(
  Input_Overlayvue_type_script_lang_js_,
  Overlayvue_type_template_id_7be65390_scoped_true_lang_pug_render,
  Overlayvue_type_template_id_7be65390_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "7be65390",
  null
  
)

/* harmony default export */ var Overlay = (Overlay_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Input/Main.vue?vue&type=script&lang=js&




//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




function Mainvue_type_script_lang_js_initialState() {
  /** The list of selected subjects */
  var selectedSubjects = [];
  /** The list of selected components */

  var selectedComplements = [];
  /** The current step of the annotation */

  var step = 1;
  /** The type of selections */

  var selectionMethods = [];
  /** Whether a free annotation is being taken */

  var free = false;
  return {
    selectedSubjects: selectedSubjects,
    selectedComplements: selectedComplements,
    selectionMethods: selectionMethods,
    step: step,
    free: free
  };
}
/**
 * Create a new Colvis.
 * This component manages all data and communication between
 * the other subcomponents (chiefly, the Selector and the Overlay).
 */


/* harmony default export */ var Mainvue_type_script_lang_js_ = ({
  components: {
    ColInputOverlay: Overlay,
    ColInputSelector: Selector
  },
  props: {
    /** Whether the user is annotating the visualization */
    annotating: {
      type: Boolean,
      default: false
    },

    /** Whether to use the dark theme for the Vuetify's components */
    dark: {
      type: Boolean,
      default: false
    },

    /** The list of existing units in the backend's database */
    previousUnits: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** The list of existing tags in the backend's database */
    previousTags: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** Whether the parent is loading data */
    loading: {
      type: Boolean,
      default: false
    },

    /** The state of the visualization */
    state: {
      type: Object
    },

    /** Whether Colvis should reset Data Binders */
    resetBinders: {
      type: Boolean,
      default: false
    },

    /** The annotation that is being replied to */
    replyTo: {
      type: Object
    }
  },
  data: function data() {
    return Mainvue_type_script_lang_js_initialState();
  },
  methods: {
    /**
     * Select a list of elements, based on the selection's coordinates usually sent from a Selector.
     * @param {Event} event the event emitted from the Selector
     * @param {SelectionCoordinates} event.coordinates selected rectangle where to search for selectable elements
     * @param {Object} event.mode contains various information regarding the selection's behavior
     * @param {string} [pool=subject] either 'subject' or 'complement', which pool to feed with the selection
     */
    selectElementsBySelector: function selectElementsBySelector(_ref) {
      var coordinates = _ref.coordinates,
          mode = _ref.mode;
      var pool = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'subject';
      var currentPool = pool === 'subject' ? this.selectedSubjects : this.selectedComplements;
      var opposedPool = pool === 'subject' ? this.selectedComplements : this.selectedSubjects;
      var selectedElements = this.$colvis.getAnnotables().map(function (el) {
        var elRect = el.domElement.getBoundingClientRect();
        el.selected = checkIfSelected(elRect, coordinates) && ensureNotPresent(el, opposedPool);
        return el;
      }).filter(function (el) {
        return el.selected;
      });
      this.selectionMethods.push('selector');

      if (!mode.add) {
        currentPool.length = 0;
        currentPool.push.apply(currentPool, _toConsumableArray(selectedElements));
      } else {
        var els = enforceUniqueId(currentPool.concat(selectedElements));
        currentPool.length = 0;
        currentPool.push.apply(currentPool, _toConsumableArray(els));
      }
    },

    /**
     * Select a list of elements.
     * @param {Array} elements a list of elements
     * @param {string} [pool=subject] either 'subject' or 'complement', which pool to feed with the selection
     */
    selectElementsByBox: function selectElementsByBox(elements) {
      var pool = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'subject';
      var els = elements.map(function (el) {
        return el.id;
      });
      var selectedElements = this.$colvis.getAnnotables().map(function (el) {
        el.selected = els.includes(el.id);
        return el;
      }).filter(function (el) {
        return el.selected;
      });
      this.selectionMethods.push('combobox');
      if (pool === 'subject') this.selectedSubjects = selectedElements;else this.selectedComplements = selectedElements;
    },
    submit: function submit(a) {
      this.$emit('submittingAnnotation', a);
      Object.assign(this.$data, Mainvue_type_script_lang_js_initialState());
    }
  },
  watch: {
    state: function state() {
      this.$colvis.setVizState(this.state);
    },
    resetBinders: function resetBinders() {
      if (this.resetBinders) {
        this.$colvis.resetDataBinders();
        this.$emit('bindersReset');
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/Input/Main.vue?vue&type=script&lang=js&
 /* harmony default export */ var Input_Mainvue_type_script_lang_js_ = (Mainvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Input/Main.vue?vue&type=style&index=0&lang=sass&
var Mainvue_type_style_index_0_lang_sass_ = __webpack_require__("2bdc");

// CONCATENATED MODULE: ./src/components/Input/Main.vue






/* normalize component */

var Main_component = normalizeComponent(
  Input_Mainvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var Main = (Main_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/Viz.vue?vue&type=template&id=1db12965&lang=pug&
var Vizvue_type_template_id_1db12965_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.$colvis.isInit())?_c('section',{staticClass:"col-retrieval--viz"},[_c('col-retrieval-viewer',{directives:[{name:"show",rawName:"v-show",value:(_vm.activeUnits.length),expression:"activeUnits.length"}],attrs:{"activeUnits":_vm.activeUnits,"init":_vm.$colvis.isInit(),"offsetTop":_vm.offsetTop}}),_c('col-retrieval-list',{attrs:{"annotations":_vm.annotations,"activeAnnotation":_vm.activeAnnotation,"canReply":_vm.canReply,"state":_vm.state,"viewing":_vm.viewing,"replyTo":_vm.replyTo,"activeParent":_vm.activeParent,"answersToActive":_vm.answersToActive},on:{"highlight":_vm.selectUnit,"selectAnnotation":_vm.selectAnnotation,"avatar":function($event){return _vm.$emit('avatar', $event)},"close":function($event){return _vm.$emit('close')},"reply":function($event){return _vm.$emit('reply', $event)},"endorse":function($event){return _vm.$emit('endorse', $event)},"funny":function($event){return _vm.$emit('funny', $event)}},scopedSlots:_vm._u([{key:"card",fn:function(){return [_vm._t("card")]},proxy:true}],null,true)})],1):_vm._e()}
var Vizvue_type_template_id_1db12965_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Retrieval/Viz.vue?vue&type=template&id=1db12965&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/Viewer.vue?vue&type=template&id=090f8f76&lang=pug&
var Viewervue_type_template_id_090f8f76_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.$colvis.hasViewer())?_c('svg',{ref:"selection",staticClass:"col-retrieval-viewer",class:{ active: _vm.activeUnits.length },style:({ top: (_vm.compTop + "px"), left: (_vm.compLeft + "px"), width: (_vm.width + "px"), height: (_vm.height + "px") })},[_vm._l((_vm.pathPool),function(el,i){return _c('path',{key:i,staticClass:"col-retrieval-viewer__selected",class:{ subject: el.role === 'subject', complement: el.role === 'complement' },attrs:{"transform":_vm.getTransform(el),"d":el.domElement.attributes.d.value}})}),_vm._l((_vm.circlePool),function(el,i){return _c('circle',{key:i,staticClass:"col-retrieval-viewer__selected",class:{ subject: el.role === 'subject', complement: el.role === 'complement' },attrs:{"transform":_vm.getTransform(el),"cx":el.domElement.cx.animVal.value,"cy":el.domElement.cy.animVal.value,"r":el.domElement.r.animVal.value}})}),_vm._l((_vm.rectPool),function(el,i){return _c('rect',{key:i,staticClass:"col-retrieval-viewer__selected",class:{ subject: el.role === 'subject', complement: el.role === 'complement' },attrs:{"transform":_vm.getTransform(el),"x":el.domElement.attributes.x.value,"y":el.domElement.attributes.y.value,"width":el.domElement.attributes.width.value,"height":el.domElement.attributes.height.value}})})],2):_vm._e()}
var Viewervue_type_template_id_090f8f76_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Retrieval/Viewer.vue?vue&type=template&id=090f8f76&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/Viewer.vue?vue&type=script&lang=js&




//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/**
 * Create a new Selector.
 * Selectors are the visual interface that allows analysts to select data units from their visualizations.
 * @extends Vue
 */
/* harmony default export */ var Viewervue_type_script_lang_js_ = ({
  props: {
    /** Whether the visualization has been initialized */
    init: {
      type: Boolean,
      required: true
    },

    /** The list of selected units */
    activeUnits: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    /** The top position of the Selector, in pixels */
    var top = 0;
    /** The left position of the Selector, in pixels */

    var left = 0;
    /** The width of the Selector, in pixels */

    var width = 0;
    /** The height of the Selector, in pixels */

    var height = 0;
    /** Some parent elements of the Selector might be in Position relative  */

    var offsetTop = 0;
    var offsetLeft = 0;
    /** List of event listeners to remove once the component is destroyed */

    var eventListeners = [];
    /** ResizeObserver to make sure that the selector matches the SVG it overlays */

    var resizeObserver = null;
    return {
      top: top,
      left: left,
      width: width,
      height: height,
      offsetTop: offsetTop,
      offsetLeft: offsetLeft,
      eventListeners: eventListeners,
      resizeObserver: resizeObserver
    };
  },
  computed: {
    /** @returns the list of selected DataBinders, either as subjects or complements. */
    pool: function pool() {
      return this.$colvis.getDataBinders(this.activeUnits);
    },

    /** @returns the list of paths within the selected DataBinders. */
    pathPool: function pathPool() {
      return this.pool.filter(function (binder) {
        return binder.domElement.tagName === 'path';
      });
    },

    /** @returns the list of circles within the selected DataBinders. */
    circlePool: function circlePool() {
      return this.pool.filter(function (binder) {
        return binder.domElement.tagName === 'circle';
      });
    },

    /** @returns the list of rectangles within the selected DataBinders. */
    rectPool: function rectPool() {
      return this.pool.filter(function (binder) {
        return binder.domElement.tagName === 'rect';
      });
    },
    compLeft: function compLeft() {
      return this.left - this.offsetLeft;
    },
    compTop: function compTop() {
      return this.top - this.offsetTop;
    }
  },
  methods: {
    /**
     * Return the translation value of a given element, based on its transformation matrix.
     * It allows to restore the element's precise position despite potential transformations
     * implied by parents.
     * @param {DataBinder} element the element whose translate value is required
     */
    getTransform: function getTransform(element) {
      if (!(element.domElement instanceof SVGGraphicsElement)) throw new Error('The selected element is not a SVG element');
      var matrix = element.domElement.getCTM();
      return matrix ? "translate(".concat(matrix.e, ", ").concat(matrix.f, ")") : "";
    },

    /**
     * Get the position of the container of the visualization.
     * Setting { top, left, width, height } properties of the instance.
     */
    getPosition: function getPosition() {
      var _this = this;

      function position() {
        var target = document.querySelector(this.$colvis.getSpecs().visualization.container);

        if (!target) {
          throw new Error('Could not locate a target container for the Selector. Please make sure the specifications is set correctly.');
        }

        var offsetTop = 0;
        var offsetLeft = 0;
        var offsetter = this.$refs.selection.parentElement;

        while (offsetter.offsetParent) {
          offsetTop += offsetter.offsetParent.offsetTop;
          offsetLeft += offsetter.offsetParent.offsetLeft;
          offsetter = offsetter.offsetParent;
        }

        var _target$getBoundingCl = target.getBoundingClientRect(),
            top = _target$getBoundingCl.top,
            left = _target$getBoundingCl.left,
            width = _target$getBoundingCl.width,
            height = _target$getBoundingCl.height;

        Object.assign(this.$data, {
          top: top,
          left: left,
          width: width,
          height: height,
          offsetTop: offsetTop,
          offsetLeft: offsetLeft
        });
      }

      position.call(this);

      if (this.height <= 0) {
        console.warn('Could not attach Selector. Will try again in 1 second.');
        var interval = window.setInterval(function (e) {
          position.call(_this);

          if (_this.height > 0) {
            console.log('Selector attached');
            clearInterval(interval);
          } else console.warn('Could not attach Selector. Will try again in 1 second.');
        }, 1000);
      }
    }
  },
  watch: {
    init: function init() {
      if (this.$colvis.isInit()) this.getPosition();
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    this.getPosition();
    addEventListener('resize', this.getPosition);
    this.eventListeners.push({
      resize: this.getPosition
    });
    /**
     * We resize the selector when the selector's parent's element is resized. We cannot observe
     * SVGs, since there are multiple issues with SVG and bounding boxes.
     */

    this.resizeObserver = new ResizeObserver(function (e) {
      console.log('triggering resize');

      _this2.getPosition();
    });
    this.resizeObserver.observe(this.$refs.selection.parentElement);
  },
  destroyed: function destroyed() {
    this.eventListeners.forEach(function (e) {
      Object.keys(e).forEach(function (key) {
        removeEventListener(key, e[key]);
      });
    });
    this.resizeObserver.disconnect();
  }
});
// CONCATENATED MODULE: ./src/components/Retrieval/Viewer.vue?vue&type=script&lang=js&
 /* harmony default export */ var Retrieval_Viewervue_type_script_lang_js_ = (Viewervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Retrieval/Viewer.vue?vue&type=style&index=0&lang=sass&
var Viewervue_type_style_index_0_lang_sass_ = __webpack_require__("4d5a");

// CONCATENATED MODULE: ./src/components/Retrieval/Viewer.vue






/* normalize component */

var Viewer_component = normalizeComponent(
  Retrieval_Viewervue_type_script_lang_js_,
  Viewervue_type_template_id_090f8f76_lang_pug_render,
  Viewervue_type_template_id_090f8f76_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var Viewer = (Viewer_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/List.vue?vue&type=template&id=57a3a2bb&scoped=true&lang=pug&
var Listvue_type_template_id_57a3a2bb_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('aside',{staticClass:"col-retrieval--viz",class:{ active: _vm.viewing },style:({ width: ("calc(25rem + " + _vm.width + "px)")})},[_c('header',{staticClass:"col-retrieval--viz__header"},[_c('h3',[_vm._v(_vm._s(_vm.filteredAnnotations.length)+" Annotations")]),_c('div',{staticClass:"spacer"}),_c('button',{attrs:{"icon":""},on:{"mousedown":function($event){$event.stopPropagation();return _vm.$emit('close')}}},[_vm._v("Close")])]),_c('c-toolbar',{attrs:{"unit":_vm.unit,"tag":_vm.tag,"pattern":_vm.pattern,"annotations":_vm.annotations,"loi":_vm.loi,"activeAnnotation":_vm.activeAnnotation,"state":""},on:{"removeSort":_vm.removeSort,"search":function($event){_vm.needle = $event}}}),_c('button',{staticClass:"col-retrieval--viz__handle",on:{"mousedown":_vm.start,"mousemove":_vm.move,"mouseup":_vm.stop}}),_c('transition-group',{ref:"list",staticClass:"annotations-list",attrs:{"name":"flip-list","tag":"ul"}},_vm._l((_vm.pagedAnnotations),function(annotation){return _c('li',{key:annotation.meta.timestamp,staticClass:"annotations-list__item",class:{ linked: _vm.isChild(annotation) || _vm.isParent(annotation) || (annotation === _vm.activeAnnotation && (_vm.activeParent || _vm.answersToActive.length)), child: _vm.isChild(annotation), parent: _vm.isParent(annotation) }},[_c('c-card',{attrs:{"annotation":annotation,"activeAnnotation":_vm.activeAnnotation,"filter":true,"replyTo":_vm.replyTo,"canReply":_vm.canReply},on:{"highlight":function($event){return _vm.$emit('highlight', $event)},"selectAnnotation":_vm.selectAnnotation,"tag":_vm.sortByTag,"unit":_vm.sortByUnit,"pattern":_vm.sortByPattern,"loi":_vm.sortByLoi,"avatar":function($event){return _vm.$emit('avatar', $event)},"reply":function($event){return _vm.$emit('reply', $event)},"endorse":function($event){return _vm.$emit('endorse', $event)},"funny":function($event){return _vm.$emit('funny', $event)}},scopedSlots:_vm._u([{key:"extension",fn:function(){return [_vm._t("card")]},proxy:true}],null,true)})],1)}),0),_c('transition',{attrs:{"name":"fade"}},[_c('button',{directives:[{name:"show",rawName:"v-show",value:(_vm.numPages < _vm.pages),expression:"numPages < pages"}],staticClass:"more btn primary",on:{"click":function($event){_vm.numPages += 1}}},[_vm._v("show more")])])],1)}
var Listvue_type_template_id_57a3a2bb_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Retrieval/List.vue?vue&type=template&id=57a3a2bb&scoped=true&lang=pug&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.array.sort.js
var es6_array_sort = __webpack_require__("55dd");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_retrieval/CToolbar.vue?vue&type=template&id=54c78777&scoped=true&lang=pug&
var CToolbarvue_type_template_id_54c78777_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('nav',{staticClass:"toolbar"},[_c('div',{staticClass:"toolbar__search"},[_c('label',[_c('search-icon'),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.needle),expression:"needle"}],staticClass:"toolbar__search__input",attrs:{"type":"search","placeholder":"Search for an annotation"},domProps:{"value":(_vm.needle)},on:{"input":function($event){if($event.target.composing){ return; }_vm.needle=$event.target.value}}})],1)]),_c('div',{staticClass:"toolbar__sort"},[_c('span',[_vm._v("Sort by:")]),_c('c-chip',{attrs:{"inactive":""}},[_vm._v("Time")]),(_vm.state)?_c('c-chip',{attrs:{"inactive":""}},[_vm._v("Current state")]):_vm._e(),(_vm.unit)?_c('c-chip',{attrs:{"closeable":""},on:{"close":function($event){return _vm.$emit('removeSort', 'unit')}}},[_vm._v("Unit: "+_vm._s(_vm.unit.title))]):_vm._e(),(_vm.tag)?_c('c-chip',{attrs:{"closeable":""},on:{"close":function($event){return _vm.$emit('removeSort', 'tag')}}},[_vm._v("Tag: "+_vm._s(_vm.tag))]):_vm._e(),(_vm.pattern)?_c('c-chip',{attrs:{"closeable":""},on:{"close":function($event){return _vm.$emit('removeSort', 'pattern')}}},[_vm._v("Pattern: "+_vm._s(_vm.pattern))]):_vm._e(),(_vm.loi)?_c('c-chip',{attrs:{"closeable":""},on:{"close":function($event){return _vm.$emit('removeSort', 'loi')}}},[_vm._v("LOI: "+_vm._s(_vm.loi))]):_vm._e(),(_vm.activeAnnotation)?_c('c-chip',{attrs:{"closeable":""},on:{"close":function($event){return _vm.$emit('removeSort', 'selectAnnotation')}}},[_vm._v("Selected annotation")]):_vm._e()],1)])}
var CToolbarvue_type_template_id_54c78777_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_retrieval/CToolbar.vue?vue&type=template&id=54c78777&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/SearchIcon.vue?vue&type=template&id=a4d05a9a&lang=pug&
var SearchIconvue_type_template_id_a4d05a9a_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ 'vertical-align': 'middle', fill: _vm.color }),attrs:{"width":_vm.size,"height":_vm.size,"viewBox":"0 0 24 24"},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_c('path',{attrs:{"d":"M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z"}})])}
var SearchIconvue_type_template_id_a4d05a9a_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/SearchIcon.vue?vue&type=template&id=a4d05a9a&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/SearchIcon.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
/* harmony default export */ var SearchIconvue_type_script_lang_js_ = ({
  props: {
    color: {
      type: String,
      default: 'white'
    },
    size: {
      type: Number,
      default: 24
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/SearchIcon.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_SearchIconvue_type_script_lang_js_ = (SearchIconvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/SearchIcon.vue





/* normalize component */

var SearchIcon_component = normalizeComponent(
  _generic_SearchIconvue_type_script_lang_js_,
  SearchIconvue_type_template_id_a4d05a9a_lang_pug_render,
  SearchIconvue_type_template_id_a4d05a9a_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var SearchIcon = (SearchIcon_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_retrieval/CToolbar.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var CToolbarvue_type_script_lang_js_ = ({
  components: {
    CChip: CChip,
    SearchIcon: SearchIcon
  },
  props: {
    unit: {
      type: Object
    },
    tag: {
      type: String
    },
    pattern: {
      type: String
    },
    loi: {
      type: String
    },
    activeAnnotation: {
      type: Object
    },
    state: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      needle: null
    };
  },
  watch: {
    needle: function needle() {
      this.$emit('search', this.needle);
    }
  }
});
// CONCATENATED MODULE: ./src/components/_retrieval/CToolbar.vue?vue&type=script&lang=js&
 /* harmony default export */ var _retrieval_CToolbarvue_type_script_lang_js_ = (CToolbarvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_retrieval/CToolbar.vue?vue&type=style&index=0&id=54c78777&lang=sass&scoped=true&
var CToolbarvue_type_style_index_0_id_54c78777_lang_sass_scoped_true_ = __webpack_require__("4730");

// CONCATENATED MODULE: ./src/components/_retrieval/CToolbar.vue






/* normalize component */

var CToolbar_component = normalizeComponent(
  _retrieval_CToolbarvue_type_script_lang_js_,
  CToolbarvue_type_template_id_54c78777_scoped_true_lang_pug_render,
  CToolbarvue_type_template_id_54c78777_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "54c78777",
  null
  
)

/* harmony default export */ var CToolbar = (CToolbar_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/CCard.vue?vue&type=template&id=1a43f82e&scoped=true&lang=pug&
var CCardvue_type_template_id_1a43f82e_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"card",class:{ active: _vm.active, 'max-width': _vm.maxWidth }},[_c('section',{staticClass:"body",on:{"click":function($event){return _vm.$emit('selectAnnotation', _vm.annotation)}}},[_c('header',[_c('c-logo',{attrs:{"color":_vm.annotation.patterns ? 'red' : 'blue'}}),_c('span',[_vm._v(_vm._s(_vm.annotation.patterns ? 'structured' : 'free')+" annotation")]),_c('div',{staticClass:"spacer"}),(_vm.annotation.patterns)?_c('div',{staticClass:"patterns"},[(_vm.annotation.patterns.singularity && !_vm.annotation.patterns.generality)?_c('c-singularity',{on:{"click":function($event){return _vm.$emit('pattern','singularity')}}}):_vm._e(),(_vm.annotation.patterns.duality)?_c('c-duality',{on:{"click":function($event){return _vm.$emit('pattern','duality')}}}):_vm._e(),(_vm.annotation.patterns.generality)?_c('c-generality',{on:{"click":function($event){return _vm.$emit('pattern','generality')}}}):_vm._e()],1):_vm._e()],1),_c('c-text',{attrs:{"annotation":_vm.annotation,"activeAnnotation":_vm.activeAnnotation,"filter":_vm.filter,"agDetails":""},on:{"highlight":function($event){return _vm.$emit('highlight', $event)},"unit":function($event){return _vm.$emit('unit', $event)}}}),_c('footer',{staticClass:"tags"},_vm._l((_vm.annotation.tags),function(tag){return _c('c-chip',{key:tag.id || tag,attrs:{"small":true},on:{"click":function($event){return _vm.$emit('tag', tag.name || tag)}},scopedSlots:_vm._u([{key:"prepend",fn:function(){return [_c('tag-icon')]},proxy:true}],null,true)},[_vm._v(_vm._s(tag.name || tag))])}),1),_c('small',{staticClass:"date"},[_vm._v(_vm._s((new Date(+_vm.annotation.meta.timestamp)).toLocaleString()))])],1),(_vm.hasFooter)?_c('section',{staticClass:"footer"},[(_vm.annotation.social.author)?_c('div',{staticClass:"social"},[_c('aside',{staticClass:"social__avatar",on:{"click":function($event){return _vm.$emit('avatar', _vm.annotation.social.author.id)}}},[(_vm.annotation.social.author.avatar)?_c('img',{attrs:{"src":_vm.annotation.social.author.avatar}}):_c('div',[_vm._v(_vm._s(_vm.annotation.social.author.name.substr(0, 1)))])]),_c('span',[_vm._v(_vm._s(_vm.annotation.social.author.name))])]):_vm._e(),_c('div',{staticClass:"actions"},[(_vm.canReply)?_c('reply-icon',{staticClass:"reply",class:{ selected: _vm.replying },attrs:{"color":null,"size":48},on:{"click":function($event){return _vm.$emit('reply', _vm.annotation)}}}):_vm._e(),_c('thumb-icon',{class:{ selected: _vm.annotation.social.endorsed },attrs:{"color":null,"size":48},on:{"click":function($event){return _vm.$emit('endorse', _vm.annotation)}}}),_c('funny-icon',{class:{ selected: _vm.annotation.social.funny },attrs:{"color":null,"size":48},on:{"click":function($event){return _vm.$emit('funny', _vm.annotation)}}})],1),_c('div',{staticClass:"stats"},[(_vm.annotation.replyTo)?_c('small',[_vm._v("this annotation is a reply of type \""+_vm._s(_vm.annotation.replyType)+"\"")]):_vm._e(),(_vm.replies.length)?_c('small',[_vm._v("this annotation has "+_vm._s(_vm.replies.length)+" replies")]):_vm._e()])]):_vm._e(),_vm._t("extension",null,{"annotation":_vm.annotation})],2)}
var CCardvue_type_template_id_1a43f82e_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Retrieval/CCard.vue?vue&type=template&id=1a43f82e&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CLogo.vue?vue&type=template&id=c5369ede&lang=pug&
var CLogovue_type_template_id_c5369ede_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{staticStyle:{"fill-rule":"evenodd","clip-rule":"evenodd","stroke-linejoin":"round","stroke-miterlimit":"2"},attrs:{"viewBox":"0 0 263 263","width":_vm.size,"height":_vm.size,"version":"1.1","xmlns":"http://www.w3.org/2000/svg","xmlns:xlink":"http://www.w3.org/1999/xlink","xml:space":"preserve","xmlns:serif":"http://www.serif.com/"}},[_c('g',{attrs:{"id":"Information-Visualization","serif:id":"Information Visualization"}},[_c('path',{style:({ fill: _vm.color, 'fill-rule': 'nonzero' }),attrs:{"d":"M180.032,157.426l-20.675,0l0,-20.678l20.675,0l0,20.678Zm-81.778,-50.127c-8.924,0 -16.185,-7.264 -16.185,-16.189c0,-8.923 7.261,-16.188 16.185,-16.188c8.928,0 16.188,7.265 16.188,16.188c0,8.925 -7.26,16.189 -16.188,16.189m16.188,64.313c0,8.923 -7.26,16.188 -16.188,16.188c-8.924,0 -16.185,-7.265 -16.185,-16.188c0,-8.925 7.261,-16.183 16.185,-16.183c8.928,0 16.188,7.258 16.188,16.183m39.403,-53.681l-21.097,-21.093c0.31,-1.868 0.511,-3.773 0.511,-5.728c0,-19.299 -15.702,-35.004 -35.005,-35.004c-19.3,0 -35.001,15.705 -35.001,35.004c0,16.239 11.131,29.891 26.151,33.827l0,12.854c-15.02,3.934 -26.151,17.582 -26.151,33.821c0,19.299 15.701,35.003 35.001,35.003c19.303,0 35.005,-15.704 35.005,-35.003c0,-15.838 -10.581,-29.231 -25.039,-33.534l0,-13.428c6.149,-1.831 11.573,-5.318 15.802,-9.931l16.519,16.517l0,45.006l58.307,0l0,-58.311l-45.003,0Z"}}),_c('path',{style:({ fill: _vm.color, 'fill-rule': 'nonzero' }),attrs:{"d":"M219.458,219.458l-176.817,0l0,-176.815l176.817,0l0,176.815Zm-219.458,42.643l262.101,0l0,-262.101l-262.101,0l0,262.101Z"}})])])}
var CLogovue_type_template_id_c5369ede_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/CLogo.vue?vue&type=template&id=c5369ede&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CLogo.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
/* harmony default export */ var CLogovue_type_script_lang_js_ = ({
  props: {
    size: {
      type: Number,
      default: 32
    },
    color: {
      type: String,
      default: '#f32885'
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/CLogo.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_CLogovue_type_script_lang_js_ = (CLogovue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/CLogo.vue





/* normalize component */

var CLogo_component = normalizeComponent(
  _generic_CLogovue_type_script_lang_js_,
  CLogovue_type_template_id_c5369ede_lang_pug_render,
  CLogovue_type_template_id_c5369ede_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var CLogo = (CLogo_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CSingularity.vue?vue&type=template&id=ab2b2f80&lang=pug&
var CSingularityvue_type_template_id_ab2b2f80_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ fill: _vm.color }),attrs:{"viewBox":"0 0 187 124","width":_vm.size,"height":_vm.size},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_c('path',{style:({ opacity: .5 }),attrs:{"d":"M17.609,61.632c0,-46.755 107.173,-61.632 107.173,-61.632l0,123.264c0,0 -107.173,-14.877 -107.173,-61.632Z"}}),_c('circle',{attrs:{"cx":"17.609","cy":"61.632","r":"17.609"}}),_c('circle',{attrs:{"cx":"124.782","cy":"61.632","r":"61.632"}})])}
var CSingularityvue_type_template_id_ab2b2f80_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/CSingularity.vue?vue&type=template&id=ab2b2f80&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CSingularity.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
/* harmony default export */ var CSingularityvue_type_script_lang_js_ = ({
  props: {
    size: {
      type: Number,
      default: 32
    },
    color: {
      type: String,
      default: '#616979'
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/CSingularity.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_CSingularityvue_type_script_lang_js_ = (CSingularityvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/CSingularity.vue





/* normalize component */

var CSingularity_component = normalizeComponent(
  _generic_CSingularityvue_type_script_lang_js_,
  CSingularityvue_type_template_id_ab2b2f80_lang_pug_render,
  CSingularityvue_type_template_id_ab2b2f80_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var CSingularity = (CSingularity_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CDuality.vue?vue&type=template&id=0f520b2c&lang=pug&
var CDualityvue_type_template_id_0f520b2c_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ fill: _vm.color, stroke: _vm.color, 'stroke-width': 0 }),attrs:{"viewBox":"0 0 24 24","width":_vm.size,"height":_vm.size},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_c('line',{style:({ 'stroke-width': 8, opacity: .5 }),attrs:{"x1":"8","x2":"16","y1":"12","y2":"12"}}),_c('circle',{attrs:{"cx":"5","cy":"12","r":"5"}}),_c('circle',{attrs:{"cx":"19","cy":"12","r":"5"}})])}
var CDualityvue_type_template_id_0f520b2c_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/CDuality.vue?vue&type=template&id=0f520b2c&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CDuality.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
/* harmony default export */ var CDualityvue_type_script_lang_js_ = ({
  props: {
    size: {
      type: Number,
      default: 32
    },
    color: {
      type: String,
      default: '#616979'
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/CDuality.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_CDualityvue_type_script_lang_js_ = (CDualityvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/CDuality.vue





/* normalize component */

var CDuality_component = normalizeComponent(
  _generic_CDualityvue_type_script_lang_js_,
  CDualityvue_type_template_id_0f520b2c_lang_pug_render,
  CDualityvue_type_template_id_0f520b2c_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var CDuality = (CDuality_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CGenerality.vue?vue&type=template&id=5412edbf&lang=pug&
var CGeneralityvue_type_template_id_5412edbf_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ fill: _vm.color, stroke: _vm.color, 'stroke-width': 0 }),attrs:{"viewBox":"0 0 124 124","width":_vm.size,"height":_vm.size},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_c('circle',{style:({ opacity: .5 }),attrs:{"cx":"61.632","cy":"61.632","r":"61.632"}}),_c('circle',{attrs:{"cx":"61.632","cy":"19.609","r":"17.609"}}),_c('circle',{attrs:{"cx":"61.632","cy":"103.655","r":"17.609"}}),_c('circle',{attrs:{"cx":"61.632","cy":"61.632","r":"17.609"}}),_c('circle',{attrs:{"cx":"103.655","cy":"61.632","r":"17.609"}}),_c('circle',{attrs:{"cx":"19.609","cy":"61.632","r":"17.609"}})])}
var CGeneralityvue_type_template_id_5412edbf_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/CGenerality.vue?vue&type=template&id=5412edbf&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/CGenerality.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var CGeneralityvue_type_script_lang_js_ = ({
  props: {
    size: {
      type: Number,
      default: 32
    },
    color: {
      type: String,
      default: '#616979'
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/CGenerality.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_CGeneralityvue_type_script_lang_js_ = (CGeneralityvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/CGenerality.vue





/* normalize component */

var CGenerality_component = normalizeComponent(
  _generic_CGeneralityvue_type_script_lang_js_,
  CGeneralityvue_type_template_id_5412edbf_lang_pug_render,
  CGeneralityvue_type_template_id_5412edbf_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var CGenerality = (CGenerality_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/TagIcon.vue?vue&type=template&id=75f4b503&lang=pug&
var TagIconvue_type_template_id_75f4b503_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ 'enable-background': 'new 0 0 24 24', 'vertical-align': 'middle', fill: _vm.color }),attrs:{"x":"0px","y":"0px","width":_vm.size,"height":_vm.size,"viewBox":"0 0 24 24","xml:space":"preserve"},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_c('g',{staticStyle:{"opacity":"0.75"},attrs:{"id":"tag"}},[_c('path',{attrs:{"d":"M18.748,11.717c0.389,0.389,0.389,1.025,0,1.414l-4.949,4.95c-0.389,0.389-1.025,0.389-1.414,0l-6.01-6.01 c-0.389-0.389-0.707-1.157-0.707-1.707L5.667,6c0-0.55,0.45-1,1-1h4.364c0.55,0,1.318,0.318,1.707,0.707L18.748,11.717z M8.104,7.456C7.525,8.032,7.526,8.97,8.103,9.549c0.578,0.577,1.516,0.577,2.095,0.001c0.576-0.578,0.576-1.517,0-2.095 C9.617,6.879,8.68,6.878,8.104,7.456z"}})])])}
var TagIconvue_type_template_id_75f4b503_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/TagIcon.vue?vue&type=template&id=75f4b503&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/TagIcon.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var TagIconvue_type_script_lang_js_ = ({
  props: {
    color: {
      type: String,
      default: 'white'
    },
    size: {
      type: Number,
      default: 24
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/TagIcon.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_TagIconvue_type_script_lang_js_ = (TagIconvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/TagIcon.vue





/* normalize component */

var TagIcon_component = normalizeComponent(
  _generic_TagIconvue_type_script_lang_js_,
  TagIconvue_type_template_id_75f4b503_lang_pug_render,
  TagIconvue_type_template_id_75f4b503_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var TagIcon = (TagIcon_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/ReplyIcon.vue?vue&type=template&id=220842f0&lang=pug&
var ReplyIconvue_type_template_id_220842f0_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ 'vertical-align': 'middle', fill: _vm.color }),attrs:{"width":_vm.size,"height":_vm.size,"viewBox":"0 0 24 24"},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_c('path',{attrs:{"d":"M10,9V5L3,12L10,19V14.9C15,14.9 18.5,16.5 21,20C20,15 17,10 10,9Z"}})])}
var ReplyIconvue_type_template_id_220842f0_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/ReplyIcon.vue?vue&type=template&id=220842f0&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/ReplyIcon.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
/* harmony default export */ var ReplyIconvue_type_script_lang_js_ = ({
  props: {
    color: {
      type: String,
      default: 'white'
    },
    size: {
      type: Number,
      default: 24
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/ReplyIcon.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_ReplyIconvue_type_script_lang_js_ = (ReplyIconvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/ReplyIcon.vue





/* normalize component */

var ReplyIcon_component = normalizeComponent(
  _generic_ReplyIconvue_type_script_lang_js_,
  ReplyIconvue_type_template_id_220842f0_lang_pug_render,
  ReplyIconvue_type_template_id_220842f0_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var ReplyIcon = (ReplyIcon_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/ThumbIcon.vue?vue&type=template&id=2f9fdfe4&lang=pug&
var ThumbIconvue_type_template_id_2f9fdfe4_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ 'vertical-align': 'middle', fill: _vm.color }),attrs:{"width":_vm.size,"height":_vm.size,"viewBox":"0 0 24 24"},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_c('path',{attrs:{"d":"M23,10C23,8.89 22.1,8 21,8H14.68L15.64,3.43C15.66,3.33 15.67,3.22 15.67,3.11C15.67,2.7 15.5,2.32 15.23,2.05L14.17,1L7.59,7.58C7.22,7.95 7,8.45 7,9V19A2,2 0 0,0 9,21H18C18.83,21 19.54,20.5 19.84,19.78L22.86,12.73C22.95,12.5 23,12.26 23,12V10M1,21H5V9H1V21Z"}})])}
var ThumbIconvue_type_template_id_2f9fdfe4_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/ThumbIcon.vue?vue&type=template&id=2f9fdfe4&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/ThumbIcon.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
/* harmony default export */ var ThumbIconvue_type_script_lang_js_ = ({
  props: {
    color: {
      type: String,
      default: 'white'
    },
    size: {
      type: Number,
      default: 24
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/ThumbIcon.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_ThumbIconvue_type_script_lang_js_ = (ThumbIconvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/ThumbIcon.vue





/* normalize component */

var ThumbIcon_component = normalizeComponent(
  _generic_ThumbIconvue_type_script_lang_js_,
  ThumbIconvue_type_template_id_2f9fdfe4_lang_pug_render,
  ThumbIconvue_type_template_id_2f9fdfe4_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var ThumbIcon = (ThumbIcon_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/FunnyIcon.vue?vue&type=template&id=c62effc0&lang=pug&
var FunnyIconvue_type_template_id_c62effc0_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{style:({ 'vertical-align': 'middle', fill: _vm.color }),attrs:{"width":_vm.size,"height":_vm.size,"viewBox":"0 0 24 24"},on:{"click":function($event){$event.stopPropagation();return _vm.$emit('click', $event)}}},[_c('path',{attrs:{"d":"M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22A10,10 0 0,0 22,12C22,6.47 17.5,2 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M13,9.94L14.06,11L15.12,9.94L16.18,11L17.24,9.94L15.12,7.82L13,9.94M8.88,9.94L9.94,11L11,9.94L8.88,7.82L6.76,9.94L7.82,11L8.88,9.94M12,17.5C14.33,17.5 16.31,16.04 17.11,14H6.89C7.69,16.04 9.67,17.5 12,17.5Z"}})])}
var FunnyIconvue_type_template_id_c62effc0_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_generic/FunnyIcon.vue?vue&type=template&id=c62effc0&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_generic/FunnyIcon.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
/* harmony default export */ var FunnyIconvue_type_script_lang_js_ = ({
  props: {
    color: {
      type: String,
      default: 'white'
    },
    size: {
      type: Number,
      default: 24
    }
  }
});
// CONCATENATED MODULE: ./src/components/_generic/FunnyIcon.vue?vue&type=script&lang=js&
 /* harmony default export */ var _generic_FunnyIconvue_type_script_lang_js_ = (FunnyIconvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/_generic/FunnyIcon.vue





/* normalize component */

var FunnyIcon_component = normalizeComponent(
  _generic_FunnyIconvue_type_script_lang_js_,
  FunnyIconvue_type_template_id_c62effc0_lang_pug_render,
  FunnyIconvue_type_template_id_c62effc0_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var FunnyIcon = (FunnyIcon_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/CCard.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//











/* harmony default export */ var CCardvue_type_script_lang_js_ = ({
  components: {
    CText: CText,
    CChip: CChip,
    CLogo: CLogo,
    CSingularity: CSingularity,
    CDuality: CDuality,
    CGenerality: CGenerality,
    TagIcon: TagIcon,
    ReplyIcon: ReplyIcon,
    ThumbIcon: ThumbIcon,
    FunnyIcon: FunnyIcon
  },
  props: {
    annotation: {
      type: Object,
      required: true
    },
    activeAnnotation: {
      type: Object
    },
    maxWidth: {
      type: Boolean,
      default: false
    },
    filter: {
      type: Boolean,
      default: false
    },
    canReply: {
      type: Boolean,
      default: true
    },
    replyTo: {
      type: Object
    },
    replies: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  methods: {
    strfy: strfy
  },
  computed: {
    active: function active() {
      return this.annotation === this.activeAnnotation;
    },
    replying: function replying() {
      return this.annotation === this.replyTo;
    },
    hasFooter: function hasFooter() {
      return this.annotation.social || this.$colvis.getEndorsement;
    },
    loi: function loi() {
      var loi = '';

      for (var prop in this.annotation.loi) {
        if (this.annotation.loi[prop]) loi += "".concat(prop, " ");
      }

      return loi.substr(0, loi.length - 1);
    }
  }
});
// CONCATENATED MODULE: ./src/components/Retrieval/CCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var Retrieval_CCardvue_type_script_lang_js_ = (CCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Retrieval/CCard.vue?vue&type=style&index=0&id=1a43f82e&lang=sass&scoped=true&
var CCardvue_type_style_index_0_id_1a43f82e_lang_sass_scoped_true_ = __webpack_require__("419d");

// CONCATENATED MODULE: ./src/components/Retrieval/CCard.vue






/* normalize component */

var CCard_component = normalizeComponent(
  Retrieval_CCardvue_type_script_lang_js_,
  CCardvue_type_template_id_1a43f82e_scoped_true_lang_pug_render,
  CCardvue_type_template_id_1a43f82e_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "1a43f82e",
  null
  
)

/* harmony default export */ var CCard = (CCard_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/List.vue?vue&type=script&lang=js&







//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var Listvue_type_script_lang_js_ = ({
  components: {
    CToolbar: CToolbar,
    CCard: CCard
  },
  props: {
    /**
     * The current state of the application's visualization.
     * Used to display relevant annotations
     */
    state: {
      type: Object,
      default: function _default() {}
    },

    /** The list of prior annotations */
    annotations: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** Whether the list should be visible */
    viewing: {
      type: Boolean,
      default: false
    },
    activeAnnotation: {
      type: Object
    },
    replyTo: {
      type: Object
    },
    canReply: {
      type: Boolean,
      default: true
    },
    activeParent: {
      type: Object
    },
    answersToActive: {
      type: Array
    }
  },
  data: function data() {
    return {
      open: false,
      dragging: false,
      initialX: null,
      width: 0,
      eventListeners: [],
      unit: null,
      tag: null,
      loi: null,
      pattern: null,
      needle: null,
      aByPage: 5,
      numPages: 1
    };
  },
  methods: {
    start: function start(event) {
      this.initialX = event.clientX;
      this.dragging = true;
    },
    stop: function stop(event) {
      this.initialX = null;
      this.dragging = false;
    },
    move: function move(event) {
      if (this.dragging) {
        this.width += this.initialX - event.clientX;
        this.initialX = event.clientX;
      }
    },
    sortByTag: function sortByTag(tag) {
      this.tag = this.tag === tag ? null : tag;
    },
    sortByLoi: function sortByLoi(loi) {
      this.loi = this.loi === loi ? null : loi;
    },
    sortByPattern: function sortByPattern(pattern) {
      this.pattern = this.pattern === pattern ? null : pattern;
    },
    sortByUnit: function sortByUnit(unit) {
      var _this$unit;

      this.unit = ((_this$unit = this.unit) === null || _this$unit === void 0 ? void 0 : _this$unit.title) === (unit === null || unit === void 0 ? void 0 : unit.title) ? null : unit;
    },
    selectAnnotation: function selectAnnotation(annotation) {
      this.$emit('selectAnnotation', annotation);
      document.querySelector('.annotations-list').scrollIntoView({
        behavior: 'smooth'
      });
    },
    removeSort: function removeSort(variable) {
      if (variable === 'selectAnnotation') this.$emit('selectAnnotation');else this[variable] = null;
    },
    isParent: function isParent(annotation) {
      var _this$activeParent;

      return annotation.id === (((_this$activeParent = this.activeParent) === null || _this$activeParent === void 0 ? void 0 : _this$activeParent.id) || this.activeParent);
    },
    isChild: function isChild(annotation) {
      var _this$answersToActive;

      return (_this$answersToActive = this.answersToActive) === null || _this$answersToActive === void 0 ? void 0 : _this$answersToActive.map(function (a) {
        return a.id;
      }).includes(annotation.id);
    }
  },
  computed: {
    pages: function pages() {
      return Math.ceil(this.filteredAnnotations.length / this.aByPage);
    },
    selectedSubjects: function selectedSubjects() {
      if (!this.activeAnnotation) return [];else {
        return this.$colvis.getAnnotationDataBinders(this.activeAnnotation);
      }
    },
    filteredAnnotations: function filteredAnnotations() {
      if (!this.needle || this.needle.length < 3) return this.annotations;else {
        var n = this.needle.toLowerCase();
        return this.annotations.filter(function (a) {
          var _a$social, _a$social$author, _a$tags, _haystacks2;

          var haystacks = [];
          var valid = false;
          if (a.rawAnnotation) haystacks.push(a.rawAnnotation.text);
          if (a.text) haystacks.push(a.text);

          if (a.dataUnits) {
            a.dataUnits.forEach(function (unit) {
              var _haystacks;

              if (!unit.aggregated) haystacks.push(unit.title);else (_haystacks = haystacks).push.apply(_haystacks, _toConsumableArray(unit.units.map(function (subunit) {
                return subunit.title;
              })));
            });
          }

          if ((_a$social = a.social) === null || _a$social === void 0 ? void 0 : (_a$social$author = _a$social.author) === null || _a$social$author === void 0 ? void 0 : _a$social$author.name) haystacks.push(a.social.author.name);
          if ((_a$tags = a.tags) === null || _a$tags === void 0 ? void 0 : _a$tags.length) (_haystacks2 = haystacks).push.apply(_haystacks2, _toConsumableArray(a.tags.map(function (t) {
            return t.name || t;
          })));
          haystacks = haystacks.filter(function (h) {
            return h;
          });
          return haystacks.reduce(function (valid, haystack) {
            return haystack.toLowerCase().includes(n) ? true : valid;
          }, valid);
        });
      }
    },
    sortedAnnotations: function sortedAnnotations() {
      var _this = this;

      var clone = _toConsumableArray(this.filteredAnnotations);

      var state = this.$colvis.getVizState();
      clone.forEach(function (a) {
        var s = a.meta.state;
        var sim = 0;

        for (var prop in s) {
          if (s[prop] === state[prop]) sim += 1;
        }

        a.sim = sim;
      });
      var sortFns = []; // Active annotation on top

      sortFns.push(function (a, b) {
        var _this$activeParent2, _this$activeParent3;

        return a.id === (((_this$activeParent2 = _this.activeParent) === null || _this$activeParent2 === void 0 ? void 0 : _this$activeParent2.id) || _this.activeParent) ? -1 : b.id === (((_this$activeParent3 = _this.activeParent) === null || _this$activeParent3 === void 0 ? void 0 : _this$activeParent3.id) || _this.activeParent) ? 1 : 0;
      });
      sortFns.push(function (a, b) {
        return a === _this.activeAnnotation ? -1 : b === _this.activeAnnotation ? 1 : 0;
      });
      sortFns.push(function (a, b) {
        var _this$answersToActive2, _this$answersToActive3;

        return ((_this$answersToActive2 = _this.answersToActive) === null || _this$answersToActive2 === void 0 ? void 0 : _this$answersToActive2.map(function (annotation) {
          return annotation.id;
        }).includes(a.id)) ? -1 : ((_this$answersToActive3 = _this.answersToActive) === null || _this$answersToActive3 === void 0 ? void 0 : _this$answersToActive3.map(function (annotation) {
          return annotation.id;
        }).includes(b.id)) ? 1 : 0;
      }); // Then sort by level of interpretation

      if (this.loi) sortFns.push(function (a, b) {
        var levels = _this.loi.split(' ');

        function hasLevels(annotation, levels) {
          var valid = true;
          levels.forEach(function (l) {
            var _a$loi;

            return valid = ((_a$loi = a.loi) === null || _a$loi === void 0 ? void 0 : _a$loi[l]) ? valid : false;
          });
          return valid;
        }

        if (hasLevels(a, levels) && !hasLevels(b, levels)) return -1;else if (!hasLevels(a, levels) && hasLevels(b, levels)) return 1;else return 0;
      }); // Then by patterns

      if (this.pattern) sortFns.push(function (a, b) {
        var _a$patterns, _b$patterns, _a$patterns2, _b$patterns2;

        if (((_a$patterns = a.patterns) === null || _a$patterns === void 0 ? void 0 : _a$patterns[_this.pattern]) && !((_b$patterns = b.patterns) === null || _b$patterns === void 0 ? void 0 : _b$patterns[_this.pattern])) return -1;else if (!((_a$patterns2 = a.patterns) === null || _a$patterns2 === void 0 ? void 0 : _a$patterns2[_this.pattern]) && ((_b$patterns2 = b.patterns) === null || _b$patterns2 === void 0 ? void 0 : _b$patterns2[_this.pattern])) return 1;else return 0;
      }); // Then sort by tag

      if (this.tag) sortFns.push(function (a, b) {
        if (a.tags.map(function (t) {
          return t.name || t;
        }).includes(_this.tag) && !b.tags.map(function (t) {
          return t.name || t;
        }).includes(_this.tag)) return -1;else if (!a.tags.map(function (t) {
          return t.name || t;
        }).includes(_this.tag) && b.tags.map(function (t) {
          return t.name || t;
        }).includes(_this.tag)) return 1;else return 0;
      }); // Then by unit

      if (this.unit) sortFns.push(function (a, b) {
        var _a$dataUnits, _b$dataUnits, _a$dataUnits2, _b$dataUnits2;

        if (((_a$dataUnits = a.dataUnits) === null || _a$dataUnits === void 0 ? void 0 : _a$dataUnits.map(function (u) {
          return u.title;
        }).includes(_this.unit.title)) && !((_b$dataUnits = b.dataUnits) === null || _b$dataUnits === void 0 ? void 0 : _b$dataUnits.map(function (u) {
          return u.title;
        }).includes(_this.unit.title))) return -1;else if (!((_a$dataUnits2 = a.dataUnits) === null || _a$dataUnits2 === void 0 ? void 0 : _a$dataUnits2.map(function (u) {
          return u.title;
        }).includes(_this.unit.title)) && ((_b$dataUnits2 = b.dataUnits) === null || _b$dataUnits2 === void 0 ? void 0 : _b$dataUnits2.map(function (u) {
          return u.title;
        }).includes(_this.unit.title))) return 1;else return 0;
      }); // Then by state

      sortFns.push(function (a, b) {
        if (a.sim > b.sim) return -1;else if (a.sim < b.sim) return 1;else return 0;
      }); // Then by time

      sortFns.push(function (a, b) {
        return a.meta.timestamp > b.meta.timestamp ? -1 : 1;
      });
      clone.sort(function (a, b) {
        var value = 1;

        for (var i = 0; i < sortFns.length; i++) {
          value = sortFns[i](a, b);
          if (value !== 0) return value;
        }

        return value;
      });
      return clone;
    },
    pagedAnnotations: function pagedAnnotations() {
      var _this2 = this;

      return this.sortedAnnotations.filter(function (a, i) {
        return i < _this2.aByPage * _this2.numPages;
      });
    }
  },
  mounted: function mounted() {
    this.eventListeners.push({
      type: 'mousemove',
      handler: addEventListener('mousemove', this.move)
    });
    this.eventListeners.push({
      type: 'mouseup',
      handler: addEventListener('mouseup', this.stop)
    });
  },
  destroyed: function destroyed() {
    this.eventListeners.forEach(function (e) {
      return removeEventListener(e.type, e.handler);
    });
  }
});
// CONCATENATED MODULE: ./src/components/Retrieval/List.vue?vue&type=script&lang=js&
 /* harmony default export */ var Retrieval_Listvue_type_script_lang_js_ = (Listvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Retrieval/List.vue?vue&type=style&index=0&id=57a3a2bb&lang=sass&scoped=true&
var Listvue_type_style_index_0_id_57a3a2bb_lang_sass_scoped_true_ = __webpack_require__("70f0");

// CONCATENATED MODULE: ./src/components/Retrieval/List.vue






/* normalize component */

var List_component = normalizeComponent(
  Retrieval_Listvue_type_script_lang_js_,
  Listvue_type_template_id_57a3a2bb_scoped_true_lang_pug_render,
  Listvue_type_template_id_57a3a2bb_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "57a3a2bb",
  null
  
)

/* harmony default export */ var List = (List_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/Viz.vue?vue&type=script&lang=js&



//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ var Vizvue_type_script_lang_js_ = ({
  components: {
    ColRetrievalViewer: Viewer,
    ColRetrievalList: List
  },
  props: {
    /**
     * The current state of the application's visualization.
     * Used to display relevant annotations
     * */
    state: {
      type: Object,
      default: function _default() {}
    },

    /** The list of prior annotations */
    annotations: {
      type: Array,
      default: function _default() {
        return [];
      }
    },

    /** Whether the list should be visible */
    viewing: {
      type: Boolean,
      default: false
    },
    replyTo: {
      type: Object
    },
    canReply: {
      type: Boolean,
      default: true
    },
    openWith: {
      type: Object
    }
  },
  data: function data() {
    return {
      activeAnnotation: null,
      activeParent: null,
      answersToActive: [],
      activeUnits: []
    };
  },
  methods: {
    selectUnit: function selectUnit(unit) {
      if (!unit) this.activeUnits = [];else this.activeUnits.push(unit);
    },
    selectAnnotation: function selectAnnotation(annotation) {
      if (!annotation) return this.activeAnnotation = null;

      if (!this.annotations.includes(annotation)) {
        annotation = this.annotations.filter(function (a) {
          return a.id === annotation.id;
        })[0]; // ToDo: Find another way
      }

      if (this.activeAnnotation === annotation) this.activeAnnotation = null;else this.activeAnnotation = annotation;
    }
  },
  mounted: function mounted() {
    if (this.openWith) {
      this.selectAnnotation(this.openWith);
      this.$emit('openedWith');
    }
  },
  watch: {
    activeAnnotation: function activeAnnotation() {
      var _this = this;

      var active = this.activeAnnotation;
      this.activeUnits = [];

      if (active) {
        var _active$dataUnits;

        (_active$dataUnits = active.dataUnits) === null || _active$dataUnits === void 0 ? void 0 : _active$dataUnits.forEach(function (d) {
          _this.selectUnit(d);
        });
        this.$emit('updateState', active.meta.state);

        if (active.replyTo) {
          this.activeParent = this.annotations.filter(function (a) {
            var _active$replyTo;

            return a.id === (((_active$replyTo = active.replyTo) === null || _active$replyTo === void 0 ? void 0 : _active$replyTo.id) || active.replyTo);
          })[0];
        } else this.activeParent = null;

        this.answersToActive = this.annotations.filter(function (a) {
          var _a$replyTo;

          return active.id === (((_a$replyTo = a.replyTo) === null || _a$replyTo === void 0 ? void 0 : _a$replyTo.id) || a.replyTo);
        });
      } else {
        this.activeParent = null;
        this.answersToActive = [];
      }
    },
    openWith: function openWith() {
      if (this.openWith) this.selectAnnotation(this.openWith);
      this.$emit('openedWith');
    }
  }
});
// CONCATENATED MODULE: ./src/components/Retrieval/Viz.vue?vue&type=script&lang=js&
 /* harmony default export */ var Retrieval_Vizvue_type_script_lang_js_ = (Vizvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Retrieval/Viz.vue





/* normalize component */

var Viz_component = normalizeComponent(
  Retrieval_Vizvue_type_script_lang_js_,
  Vizvue_type_template_id_1db12965_lang_pug_render,
  Vizvue_type_template_id_1db12965_lang_pug_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var Viz = (Viz_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/Main.vue?vue&type=template&id=4ac51c12&scoped=true&lang=pug&
var Mainvue_type_template_id_4ac51c12_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('aside',{staticClass:"col-retrieval--main"},[_c('header',{staticClass:"col-retrieval--main__header"},[_c('h3',[_vm._v(_vm._s(_vm.filteredAnnotations.length)+" Annotations")])]),_c('c-toolbar',{attrs:{"unit":_vm.unit,"tag":_vm.tag,"pattern":_vm.pattern,"annotations":_vm.annotations,"loi":_vm.loi,"activeAnnotation":_vm.activeAnnotation},on:{"removeSort":_vm.removeSort,"search":function($event){_vm.needle = $event}}}),_c('hr'),_c('c-filter',{attrs:{"annotations":_vm.filteredAnnotations,"openWith":_vm.openWith},on:{"select":function($event){_vm.subset = $event},"openedWith":function($event){return _vm.$emit('openedWith')}}}),_c('transition-group',{ref:"list",staticClass:"annotations-list",attrs:{"name":"flip-list","tag":"ul"}},_vm._l((_vm.pagedAnnotations),function(annotation){return _c('li',{key:annotation.meta.timestamp,staticClass:"annotations-list__item",class:{ linked: _vm.isChild(annotation) || _vm.isParent(annotation) || (annotation === _vm.activeAnnotation && (_vm.activeParent || _vm.answersToActive.length)), child: _vm.isChild(annotation), parent: _vm.isParent(annotation) }},[_c('c-card',{attrs:{"annotation":annotation,"activeAnnotation":_vm.activeAnnotation,"filter":true,"replyTo":_vm.replyTo,"canReply":_vm.canReply,"replies":_vm.getReplies(annotation)},on:{"highlight":function($event){return _vm.$emit('highlight', $event)},"selectAnnotation":_vm.selectAnnotation,"tag":_vm.sortByTag,"unit":_vm.sortByUnit,"pattern":_vm.sortByPattern,"loi":_vm.sortByLoi,"avatar":function($event){return _vm.$emit('avatar', $event)},"reply":function($event){return _vm.$emit('reply', $event)},"endorse":function($event){return _vm.$emit('endorse', $event)},"funny":function($event){return _vm.$emit('funny', $event)}},scopedSlots:_vm._u([{key:"extension",fn:function(){return [_vm._t("card")]},proxy:true}],null,true)})],1)}),0),_c('transition',{attrs:{"name":"fade"}},[_c('button',{directives:[{name:"show",rawName:"v-show",value:(_vm.numPages < _vm.pages),expression:"numPages < pages"}],staticClass:"more btn primary",on:{"click":function($event){_vm.numPages += 1}}},[_vm._v("show more")])])],1)}
var Mainvue_type_template_id_4ac51c12_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Retrieval/Main.vue?vue&type=template&id=4ac51c12&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"4e57230e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/pug-plain-loader!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_retrieval/CFilter.vue?vue&type=template&id=68772c0c&scoped=true&lang=pug&
var CFiltervue_type_template_id_68772c0c_scoped_true_lang_pug_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('nav',{staticClass:"filter"},[_c('ul',{staticClass:"tabs"},[_c('li',{class:{ active: _vm.tab === 'units' },on:{"click":function($event){_vm.tab = 'units'}}},[_vm._v("Units")]),_c('li',{class:{ active: _vm.tab === 'tags' },on:{"click":function($event){_vm.tab = 'tags'}}},[_vm._v("Tags")]),_c('li',{class:{ active: _vm.tab === 'patterns' },on:{"click":function($event){_vm.tab = 'patterns'}}},[_vm._v("Patterns")]),_c('li',{class:{ active: _vm.tab === 'loi' },on:{"click":function($event){_vm.tab = 'loi'}}},[_vm._v("Levels of Interpretation")])]),(_vm.tab)?_c('section',{staticClass:"content"},[_vm._l((_vm.view.explanations),function(e,o){return (_vm.view.explanations)?_c('small',{staticClass:"explanation"},[_c('strong',[_vm._v(_vm._s(o)+": ")]),_c('span',[_vm._v(_vm._s(e.text))])]):_vm._e()}),(_vm.view.search)?_c('div',{staticClass:"filter__search"},[_c('label',[_c('search-icon'),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.needle),expression:"needle"}],staticClass:"filter__search__input",attrs:{"type":"search","placeholder":"Search"},domProps:{"value":(_vm.needle)},on:{"input":function($event){if($event.target.composing){ return; }_vm.needle=$event.target.value}}})],1)]):_vm._e(),_c('transition-group',{staticClass:"content__chips",attrs:{"tag":"div","name":"flip-list"}},_vm._l((_vm.filteredItems),function(item){return _c('c-chip',{key:item.name,class:{ selected: _vm.selected && _vm.selected.name === item.name },on:{"click":function($event){return _vm.select(item)}},scopedSlots:_vm._u([(item.icon)?{key:"prepend",fn:function(){return [(item.name === 'singularity')?_c('c-singularity',{attrs:{"color":"white"}}):_vm._e(),(item.name === 'duality')?_c('c-duality',{attrs:{"color":"white"}}):_vm._e(),(item.name === 'generality')?_c('c-generality',{attrs:{"color":"white"}}):_vm._e()]},proxy:true}:null,{key:"append",fn:function(){return [_c('span',{staticClass:"count"},[_vm._v(_vm._s(item.annotations.length)+" annotations")])]},proxy:true}],null,true)},[_vm._v(_vm._s(item.name))])}),1)],2):_vm._e()])}
var CFiltervue_type_template_id_68772c0c_scoped_true_lang_pug_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/_retrieval/CFilter.vue?vue&type=template&id=68772c0c&scoped=true&lang=pug&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/_retrieval/CFilter.vue?vue&type=script&lang=js&









//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ var CFiltervue_type_script_lang_js_ = ({
  components: {
    CChip: CChip,
    CSingularity: CSingularity,
    CDuality: CDuality,
    CGenerality: CGenerality,
    SearchIcon: SearchIcon
  },
  props: {
    annotations: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    openWith: {
      type: Object
    }
  },
  data: function data() {
    return {
      tab: null,
      selected: null,
      needle: null
    };
  },
  methods: {
    select: function select(item) {
      var _this$selected;

      this.selected = item.name === ((_this$selected = this.selected) === null || _this$selected === void 0 ? void 0 : _this$selected.name) ? null : item;
      this.$emit('select', this.selected);
    }
  },
  computed: {
    tags: function tags() {
      var tags = this.annotations.reduce(function (m, annotation) {
        var tags = annotation.tags.map(function (t) {
          return t.name || t;
        });
        tags.forEach(function (t) {
          if (!m.has(t)) m.set(t, []);
          m.get(t).push(annotation);
        });
        return m;
      }, new Map());
      var tagsArray = Array.from(tags).map(function (t) {
        return {
          name: t[0],
          annotations: t[1]
        };
      }).sort(function (a, b) {
        return a.annotations.length > b.annotations.length ? -1 : 1;
      });
      tagsArray.search = true;
      return tagsArray;
    },
    units: function units() {
      var units = this.annotations.reduce(function (m, annotation) {
        var units = annotation.dataUnits;
        if (!units) return m;
        units.forEach(function (u) {
          if (!m.has(u.title)) m.set(u.title, []);
          m.get(u.title).push(annotation);
        });
        return m;
      }, new Map());
      var unitsArray = Array.from(units).map(function (u) {
        return {
          name: u[0],
          annotations: u[1]
        };
      }).sort(function (a, b) {
        return a.annotations.length > b.annotations.length ? -1 : 1;
      });
      unitsArray.search = true;
      return unitsArray;
    },
    patterns: function patterns() {
      var patterns = new Map();
      patterns.set('singularity', []);
      patterns.set('duality', []);
      patterns.set('generality', []);
      this.annotations.forEach(function (annotation) {
        var _annotation$patterns, _annotation$patterns2, _annotation$patterns3;

        if ((_annotation$patterns = annotation.patterns) === null || _annotation$patterns === void 0 ? void 0 : _annotation$patterns.singularity) patterns.get('singularity').push(annotation);
        if ((_annotation$patterns2 = annotation.patterns) === null || _annotation$patterns2 === void 0 ? void 0 : _annotation$patterns2.duality) patterns.get('duality').push(annotation);
        if ((_annotation$patterns3 = annotation.patterns) === null || _annotation$patterns3 === void 0 ? void 0 : _annotation$patterns3.generality) patterns.get('generality').push(annotation);
      });
      var explanations = {
        singularity: {
          text: 'Denotes a comparison between a small group of entites and a larger one.'
        },
        duality: {
          text: 'Denotes a comparison between two or more groups of similar size.'
        },
        generality: {
          text: 'Denotes a general comment about the whole dataset of entites.'
        }
      };
      var patternsArray = Array.from(patterns).map(function (p) {
        return {
          name: p[0],
          annotations: p[1],
          icon: true
        };
      }).sort(function (a, b) {
        return a.annotations.length > b.annotations.length ? -1 : 1;
      });
      patternsArray.explanations = explanations;
      return patternsArray;
    },
    loi: function loi() {
      var loi = new Map();
      loi.set('visual', []);
      loi.set('data', []);
      loi.set('meaning', []);
      loi.set('visual and data', []);
      loi.set('visual and meaning', []);
      loi.set('data and meaning', []);
      loi.set('all', []);
      this.annotations.forEach(function (annotation) {
        var l = annotation.loi;
        if (!l) return;
        if (l.visual && !l.data && !l.meaning) loi.get('visual').push(annotation);else if (!l.visual && l.data && !l.meaning) loi.get('data').push(annotation);else if (!l.visual && !l.data && l.meaning) loi.get('meaning').push(annotation);else if (l.visual && l.data && !l.meaning) loi.get('visual and data').push(annotation);else if (l.visual && !l.data && l.meaning) loi.get('visual and meaning').push(annotation);else if (l.visual && !l.data && l.meaning) loi.get('data and meaning').push(annotation);else if (l.visual && l.data && l.meaning) loi.get('all').push(annotation);
      });
      var explanations = {
        visual: {
          text: 'The annotator relied on visual cues to formulate the annotation.'
        },
        data: {
          text: 'The annotator relied on prior knowledge of the dataset to formulate the annotation.'
        },
        meaning: {
          text: 'The annotator formulated an hypothesis based on the data selected.'
        }
      };
      var loiArray = Array.from(loi).map(function (l) {
        return {
          name: l[0],
          annotations: l[1],
          explanation: explanations[l[0]]
        };
      }).sort(function (a, b) {
        return a.annotations.length > b.annotations.length ? -1 : 1;
      });
      loiArray.explanations = explanations;
      return loiArray;
    },
    view: function view() {
      if (!this.tab) return null;else return this[this.tab];
    },
    filteredItems: function filteredItems() {
      var _this = this;

      if (!this.needle) return this.view;
      return this.view.filter(function (item) {
        var _item$name;

        return (_item$name = item.name) === null || _item$name === void 0 ? void 0 : _item$name.toLowerCase().includes(_this.needle.toLowerCase());
      });
    }
  },
  mounted: function mounted() {
    var _this$openWith;

    if (((_this$openWith = this.openWith) === null || _this$openWith === void 0 ? void 0 : _this$openWith.type) === 'filter') {
      var _this$openWith$object = this.openWith.object,
          tab = _this$openWith$object.tab,
          selected = _this$openWith$object.selected;
      this.tab = tab;
      this.select(this[tab].filter(function (s) {
        return s.name === selected;
      })[0]);
    }

    this.$emit('openedWith');
  },
  watch: {
    openWith: function openWith() {
      var _this$openWith2;

      if (((_this$openWith2 = this.openWith) === null || _this$openWith2 === void 0 ? void 0 : _this$openWith2.type) === 'filter') {
        var _this$openWith$object2 = this.openWith.object,
            tab = _this$openWith$object2.tab,
            selected = _this$openWith$object2.selected;
        this.tab = tab;
        this.select(this[tab].filter(function (s) {
          return s.name === selected;
        })[0]);
      }

      this.$emit('openedWith');
    },
    tab: function tab() {
      this.needle = null;
    }
  }
});
// CONCATENATED MODULE: ./src/components/_retrieval/CFilter.vue?vue&type=script&lang=js&
 /* harmony default export */ var _retrieval_CFiltervue_type_script_lang_js_ = (CFiltervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/_retrieval/CFilter.vue?vue&type=style&index=0&id=68772c0c&lang=sass&scoped=true&
var CFiltervue_type_style_index_0_id_68772c0c_lang_sass_scoped_true_ = __webpack_require__("0d61");

// CONCATENATED MODULE: ./src/components/_retrieval/CFilter.vue






/* normalize component */

var CFilter_component = normalizeComponent(
  _retrieval_CFiltervue_type_script_lang_js_,
  CFiltervue_type_template_id_68772c0c_scoped_true_lang_pug_render,
  CFiltervue_type_template_id_68772c0c_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "68772c0c",
  null
  
)

/* harmony default export */ var CFilter = (CFilter_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Retrieval/Main.vue?vue&type=script&lang=js&







//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var Retrieval_Mainvue_type_script_lang_js_ = ({
  components: {
    CToolbar: CToolbar,
    CFilter: CFilter,
    CCard: CCard
  },
  props: {
    /** The list of prior annotations */
    annotations: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    openWith: {
      type: Object
    },
    canReply: {
      type: Boolean,
      default: true
    }
  },
  data: function data() {
    return {
      open: false,
      dragging: false,
      initialX: null,
      width: 0,
      eventListeners: [],
      unit: null,
      tag: null,
      loi: null,
      pattern: null,
      needle: null,
      aByPage: 10,
      numPages: 1,
      answersToActive: [],
      activeParent: null,
      activeAnnotation: null,
      subset: null
    };
  },
  methods: {
    sortByTag: function sortByTag(tag) {
      this.tag = this.tag === tag ? null : tag;
    },
    sortByLoi: function sortByLoi(loi) {
      this.loi = this.loi === loi ? null : loi;
    },
    sortByPattern: function sortByPattern(pattern) {
      this.pattern = this.pattern === pattern ? null : pattern;
    },
    sortByUnit: function sortByUnit(unit) {
      var _this$unit;

      this.unit = ((_this$unit = this.unit) === null || _this$unit === void 0 ? void 0 : _this$unit.title) === (unit === null || unit === void 0 ? void 0 : unit.title) ? null : unit;
    },
    removeSort: function removeSort(variable) {
      if (variable === 'selectAnnotation') this.activeAnnotation = null;else this[variable] = null;
    },
    isParent: function isParent(annotation) {
      var _this$activeParent;

      return annotation.id === (((_this$activeParent = this.activeParent) === null || _this$activeParent === void 0 ? void 0 : _this$activeParent.id) || this.activeParent);
    },
    isChild: function isChild(annotation) {
      var _this$answersToActive;

      return (_this$answersToActive = this.answersToActive) === null || _this$answersToActive === void 0 ? void 0 : _this$answersToActive.map(function (a) {
        return a.id;
      }).includes(annotation.id);
    },
    selectAnnotation: function selectAnnotation(annotation) {
      if (!annotation) return this.activeAnnotation = null;

      if (!this.annotations.includes(annotation)) {
        annotation = this.annotations.filter(function (a) {
          return a.id === annotation.id;
        })[0]; // ToDo: Find another way
      }

      if (this.activeAnnotation === annotation) this.activeAnnotation = null;else this.activeAnnotation = annotation;
      document.querySelector('.annotations-list').scrollIntoView({
        behavior: 'smooth'
      });
    },
    getReplies: function getReplies(annotation) {
      return this.annotations.filter(function (a) {
        var _a$replyTo;

        return ((_a$replyTo = a.replyTo) === null || _a$replyTo === void 0 ? void 0 : _a$replyTo.id) === annotation.id || a.replyTo === annotation.id;
      });
    }
  },
  computed: {
    pages: function pages() {
      return Math.ceil(this.filteredAnnotations.length / this.aByPage);
    },
    subsetAnnotations: function subsetAnnotations() {
      if (!this.subset) return this.annotations;else return this.subset.annotations;
    },
    filteredAnnotations: function filteredAnnotations() {
      if (!this.needle || this.needle.length < 3) return this.subsetAnnotations;else {
        var n = this.needle.toLowerCase();
        return this.subsetAnnotations.filter(function (a) {
          var _a$social, _a$social$author, _a$tags, _haystacks2;

          var haystacks = [];
          var valid = false;
          if (a.rawAnnotation) haystacks.push(a.rawAnnotation.text);
          if (a.text) haystacks.push(a.text);

          if (a.dataUnits) {
            a.dataUnits.forEach(function (unit) {
              var _haystacks;

              if (!unit.aggregated) haystacks.push(unit.title);else (_haystacks = haystacks).push.apply(_haystacks, _toConsumableArray(unit.units.map(function (subunit) {
                return subunit.title;
              })));
            });
          }

          if ((_a$social = a.social) === null || _a$social === void 0 ? void 0 : (_a$social$author = _a$social.author) === null || _a$social$author === void 0 ? void 0 : _a$social$author.name) haystacks.push(a.social.author.name);
          if ((_a$tags = a.tags) === null || _a$tags === void 0 ? void 0 : _a$tags.length) (_haystacks2 = haystacks).push.apply(_haystacks2, _toConsumableArray(a.tags.map(function (t) {
            return t.name || t;
          })));
          haystacks = haystacks.filter(function (h) {
            return h;
          });
          return haystacks.reduce(function (valid, haystack) {
            return haystack.toLowerCase().includes(n) ? true : valid;
          }, valid);
        });
      }
    },
    sortedAnnotations: function sortedAnnotations() {
      var _this = this;

      var clone = _toConsumableArray(this.filteredAnnotations);

      var sortFns = []; // Active annotation on top

      sortFns.push(function (a, b) {
        var _this$activeParent2, _this$activeParent3;

        return a.id === (((_this$activeParent2 = _this.activeParent) === null || _this$activeParent2 === void 0 ? void 0 : _this$activeParent2.id) || _this.activeParent) ? -1 : b.id === (((_this$activeParent3 = _this.activeParent) === null || _this$activeParent3 === void 0 ? void 0 : _this$activeParent3.id) || _this.activeParent) ? 1 : 0;
      });
      sortFns.push(function (a, b) {
        return a === _this.activeAnnotation ? -1 : b === _this.activeAnnotation ? 1 : 0;
      });
      sortFns.push(function (a, b) {
        var _this$answersToActive2, _this$answersToActive3;

        return ((_this$answersToActive2 = _this.answersToActive) === null || _this$answersToActive2 === void 0 ? void 0 : _this$answersToActive2.map(function (annotation) {
          return annotation.id;
        }).includes(a.id)) ? -1 : ((_this$answersToActive3 = _this.answersToActive) === null || _this$answersToActive3 === void 0 ? void 0 : _this$answersToActive3.map(function (annotation) {
          return annotation.id;
        }).includes(b.id)) ? 1 : 0;
      }); // Then sort by level of interpretation

      if (this.loi) sortFns.push(function (a, b) {
        var levels = _this.loi.split(' ');

        function hasLevels(annotation, levels) {
          var valid = true;
          levels.forEach(function (l) {
            var _a$loi;

            return valid = ((_a$loi = a.loi) === null || _a$loi === void 0 ? void 0 : _a$loi[l]) ? valid : false;
          });
          return valid;
        }

        if (hasLevels(a, levels) && !hasLevels(b, levels)) return -1;else if (!hasLevels(a, levels) && hasLevels(b, levels)) return 1;else return 0;
      }); // Then by patterns

      if (this.pattern) sortFns.push(function (a, b) {
        var _a$patterns, _b$patterns, _a$patterns2, _b$patterns2;

        if (((_a$patterns = a.patterns) === null || _a$patterns === void 0 ? void 0 : _a$patterns[_this.pattern]) && !((_b$patterns = b.patterns) === null || _b$patterns === void 0 ? void 0 : _b$patterns[_this.pattern])) return -1;else if (!((_a$patterns2 = a.patterns) === null || _a$patterns2 === void 0 ? void 0 : _a$patterns2[_this.pattern]) && ((_b$patterns2 = b.patterns) === null || _b$patterns2 === void 0 ? void 0 : _b$patterns2[_this.pattern])) return 1;else return 0;
      }); // Then sort by tag

      if (this.tag) sortFns.push(function (a, b) {
        if (a.tags.map(function (t) {
          return t.name || t;
        }).includes(_this.tag) && !b.tags.map(function (t) {
          return t.name || t;
        }).includes(_this.tag)) return -1;else if (!a.tags.map(function (t) {
          return t.name || t;
        }).includes(_this.tag) && b.tags.map(function (t) {
          return t.name || t;
        }).includes(_this.tag)) return 1;else return 0;
      }); // Then by unit

      if (this.unit) sortFns.push(function (a, b) {
        var _a$dataUnits, _b$dataUnits, _a$dataUnits2, _b$dataUnits2;

        if (((_a$dataUnits = a.dataUnits) === null || _a$dataUnits === void 0 ? void 0 : _a$dataUnits.map(function (u) {
          return u.title;
        }).includes(_this.unit.title)) && !((_b$dataUnits = b.dataUnits) === null || _b$dataUnits === void 0 ? void 0 : _b$dataUnits.map(function (u) {
          return u.title;
        }).includes(_this.unit.title))) return -1;else if (!((_a$dataUnits2 = a.dataUnits) === null || _a$dataUnits2 === void 0 ? void 0 : _a$dataUnits2.map(function (u) {
          return u.title;
        }).includes(_this.unit.title)) && ((_b$dataUnits2 = b.dataUnits) === null || _b$dataUnits2 === void 0 ? void 0 : _b$dataUnits2.map(function (u) {
          return u.title;
        }).includes(_this.unit.title))) return 1;else return 0;
      }); // Then by time

      sortFns.push(function (a, b) {
        return a.meta.timestamp > b.meta.timestamp ? -1 : 1;
      });
      clone.sort(function (a, b) {
        var value = 1;

        for (var i = 0; i < sortFns.length; i++) {
          value = sortFns[i](a, b);
          if (value !== 0) return value;
        }

        return value;
      });
      return clone;
    },
    pagedAnnotations: function pagedAnnotations() {
      var _this2 = this;

      return this.sortedAnnotations.filter(function (a, i) {
        return i < _this2.aByPage * _this2.numPages;
      });
    }
  },
  mounted: function mounted() {
    var _this$openWith;

    if (((_this$openWith = this.openWith) === null || _this$openWith === void 0 ? void 0 : _this$openWith.type) === 'annotation') {
      this.selectAnnotation(this.openWith.object);
      this.$emit('openedWith');
    }
  },
  watch: {
    activeAnnotation: function activeAnnotation() {
      var active = this.activeAnnotation;
      this.activeUnits = [];

      if (active) {
        if (active.replyTo) {
          this.activeParent = this.annotations.filter(function (a) {
            var _active$replyTo;

            return a.id === (((_active$replyTo = active.replyTo) === null || _active$replyTo === void 0 ? void 0 : _active$replyTo.id) || active.replyTo);
          })[0];
        } else this.activeParent = null;

        this.answersToActive = this.annotations.filter(function (a) {
          var _a$replyTo2;

          return active.id === (((_a$replyTo2 = a.replyTo) === null || _a$replyTo2 === void 0 ? void 0 : _a$replyTo2.id) || a.replyTo);
        });
      } else {
        this.activeParent = null;
        this.answersToActive = [];
      }
    },
    openWith: function openWith() {
      var _this$openWith2;

      if (((_this$openWith2 = this.openWith) === null || _this$openWith2 === void 0 ? void 0 : _this$openWith2.type) === 'annotation') this.selectAnnotation(this.openWith.object);
      this.$emit('openedWith');
    }
  }
});
// CONCATENATED MODULE: ./src/components/Retrieval/Main.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Retrieval_Mainvue_type_script_lang_js_ = (Retrieval_Mainvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Retrieval/Main.vue?vue&type=style&index=0&id=4ac51c12&lang=sass&scoped=true&
var Mainvue_type_style_index_0_id_4ac51c12_lang_sass_scoped_true_ = __webpack_require__("ddcf");

// CONCATENATED MODULE: ./src/components/Retrieval/Main.vue






/* normalize component */

var Retrieval_Main_component = normalizeComponent(
  components_Retrieval_Mainvue_type_script_lang_js_,
  Mainvue_type_template_id_4ac51c12_scoped_true_lang_pug_render,
  Mainvue_type_template_id_4ac51c12_scoped_true_lang_pug_staticRenderFns,
  false,
  null,
  "4ac51c12",
  null
  
)

/* harmony default export */ var Retrieval_Main = (Retrieval_Main_component.exports);
// EXTERNAL MODULE: ./src/assets/utils/defaultOptions.json
var defaultOptions = __webpack_require__("cd8d");

// EXTERNAL MODULE: ./src/assets/styles/main.sass
var main = __webpack_require__("f17c");

// CONCATENATED MODULE: ./src/lib.js















/**
 * Colvis Plugin
 */

var ColvisPlugin = {
  install: function install(Vue) {
    /** Registering the main components */
    Vue.component('ColInputMain', Main);
    Vue.component('ColRetrievalMain', Retrieval_Main);
    Vue.component('ColRetrievalViz', Viz);
    Vue.component('CText', CText);
    Vue.component('CCard', CCard);
    /** Installing the colvis plugin under $colvis namespace */

    Vue.prototype.$colvis = {
      _vm: new Vue({
        data: {
          /** 
           * Whether the plugin has been initialized. Should be set to 
           * true only after the Vega viz has been rendered.
           */
          init: false,

          /**
           * The specs of the Colvis interface. Please refer to the
           * examples or the schema for structure.
           */
          specs: null,

          /**
           * Optional object representing the state of the visualizaton
           * when an annotation is taken.
           */
          vizState: null,

          /**
           * Whether the social features should be on
           */
          endorsement: false
        }
      }),
      initialize: function initialize(_ref) {
        var _this = this;

        var specs = _ref.specs,
            staticData = _ref.staticData,
            vizState = _ref.vizState,
            endorsement = _ref.endorsement;
        if (!specs) throw new Error('Please provide a specifications object to initialize Colvis.');
        if (!specs.options) specs.options = defaultOptions;
        this._vm.$data.rawSpecs = JSON.stringify(specs);
        this._vm.$data.specs = specs;
        this._vm.$data.vizState = vizState;
        this._vm.$data.endorsement = endorsement;

        this._vm.$data.specs.natures.forEach(function (n) {
          if (n.annotable) {
            n.list = getDataFromContainer(_this._vm.$data.specs.visualization.container, {
              title: n.annotable.title,
              id: n.annotable.id,
              natureId: n.id,
              natureMarkType: n.annotable.markType,
              selector: n.annotable.selector,
              method: specs.visualization.method,
              allResults: n.annotable.allResults
            });
          } else {
            if (!staticData || !staticData[n.id]) {
              n.list = [];
            } else {
              n.list = staticData[n.id].map(function (d, index) {
                return new DataBinder_DataBinder({
                  id: d,
                  natureId: n.id,
                  obj: d,
                  domElement: null
                });
              });
            }
          }
        });

        this._vm.$data.init = true;
      },
      isInit: function isInit() {
        return this._vm.$data.init;
      },
      getAnnotables: function getAnnotables() {
        var _ref2;

        return (_ref2 = []).concat.apply(_ref2, _toConsumableArray(this._vm.$data.specs.natures.filter(function (n) {
          return n.annotable;
        }).map(function (n) {
          return n.list;
        })));
      },
      getCombinations: function getCombinations() {
        var _ref3;

        return (_ref3 = []).concat.apply(_ref3, _toConsumableArray(this._vm.$data.specs.combinations));
      },
      getEntities: function getEntities() {
        var _ref4;

        return (_ref4 = []).concat.apply(_ref4, _toConsumableArray(this._vm.$data.specs.natures.map(function (n) {
          return n.list;
        })));
      },
      getEntitiesHash: function getEntitiesHash() {
        var _ref5;

        return hashCode(strfy((_ref5 = []).concat.apply(_ref5, _toConsumableArray(this._vm.$data.specs.natures.map(function (n) {
          return n.list;
        })))));
      },
      getNatures: function getNatures() {
        return this._vm.$data.specs.natures;
      },
      getNatureName: function getNatureName(id) {
        var plural = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        var nature = this.getNature(id);
        if (!nature.name) return nature.id;

        if (nature.name && plural) {
          return nature.namePlural || "".concat(nature.name, "s");
        }

        return nature.name;
      },
      getNature: function getNature(id) {
        var natureIndex = this._vm.$data.specs.natures.map(function (n) {
          return n.id;
        }).indexOf(id);

        var nature = this._vm.$data.specs.natures[natureIndex];
        return nature;
      },
      getSpecs: function getSpecs() {
        return this._vm.$data.specs;
      },
      getEndorsements: function getEndorsements() {
        return this._vm.$data.endorsement;
      },
      getSpecsHash: function getSpecsHash() {
        return hashCode(this._vm.$data.rawSpecs);
      },
      getVizState: function getVizState() {
        return this._vm.$data.vizState;
      },
      setVizState: function setVizState(vizState) {
        this._vm.$data.vizState = vizState;
      },
      getOptions: function getOptions() {
        return this._vm.$data.specs.options;
      },
      hasAutocomplete: function hasAutocomplete() {
        if (this._vm.$data.specs.options.autocomplete === false) return false;else return true;
      },
      hasSelector: function hasSelector() {
        if (this._vm.$data.specs.options.selector === false) return false;else return true;
      },
      hasViewer: function hasViewer() {
        if (this._vm.$data.specs.options.viewer === false) return false;else return true;
      },

      /**
       * 
       * @param {*} items 
       * @returns 
       */
      getFilteredList: function getFilteredList(rawEntities) {
        var hidden = _toConsumableArray(rawEntities);

        var displayed = [];

        for (var i = this.getOptions().unitsLimit; i > 0 && hidden.length; i--) {
          displayed.push(hidden.shift());
        }

        return {
          displayed: displayed,
          hidden: hidden
        };
      },
      getEntityTitle: function getEntityTitle(item) {
        var nature = this._vm.$data.specs.natures.filter(function (n) {
          return n.id === item.natureId;
        })[0];

        if (!item.obj || !nature.annotable.title) return item.id;

        if (nature.annotable.title instanceof Array) {
          return nature.annotable.title.map(function (p) {
            var isText = p.indexOf('text:').includes;
            if (isText) return p.substr(5);
            var steps = p.split('.');
            if (steps.length === 1) return item.obj[p];
            return steps.reduce(function (prev, cur) {
              return prev[cur];
            }, item.obj);
          }).join(' ');
        } else {
          var isText = nature.annotable.title.includes('text:');
          if (isText) return nature.annotable.title.substr(5);
          var steps = nature.annotable.title.split('.');
          if (steps.length === 1) return item.obj[nature.annotable.title];
          return steps.reduce(function (prev, cur) {
            return prev[cur];
          }, item.obj);
        }
      },
      getAnnotationDataBinders: function getAnnotationDataBinders(annotation) {
        var _this2 = this;

        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'subject';
        return annotation.dataUnits.filter(function (unit) {
          return unit.role === type;
        }) // remove unwanted type
        .map(function (unit) {
          // get the corresponding DataBinder
          var ids = _this2.getEntities().map(function (e) {
            return e.id;
          });

          var index = ids.indexOf(unit.id);

          if (index !== -1) {
            return _this2.getEntities()[index];
          }
        }).filter(function (binder) {
          return binder;
        }); // remove potential non-existant binders
      },
      getDataBinders: function getDataBinders(units) {
        var _this3 = this;

        var singleUnits = units.reduce(function (prev, unit) {
          return unit.units.length ? prev.concat(unit.units) : prev.concat([unit]);
        }, []);
        return singleUnits.map(function (unit) {
          // get the corresponding DataBinder
          var ids = _this3.getEntities().map(function (e) {
            return e.id;
          });

          var index = ids.indexOf(unit.id);

          if (index !== -1) {
            var entity = _this3.getEntities()[index];

            entity.role = unit.role;
            return entity;
          }
        }).filter(function (binder) {
          return binder;
        }); // remove potential non-existant binders
      },
      getBinderNature: function getBinderNature(binder) {
        var n;

        try {
          n = this.getNatures().filter(function (n) {
            return n.id === binder.natureId;
          })[0];
        } catch (e) {
          console.error(e);
          throw new Error('Cannot find nature');
        }

        return n;
      },
      getCombinationsWhereParent: function getCombinationsWhereParent(natureId) {
        var inComb = [];
        var combinations = this.getCombinations();
        combinations.forEach(function (c) {
          if (c.first === natureId || c.second === natureId) {
            inComb.push(c);
          }
        });
        return inComb;
      },
      getCombinationWhereProduct: function getCombinationWhereProduct(natureId) {
        var inComb = [];
        var combinations = this.getCombinations();
        combinations.forEach(function (c) {
          var products = c.products.map(function (p) {
            return typeof p === 'string' ? p : p.id;
          });
          if (products.includes(natureId)) inComb.push(c);
        });
        return inComb;
      },
      resetDataBinders: function resetDataBinders() {
        var _this4 = this;

        this._vm.$data.specs.natures.forEach(function (n) {
          if (n.annotable) {
            n.list = getDataFromContainer(_this4._vm.$data.specs.visualization.container, {
              title: n.annotable.title,
              id: n.annotable.id,
              natureId: n.id,
              natureMarkType: n.annotable.markType,
              selector: n.annotable.selector,
              method: _this4._vm.$data.specs.visualization.method,
              allResults: n.annotable.allResults
            });
          } else {
            if (!staticData || !staticData[n.id]) {
              n.list = [];
            } else {
              n.list = staticData[n.id].map(function (d, index) {
                return new DataBinder_DataBinder({
                  id: d,
                  natureId: n.id,
                  obj: d,
                  domElement: null
                });
              });
            }
          }
        });
      },

      /**
       * Returns a list of entities related to the entity sent as a first argument.
       * @param {*} entity 
       * @param {*} combination 
       */
      getProductEntities: function getProductEntities(entity, combination) {}
    };
  }
};
/* harmony default export */ var lib = (ColvisPlugin);
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (lib);



/***/ }),

/***/ "fb6c":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "fdef":
/***/ (function(module, exports) {

module.exports = '\x09\x0A\x0B\x0C\x0D\x20\xA0\u1680\u180E\u2000\u2001\u2002\u2003' +
  '\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';


/***/ })

/******/ });
});
//# sourceMappingURL=colvis-client.umd.js.map