module.exports = {
  presets: [ '@vue/app' ],
  plugins: [ '@babel/plugin-proposal-optional-chaining', '@babel/plugin-syntax-nullish-coalescing-operator' ]
}
