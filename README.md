# colvis-client
This library is a VueJS plugin that allows the annotation of data visualizations produced by Vega, Vega-lite and rendered as SVG.

## About the ColVis project
ColVis is an academical project that allows analysts to annotate data visualizations. Technically, it consists of several libraries, including this one.

## Usage
### Requirements
- A VueJS application
- Yarn instead of NPM

### Installation
The ColVis project is still in active development and I do not plan to release it on NPM before the code reaches a decent matury level. In order to use colvis-client, you cannot rely on the NPM repository. Instead, you add external dependencies to your package.json file in your project:

```
    dependencies: {
        ...
        "colvis-client": "ssh://gogs@gogs.vanhulst.one:Human-IST/colvis-client.git",
        ...
    }
```
Please make sure that you have added a working [SSH key to your Gogs account](https://gogs.vanhulst.one/user/settings/ssh) in order to use this dependency as illustrated above.

### Setup
1. Import colvis-client in your project
3. Tell Vue to use Colvis

```
import Vue from 'vue';
import Colvis from 'colvis-client';

Vue.use(Colvis);
new Vue(
    ...
)
```

### Run the example of this repository
Please check [the dedicated page](https://gogs.vanhulst.one/Human-IST/colvis-client/src/master/example) to run the example.

## Development

### Structure of this repository
This repository was built through Vue CLI 3. Its structure is as follows:

- dist: distribution version of the library. Use these files in your project, though this should work automatically if you `import` or `require`.
- example: an example of working Colvis interface
- src: the main content of the library
    - assets: js, and potentially other files that are processed when building the app
        - utils: various js functions meant to be used by the plugin
    - components: the VueJS components that structure the app
        - input: all components related to the annotation-taking part of Colvis
        - (soon) retrieval: all components related to the display of the annotations taken by Colvis 
    - models: classes that are regularly used in the components
- tests: unit tests
- types: some type definitions for typescript

### Compiles and minifies for production
```
yarn run build
```

### Run tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```