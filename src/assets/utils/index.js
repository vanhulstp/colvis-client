import { DataBinder } from '../../models'

/**
 * Check if a given element has a parent with the same __data__ property.
 * @param el
 */
function hasSelectableParent(el, method = 'vega') {
    if (!el.parentElement || !el.parentElement.__data__) return false;
    if (method === 'vega') return strfy(el.parentElement.__data__.datum) === strfy(el.__data__.datum);
    else return strfy(el.parentElement.__data__) === strfy(el.__data__);
}

/**
 * Take a selection and return a duplicate-free version
 * @param {DataBinder[]} selection
 * @returns {DataBinder[]} duplicate-free version of the selection 
 */
export function enforceUniqueId(selection) {
    const m = new Map(selection.map(s => [s.id, s]));
    const a = Array.from(m).map(a => a[1]);
    return a;
}

/**
 * Ensure a DataBinder is not already present within a selection
 * @param {DataBinder} el 
 * @param {DataBinder[]} selection 
 * @returns {Boolean} whether it is not present
 */
export function ensureNotPresent(el, selection) {
    const ids = selection.map(e => e.id);
    return !ids.includes(el.id);
}

/**
 * Stringify an object, ignoring all circular references.
 * @param obj object to stringify
 */
export function strfy(obj) {
    const cache = [];
    return JSON.stringify(obj, (key, value) => {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) return;
            cache.push(value);
        }
        return value;
    });
}

/**
 * Return the unique identifier of an entity
 * @param {Object} options
 * @param {DataBinder} options.entity
 * @param {String[]} options.fields
 * @param {String|Boolean} options.defaultValue
 * @returns {String} the id of the identity 
 */
export function getEntityId({ entity, fields, defaultValue = false }) {
    if (!fields) {
        if (!defaultValue) throw new Error('Please provide either a list of fields or a default value');
        else return defaultValue;
    } else {
        const values = fields.map(f => {
            if (!f) return;
            const steps = f.split('.');
            if (steps.length === 1) return entity[f]
            else {
                return steps.reduce(
                    (prev, cur) => prev[cur],
                    entity
                )
            }
        });
        return values.join('-');
    }
}

/**
 * Return the title of an entity, to display in the annotation
 * @param {Object} options
 * @param {DataBinder} options.entity
 * @param {String[]} options.fields
 * @param {String|Boolean} options.defaultValue
 * @returns {String|null} the title of the identity 
 */
export function getEntityTitle({ entity, fields, defaultValue = false }) {
    if (!fields) {
        return defaultValue || null;
    } else {
        const values = fields.map(f => {
            if (!f) return;
            const steps = f.split('.');
            if (steps.length === 1) return entity[f]
            else {
                return steps.reduce(
                    (prev, cur) => prev[cur],
                    entity
                )
            }
        });
        return values.join('-');
    }
}

/**
 * Return an array of DataBinder to be used by the AnnotationBox.
 * If using Vega, containerSelector should be a selector for the element where Vega 
 * creates the SVG. If using D3, containerSelector should be the actual selector of the SVG.
 * @param {string} containerSelector the selector used to identify where to look for data. Example: '.svgContainer'
 * @param {GetDataFromContainerOptions} options misc options
 */
export function getDataFromContainer(containerSelector, options) {
    const method = options.method || 'vega';
    const filter = options.filter || null;
    const natureId = options.natureId || null;
    const natureMarkType = options.natureMarkType || null;
    const selector = options.selector || '*';
    const allResults = options.allResults || false;
    const data = [];

    const idFields = options.id instanceof Array ? options.id : [options.id];
    let titleFields;
    if (options.title) {
        titleFields = options.title instanceof Array ? options.title : [options.title]
    }

    if (method === 'vega') {
        const d = Array.from(document.querySelectorAll(`${containerSelector} svg ${selector}`))
            .filter((el) => {
                return el.__data__ &&
                    (!natureMarkType || el.__data__.shape === natureMarkType || el.__data__.mark.marktype === natureMarkType) &&
                    el.__data__.datum &&
                    (!filter || el.__data__.datum[filter]) &&
                    (allResults || !hasSelectableParent(el));
            })
            .map((el, index) => new DataBinder({ 
                id: getEntityId({ entity: el.__data__.datum, fields: idFields, defaultValue: index }), 
                title: getEntityTitle({ entity: el.__data__.datum, fields: titleFields, defaultValue: index }), 
                natureId, 
                obj: el.__data__.datum, 
                domElement: el 
            }));
        data.push(...d);
    } else if (method === 'd3') {
        const d = Array.from(document.querySelectorAll(`${containerSelector} ${selector}`))
            .filter((el) => {
                return el.__data__  &&
                (!natureMarkType || el.tagName === natureMarkType) &&
                (!filter || el.__data__[filter]) &&
                (allResults || !hasSelectableParent(el, 'd3'));
            })
            .map((el, index) => new DataBinder({ 
                id: getEntityId({ entity: el.__data__, fields: idFields, defaultValue: index }), 
                title: getEntityTitle({ entity: el.__data__, fields: titleFields, defaultValue: index }), 
                natureId, 
                obj: el.__data__, 
                domElement: el 
            }));
        data.push(...d);
    }

    return data;
}

/**
 * Check if an element is being crossed by the rectangular selection.
 * @param {ClientRect} elBoundingRect the ClientRect of the element being checked
 * @param {SelectionCoordinates} coordinates the coordinates of the selection
 * @returns {Boolean} whether the rectangle is selected or not
 */
export function checkIfSelected(elBoundingRect, coordinates) {
    let match = true;

    // X-axis
    if (elBoundingRect.left > (coordinates.x + coordinates.width)) match = false;
    if (elBoundingRect.left + elBoundingRect.width < coordinates.x) match = false;

    // Y-axis
    if (elBoundingRect.top > (coordinates.y + coordinates.height)) match = false;
    if (elBoundingRect.top + elBoundingRect.height < coordinates.y) match = false;

    return match;
}

/**
 * Hash a string
 * courtesy of https://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
 * @param {String} str the String to hash
 * @returns {String} the hash 
 */
export function hashCode (str) {
    let hash = 0; 
    let i;
    let chr;

    if (str.length === 0) return hash;

    for (i = 0; i < str.length; i++) {
        chr   = str.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; 
    }
    
    return hash;
};