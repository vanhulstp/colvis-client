import ColInputMain from './components/Input/Main.vue';
import ColRetrievalViz from './components/Retrieval/Viz.vue';
import ColRetrievalMain from './components/Retrieval/Main.vue';
import CText from './components/Retrieval/CText.vue';
import CCard from './components/Retrieval/CCard.vue';
import { getDataFromContainer, hashCode, strfy } from './assets/utils';
import defaultOptions from './assets/utils/defaultOptions.json'
import { DataBinder } from './models';
import './assets/styles/main.sass';

/**
 * Colvis Plugin
 */
const ColvisPlugin = {
    install(Vue) {
        /** Registering the main components */
        Vue.component('ColInputMain', ColInputMain);
        Vue.component('ColRetrievalMain', ColRetrievalMain);
        Vue.component('ColRetrievalViz', ColRetrievalViz);
        Vue.component('CText', CText);
        Vue.component('CCard', CCard);

        /** Installing the colvis plugin under $colvis namespace */
        Vue.prototype.$colvis = {
            _vm: new Vue({ data: { 
                /** 
                 * Whether the plugin has been initialized. Should be set to 
                 * true only after the Vega viz has been rendered.
                 */
                init: false,

                /**
                 * The specs of the Colvis interface. Please refer to the
                 * examples or the schema for structure.
                 */
                specs: null,

                /**
                 * Optional object representing the state of the visualizaton
                 * when an annotation is taken.
                 */
                vizState: null,

                /**
                 * Whether the social features should be on
                 */
                endorsement: false
            }}),
            initialize ({ specs, staticData, vizState, endorsement }) {
                if (!specs) throw new Error('Please provide a specifications object to initialize Colvis.');
                if (!specs.options) specs.options = defaultOptions;
                
                this._vm.$data.rawSpecs = JSON.stringify(specs);
                this._vm.$data.specs = specs;
                this._vm.$data.vizState = vizState;
                this._vm.$data.endorsement = endorsement;

                this._vm.$data.specs.natures.forEach(n => {
                    if (n.annotable) {
                        n.list = getDataFromContainer(
                            this._vm.$data.specs.visualization.container,
                            {
                                title: n.annotable.title,
                                id: n.annotable.id,
                                natureId: n.id,
                                natureMarkType: n.annotable.markType,
                                selector: n.annotable.selector,
                                method: specs.visualization.method,
                                allResults: n.annotable.allResults
                            }
                        );
                    } else {
                        if (!staticData || !staticData[n.id]) {
                            n.list = [];
                        } else {
                            n.list = staticData[n.id].map((d, index) => {
                                return new DataBinder({ 
                                    id: d,
                                    natureId: n.id,
                                    obj: d, 
                                    domElement: null 
                                });
                            })
                        }
                    }
                });
                this._vm.$data.init = true;
            },
            isInit () {
                return this._vm.$data.init;
            },
            getAnnotables () {
                return [].concat(...this._vm.$data.specs.natures.filter(n => n.annotable)
                    .map(n => n.list));
            },
            getCombinations () {
                return [].concat(...this._vm.$data.specs.combinations);
            },
            getEntities () {
                return [].concat(...this._vm.$data.specs.natures.map(n => n.list));
            },
            getEntitiesHash () {
                return hashCode(strfy([].concat(...this._vm.$data.specs.natures.map(n => n.list))));
            },
            getNatures () {
                return this._vm.$data.specs.natures;
            },
            getNatureName (id, plural = false) {
                const nature = this.getNature(id);
                if (!nature.name) return nature.id;
                if (nature.name && plural) {
                    return nature.namePlural || `${nature.name}s`
                }
                return nature.name;
            },
            getNature (id) {
                const natureIndex = this._vm.$data.specs.natures.map(n => n.id).indexOf(id);
                const nature = this._vm.$data.specs.natures[natureIndex];
                return nature;
            },
            getSpecs () {
                return this._vm.$data.specs;
            },
            getEndorsements () {
                return this._vm.$data.endorsement;
            },
            getSpecsHash () {
                return hashCode(this._vm.$data.rawSpecs);
            },
            getVizState () {
                return this._vm.$data.vizState;
            },
            setVizState (vizState) {
                this._vm.$data.vizState = vizState;
            },
            getOptions () {
                return this._vm.$data.specs.options;
            },
            hasAutocomplete () {
                if (this._vm.$data.specs.options.autocomplete === false) return false;
                else return true;  
            },
            hasSelector () {
                if (this._vm.$data.specs.options.selector === false) return false;
                else return true;
            },
            hasViewer () {
                if (this._vm.$data.specs.options.viewer === false) return false;
                else return true;
            },
            /**
             * 
             * @param {*} items 
             * @returns 
             */
            getFilteredList (rawEntities) {
                const hidden = [...rawEntities];
                const displayed = [];
                for (let i = this.getOptions().unitsLimit; i > 0 && hidden.length; i--) {
                    displayed.push(hidden.shift());
                }
                return { displayed, hidden };
            },
            getEntityTitle (item) {
                const nature = this._vm.$data.specs.natures.filter(n => n.id === item.natureId)[0];
                if(!item.obj || !nature.annotable.title) return item.id;
                if (nature.annotable.title instanceof Array) {
                    return nature.annotable.title.map(p => {
                        const isText = p.indexOf('text:').includes;
                        if (isText) return p.substr(5);
                        const steps = p.split('.');
                        if (steps.length === 1) return item.obj[p];
                        return steps.reduce(
                            (prev, cur) => prev[cur],
                            item.obj
                        )
                    }).join(' ');
                }
                else {
                    const isText = nature.annotable.title.includes('text:');
                    if (isText) return nature.annotable.title.substr(5);
                    const steps = nature.annotable.title.split('.');
                    if (steps.length === 1) return item.obj[nature.annotable.title];
                    return steps.reduce(
                        (prev, cur) => prev[cur],
                        item.obj
                    )
                }
            },
            getAnnotationDataBinders (annotation, type = 'subject') {
                return annotation.dataUnits
                    .filter(unit => unit.role === type) // remove unwanted type
                    .map(unit => { // get the corresponding DataBinder
                        const ids = this.getEntities().map(e => e.id);
                        const index = ids.indexOf(unit.id);
                        if (index !== -1) {
                            return this.getEntities()[index]
                        }
                    })
                    .filter((binder) => binder) // remove potential non-existant binders
            },
            getDataBinders (units) {
                const singleUnits = units.reduce(
                    (prev, unit) => unit.units.length ? prev.concat(unit.units) : prev.concat([unit]),
                    []
                );
                return singleUnits
                    .map(unit => { // get the corresponding DataBinder
                        const ids = this.getEntities().map(e => e.id);
                        const index = ids.indexOf(unit.id);
                        if (index !== -1) {
                            const entity = this.getEntities()[index];
                            entity.role = unit.role;
                            return entity;
                        }
                    })
                    .filter((binder) => binder) // remove potential non-existant binders
            },
            getBinderNature (binder) {
                let n;
                try {
                    n = this.getNatures().filter(n => n.id === binder.natureId)[0];
                } catch (e) {
                    console.error(e)
                    throw new Error('Cannot find nature')
                }
                return n;
            },
            getCombinationsWhereParent (natureId) {
                const inComb = [];
                const combinations = this.getCombinations();
                combinations.forEach(c => {
                    if (c.first === natureId || c.second === natureId) {
                        inComb.push(c)
                    }
                })
                return inComb;
            },
            getCombinationWhereProduct (natureId) {
                const inComb = [];
                const combinations = this.getCombinations();
                combinations.forEach(c => {
                    const products = c.products.map(p => typeof p === 'string' ? p : p.id);
                    if (products.includes(natureId)) inComb.push(c);
                })
                return inComb;
            },
            resetDataBinders () {
                this._vm.$data.specs.natures.forEach(n => {
                    if (n.annotable) {
                        n.list = getDataFromContainer(
                            this._vm.$data.specs.visualization.container,
                            {
                                title: n.annotable.title,
                                id: n.annotable.id,
                                natureId: n.id,
                                natureMarkType: n.annotable.markType,
                                selector: n.annotable.selector,
                                method: this._vm.$data.specs.visualization.method,
                                allResults: n.annotable.allResults
                            }
                        );
                    } else {
                        if (!staticData || !staticData[n.id]) {
                            n.list = [];
                        } else {
                            n.list = staticData[n.id].map((d, index) => {
                                return new DataBinder({ 
                                    id: d,
                                    natureId: n.id,
                                    obj: d, 
                                    domElement: null 
                                });
                            })
                        }
                    }
                });
            },
            /**
             * Returns a list of entities related to the entity sent as a first argument.
             * @param {*} entity 
             * @param {*} combination 
             */
            getProductEntities (entity, combination) {
                
            }
        }
    },
};

export default ColvisPlugin;