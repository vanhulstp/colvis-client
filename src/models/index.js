/**
 * All classes (models) that implement Colvis
 * are exported throught this file. These models
 * are direct implementation of a theoritical
 * framework and might seem somewhat overengineered.
 * 
 * For more information about these models, please
 * refer the Colvis' documentation.
 */

import Annotation from './Annotation';
import DataBinder from './DataBinder';
import RawAnnotation from './RawAnnotation';
import Specs from './Specs';

export { Annotation, DataBinder, RawAnnotation, Specs };
