import { getEntityTitle } from '../assets/utils';

/**
 * An instance of RawAnnotation contains the values of the form
 * submitted by the analyst. It is basically an handy way to refer
 * to the raw data submitted, before Colvis classifies the annotation.
 * 
 * A RawAnnotation is composed of an observation and an optional 
 * meaning. The observation is itself subdivided into three: subjects,
 * reason, complements. An additional set of free tags can be 
 * attributed to the RawAnnotation.
 * 
 * The observation is the visual-focused or data-focused feature that
 * the analyst made during the sense making process. The meaning
 * is a tentative explanation for that feature.
 * @class
 */
export default class RawAnnotation {
    /**
     * The subjects of the observation
     * @type {DataBinder[]}
     * @public
     */
    subjects;

    /**
     * The name of the subjects
     * @type {string}
     * @public
     */
    subjectName;

    /**
     * The complements of the observation
     * @type {DataBinder[]|null}
     * @public
     */
    complements;

    /**
     * The name of the complement
     * @type {string}
     * @public
     */
    complementName;

    /**
     * The "reason" of the observation.
     * @type {Reason}
     * @public
     */
    reason;

    /**
     * The meaning
     * @type {string}
     * @public
     */
    meaning;

    /**
     * The tags of the annotation
     * @type {string[]}
     * @public
     */
    tags;

    /**
     * SelectionMethod
     * @type {('combobox', 'selector')[]} 
     */
    selectionMethods = [];

    /**
     * Textual representation of the annotation
     */
    text;

    /**
     * The state of the visualization
     */
    state;

    /**
     * the annotation being replied to
     */
    replyTo;

    /**
     * the type of reply, CommentSpace-like
     */
    replyType;

    /**
     * Constructor method of the RawAnnotation class
     * @param {Object} raw - The raw values of the form
     * @param {DataBinder[]} raw.subjects - The subjects of the annotation
     * @param {DataBinder[]} raw.complements - The complements of the annotation 
     * @param {Object} raw.reason - The reason 
     * @param {string} raw.meaning - The meaning 
     * @param {string[]} raw.tags - The tags of the annotation
     * @param {Object} raw.state - The state of the visualization
     * @param {('combobox'|'selector')[]} raw.selectionMethods - the methods used
     */
    constructor({ subjects, subjectName, complements, complementName, reason, meaning, tags, selectionMethods, specs, ci, state, replyTo, replyType }) {
        this.subjects = subjects;
        this.subjectName = subjectName || (this.subjects.length === 1 ? this.subjects[0].title : null);
        this.complements = complements;
        this.complementName = complementName || (this.complements.length === 1 ? this.complements[0].title : null);
        this.reason = reason;
        this.meaning = meaning;
        this.tags = tags;
        this.selectionMethods = selectionMethods;
        this.state = state;
        this.replyTo = replyTo;
        this.replyType = replyType;

        const subList = ci.getFilteredList(subjects, specs);
        const comList = ci.getFilteredList(complements, specs);

        if (!this.subjectName && this.subjects.length > 1) {
            let tSubList = subList.displayed.map(s => ci.getEntityTitle(s, specs));
            if (subList.hidden.length) tSubList = tSubList.concat([`and ${subList.hidden.length} others`])
            this.subjectName = tSubList.join(', ');     
        }

        if (!this.complementName && this.complements.length > 1) {
            let tComList = comList.displayed.map(c => ci.getEntityTitle(c, specs));
            if (comList.hidden.length) tComList = tComList.concat([`and ${comList.hidden.length} others`])
            this.complementName = tComList.join(', ');    
        }

        if (this.subjectName && (reason.verb || reason.details)) {
            this.text = this.subjectName;
            this.text += ` ${reason.verb.text}`;
            if (this.complementName) this.text += ` ${this.complementName}`;
            if (reason.details) this.text += ` ${reason.details}`;
            this.text += '.';
            if (meaning) this.text += ` ${meaning}.`;
        }
    }

}
