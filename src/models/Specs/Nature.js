/**
 * A Nature is a "type of entities" that can be found in data
 * visualization.
 * Providing a list of possible nature allows us to know what can
 * be annotated and what are the relations between each nature
 * within a dataset. When two data units of different nature are
 * used within an annotation, we expect the combination to be
 * the reason why the subject data unit is being annotated. For
 * more information, please refer to the Combination and the Reason 
 * models, or check Colvis' documentation.
 * 
 * Example: "Jean Valjean appears more than other characters at 
 * chapter 3". "Jean Valjean" and "other characters" both belong 
 * to the same nature, "character", while "chapter 3" belongs
 * to the "chapter" nature. "Jean Valjean" is the subject,
 * "other characters" are a comparison mean (complement),
 * and the combination of the subject and "chapter 3", "number of appearance",
 * is implicitly the reason why Jean Valjean is being annotated.
 * Possible combinations are limitless, and must be explicitely
 * defined in the Colvis specifications.
 */
export default class Nature {
    /**
     * The identifier of the nature. 
     * Example: "character", "chapter", etc.
     * @type {string}
     * @public
     */
    id;

    /**
     * Whether this nature can be annotated.
     * @type {Annotable|(false)}
     * @public
     */
    annotable;

    /**
     * The list of entities belonging to this nature.
     * @type {Object[]}
     */
    list = [];

    /**
     * Constructor method for the Nature class
     * @param {Object} settings - the settings
     * @param {string|(false)} annotable - a CSS selector string to select the annotable elements, or false for not annotable
     * @param {Object[]} list - the list of entities
     */
    constructor ({ id, annotable, list }) {
        this.id = id;
        this.annotable = annotable ? new Annotable({ selector: annotable }): false;
        this.list = list;
    }
}

/**
 * Annotable is an (overengineered) class to determine whether
 * a Nature can be annotated and, if so, what is the CSS selector
 * to search the relevant DOM Elements (those created by this nature).
 */
export class Annotable {
    /**
     * The CSS selector to find the annotable elements
     * belonging to the parent nature.
     * @type {string}
     * @public
     */
    selector;

    /**
     * Constructor method for the Annotable class
     * @param {Object} settings - the settings
     * @param {string} selector - the CSS selector to select the annotable elements
     */
    constructor({ selector }) {
        this.selector = selector;
    }
}