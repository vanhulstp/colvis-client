/**
 * A Combination encompasses all possible forms that
 * a combination between two Natures can take. It is
 * thus defined by two Natures and "products" (see dedicated
 * class below).: several
 * annotables properties that are expressed as JavaScript's 
 * primitives. Most properties are Numbers, that can be 
 * rougly described as "low", "average" or "high". 
 * There is one generic annotable, "count", that represents 
 * the number of connections between a given data 
 * unit of a Nature and all other data units of the 
 * combined Nature.
 * 
 * For example: the combination between the "Character"
 * and the "Chapter" Natures produce two annotables:
 * "count" represents the number of chapters in which
 * a character appears and "frequency of apparition" is 
 * a Number that can be assessed as "low", "average" or "high".
 */
export default class Combination {
    /**
     * First nature of the combination
     * @type {Nature}
     * @public
     */
    first;

    /**
     * Second nature of the combination
     * @type {Nature}
     * @public
     */
    second;

    /**
     * The possible results of the combinations.
     * @type {(('count')|Product)[]}
     * @public
     */
    products;

    /**
     * Constructor method for the Combination class
     * @param {Object} settings
     * @param {Nature} first
     * @param {Nature} second
     * @param {(('count')|Product)[]} product
     */
    constructor ({ first, second, products }) {
        this.first = first;
        this.second = second;
        this.products = products;
    }
}

/**
 * A Product is a property that belongs to a 
 * Combination and can be annotated. It is 
 * expressed as a type, a range and a type of
 * range.
 */
export class Product {
    /**
     * The Identifier of the Product
     * @type {string}
     * @public
     */
    id;

    /**
     * Type of the Product. Must
     * be a Javascript's global
     * Object representing a primitive.
     * Example: String
     * 
     * @type {String|Boolean|Number}
     * @public
     */
    type;

    /**
     * The type of range to use.
     * @type {('ordinal'|'linear')}
     * @public
     */
    rangeType;

    /**
     * The range itself.
     * @type {Any[]}
     * @public
     */
    range;

    /**
     * Constructor method for the Product class
     * @param {Object} settings 
     * @param {Object} type 
     * @param {('ordinal'|'linear')} rangeType
     * @param {Any[]} range
     */
    constructor ({ type, rangeType, range }) {
        this.type = type;
        this.rangeType = rangeType;
        this.range = range;
    }
}