export default class ColvisMessage {
    content;
    type;

    constructor (type, content) {
        this.type = type;
        this.content = content;
    }
}