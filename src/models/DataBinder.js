/**
 * An object that links data and dom elements to which the data are bound.
 * @class
 */
export default class DataBinder {
    /**
     * The unique identifier of the object, used to retrieve it
     * across Colvis.
     * @type {string}
     * @public
     */
    id;

    /**
     * The title of the object, to be displayed for human-readable text
     * @type {string}
     * @public
     */
    title;

    /**
     * The data object itself
     * @type {Object}
     * @public
     */
    obj;

    /**
     * The dom element created with the data.
     * @type {HTMLElement}
     * @public
     */
    domElement;

    /**
     * The ID of the Nature that the DataBinder belongs to
     * @param {string} natureId 
     * @public
     */
    natureId;

    /**
     * Creates a DataBinder
     * @param {Object} settings - the settings of the DataBinder
     * @param {string} settings.id - the identifier of the object, usually an id
     * @param {Object} settings.obj an object containing the data
     * @param {Object} settings.natureId the identifier of the Nature
     * @param {HTMLElement} settings.domElement the dom element created with the data
     */
    constructor({id, title, obj, natureId, domElement}) {
        this.id = id;
        this.title = title;
        this.obj = obj;
        this.natureId = natureId;
        this.domElement = domElement;
    }
}
