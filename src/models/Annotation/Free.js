import { Meta } from ".";

/**
 * A free annotation, without structure beside simple tags.
 * @class
 */
export default class FreeAnnotation {
    /**
     * The content of the annotation
     * @type String
     * @public
     */
    annotation

    /**
     * The list of tags
     * @type String[]
     * @public
     */
    tags = []

    /**
     * Type of reply, if any
     * @type {Boolean}
     * @public
     */
    replyType;

    /**
     * replyTo
     * @public
     */
    replyTo;

    /**
     * Meta information regarding the annotation
     * @type {Meta}
     * @public
     */
    meta;

    constructor({ annotation, tags, replyTo, replyType, ci }) {
        this.annotation = annotation;
        this.tags = tags;
        this.replyType = replyType;
        this.replyTo = replyTo;

        this.meta = new Meta(ci);
    }
}