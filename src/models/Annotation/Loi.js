/**
 * Loi - or Level of Interpretation in the classification system - is the depth 
 * of observations made by the analyst, as observable in the annotation.
 * We distinguish three possible levels:
 *  - Visual
 *  - Data
 *  - Meaning
 * For more about Levels of Interpretation, please refer to the Colvis' documentation.
 * @class
 */
export default class Loi {
    /** 
     * Visual value of the level of interpretation. 
     * @type {(boolean}
     * @public
     */
    visual;
    /** 
     * Data value of the level of interpretation. 
     * @type {boolean}
     * @public
     */
    data;
    /** 
     * Meaning value of the level of interpretation. 
     * @type {string|(false)}
     * @public
     */
    meaning;
    

    /**
     * Constructor method of the Pattern class. 
     * @param {Object} loiValue - The pattern value
     * @param {boolean} loiValue.visual - The visual value 
     * @param {boolean} loiValue.data - The data value 
     * @param {string|(false)} loiValue.meaning - The meaning value 
     */
    constructor ({ visual, data, meaning }) {
        this.visual = visual;
        this.data = data;
        this.meaning = meaning;
    }
}