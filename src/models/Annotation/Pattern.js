/**
 * Pattern - or Detected Pattern in the classification system - is a 
 * type of observations made by the analyst in the annotation.
 * We distinguish three possible patterns:
 *  - Singularity, either explicit or implicit
 *  - Duality
 *  - Generality
 * For more about Detected Patterns, please refer to the Colvis' documentation.
 * @class
 */
export default class Pattern {
    /** 
     * Singularity value of the detected pattern. "true" value is considered
     * as explicit by default.
     * @type {('explicit'|'implicit')|boolean}
     * @public
     */
    singularity;
    /** 
     * Duality value of the detected pattern. 
     * @type {boolean}
     * @public
     */
    duality;
    /** 
     * Generality value of the detected pattern. 
     * @type {boolean}
     * @public
     */
    generality;
    

    /**
     * Constructor method of the Pattern class. 
     * @param {Object} patternValue - The pattern value
     * @param {('explicit'|'implicit')|boolean} patternValue.singularity - The singularity value 
     * @param {boolean} patternValue.duality - The duality value 
     * @param {boolean} patternValue.generality - The generality value 
     */
    constructor ({ subjects, complements, reason, specs }) {
        const nature = specs.natures.filter(n => n.id === subjects[0].natureId)[0];
        const ratioSubjectToTotal = subjects.length / nature.list.length;

        if (reason.verb.id === 'sim') {
            this.singularity = false;
            this.duality = true;
            this.generality = false;
        } else {
            if (complements.length && (subjects.length / complements.length < .5 || subjects.length / complements.length >= 2)) {
                this.singularity = 'explicit';
            } else if (!complements.length) {
                this.singularity = 'implicit';
            } else {
                this.singularity = false;
            }

            this.duality = !!complements.length && subjects.length / complements.length >= .5 && subjects.length / complements.length < 2;

            this.generality = ratioSubjectToTotal > .8;    
        }

    }
}