import { RawAnnotation } from '../RawAnnotation';
import DataUnit from './DataUnit';
import Loi from './Loi';
import Pattern from './Pattern';
import NotableFeature from './NotableFeature';
import { hashCode, strfy } from '../../assets/utils';

/**
 * The concept of Annotation is at the heart of Colvis. An annotation 
 * embodies one or several insights fostered by the visualizations and 
 * left by an analyst, either for herself (to support the sense-making 
 * process) or for others (to support collaborative analysis of the 
 * data visualization).
 * 
 * Please refer to the Colvis' documentation for more about Annotations.
 */
export default class Annotation {
    /**
     * The list of data units that this annotation speaks of
     * @type {DataUnit[]}
     * @public
     */
    dataUnits = [];

    /**
     * The Level of Interpretation of the annotation
     * @type {Loi}
     * @public
     */
    loi;

    /**
     * The Detected Patterns of the annotation
     * @type {Pattern}
     * @public
     */
    patterns;

    /**
     * The raw values from the form
     * @type {RawAnnotation}
     * @public
     */
    rawAnnotation;

    /**
     * List of tags
     * @type {String[]}
     * @public
     */
    tags;

    /**
     * Type of reply, if any
     * @type {Boolean}
     * @public
     */
    replyType;
    
    /**
     * Meta information regarding the annotation
     * @type {Meta}
     * @public
     */
    meta;


    /**
     * 
     * @param {Object} settings
     * @param {RawAnnotation} rawAnnotation - The Raw Annotation from which the annotation is computed 
     * @param {Specs} specs - specifications of this visualization
     */
    constructor({ rawAnnotation, specs }) {
        // Process all subject data units
        rawAnnotation.subjects.forEach(subject => {
            this.dataUnits.push(new DataUnit({ role: 'subject', id: subject.id, natureId: subject.natureId, title: subject.title }))
        });

        rawAnnotation.complements.forEach(complement => {
            this.dataUnits.push(new DataUnit({ role: 'complement', id: complement.id, natureId: complement.natureId, title: complement.title }))
        });

        const subjects = this.dataUnits.filter(d => d.role === 'subject');
        const complements = this.dataUnits.filter(d => d.role === 'complement');
        let aggregatedSubjectDataUnit;
        let aggregatedComplementDataUnit;

        if (subjects.length > 1) {
            aggregatedSubjectDataUnit = new DataUnit({ 
                role: 'subject', 
                id: hashCode(strfy(subjects.map(s => s.id))+rawAnnotation.subjectName),
                natureId: subjects[0].natureId,
                title: rawAnnotation.subjectName,
                units: subjects,
                aggregated: true
            });
        }
        
        if (complements.length > 1) {
            aggregatedComplementDataUnit = new DataUnit({ 
                role: 'complement', 
                id: hashCode(strfy(complements.map(c => c.id))+rawAnnotation.complementName),
                natureId: complements[0].natureId,
                title: rawAnnotation.complementName,
                units: complements,
                aggregated: true
            });
        }

        if (aggregatedSubjectDataUnit || aggregatedComplementDataUnit) {
            if (aggregatedSubjectDataUnit) {
                this.dataUnits = this.dataUnits.filter(d => d.role !== 'subject');
                this.dataUnits.push(aggregatedSubjectDataUnit);
            }
            if (aggregatedComplementDataUnit) {
                this.dataUnits = this.dataUnits.filter(d => d.role !== 'complement');
                this.dataUnits.push(aggregatedComplementDataUnit);
            }
        }

        /*this.notableFeatures = new NotableFeature(rawAnnotation.reason);

        this.notableFeatures.dataFeatures.forEach(feat => {
            if (feat.explanationId) {
                const explanation = [].concat(...specs.natures.map(n => n.list)).filter(e => e.id === feat.explanationId)[0];
                this.dataUnits.push(new DataUnit({ role: 'explanation', id: explanation.id, natureId: explanation.natureId }));
            }
        });*/

        this.loi = new Loi({ 
            visual: rawAnnotation.selectionMethods.includes('selector'),
            data: rawAnnotation.selectionMethods.includes('combobox'),
            meaning: !!rawAnnotation.meaning
        });

        this.patterns = new Pattern({ 
            subjects: rawAnnotation.subjects,
            complements: rawAnnotation.complements,
            reason: rawAnnotation.reason,
            specs
        });

        this.rawAnnotation = {...rawAnnotation};
        this.rawAnnotation.subjects = this.rawAnnotation.subjects.map(s => {
            const { domElement, ...subject} = s;
            return subject;
        });
        this.rawAnnotation.complements = this.rawAnnotation.complements.map(c => {
            const { domElement, ...complement } = c;
            return complement;
        })

        this.tags = this.rawAnnotation.tags;

        this.replyType = this.rawAnnotation.replyType;
        this.replyTo = this.rawAnnotation.replyTo;
    }

    /**
     * Recursively remove all null properties on an object. 
     * Used to save memory upon saving the annotation.
     * @param {Object} obj the object to clean
     */
    deleteNullsProps (obj = this, filter) {
        for (let prop in obj) {
            if (obj[prop] === null && prop !== filter) {
                delete obj[prop];
            } else if (typeof obj[prop] === 'object') {
                this.deleteNullsProps(this[prop]);
            }
        }
    }

    /**
     * 
     * @param {Object} ci - Colvis instance
     */
    setMeta (ci) {
        this.meta = new Meta(ci);
    }
}

/**
 * Meta information regarding the annotation
 */
export class Meta {
    /** time at which the annotation is created. Useful to keep track of the succession of annotations */
    timestamp;
    /** hashes of the specs and binders */
    hashes;
    /** state of the visualization */
    state;

    constructor(ci) {
        this.timestamp = Date.now();
        this.state = ci.getVizState();
        this.hashes = {
            specs: ci.getSpecsHash(),
            entities: ci.getEntitiesHash(),
        }
    }
}