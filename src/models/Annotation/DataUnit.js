/**
 * A Data Unit is a "piece" of the overall data that the analyst
 * speaks about in an annotation. For more information about Data
 * Units, please refer to Colvis' documentation.
 * @class
 */
export default class DataUnit {
    /**
     * The role of the Data Unit
     * @type {('complement'|'subject'|'explanation')}
     * @public
     */
    role;

    /**
     * The id corresponding to the DataBinder's.
     * @type {string}
     * @public
     */
    id;

    /**
     * The id of nature of the Data Unit.
     * @type {string}
     * @public
     */
    natureId;

    /**
     * Whether the data unit is aggregated or single.
     * @type {boolean}
     * @public
     */
    aggregated = false;

    /**
     * Single units composing the aggregated data unit,
     * if aggregated. 
     * @type {DataUnit[]}
     * @public
     */
    units = [];


    /**
     * The title of the Data Unit.
     * @type {string|null}
     * @public 
     */
    title = null;

    /**
     * Constructor method for the Data Unit class
     * @param {Object} unit - The Data Unit
     * @param {('complement'|'subject'|'explanation')} role - the role
     * @param {string} id - an id corresponding to a DataBinder's
     * @param {string} natureId - the nature
     */
    constructor({ role, id, natureId, aggregated, units, title }) {
        this.role = role;
        this.id = id;
        this.natureId = natureId;

        this.aggregated = aggregated ? true : false;
        if (units) this.units.push(...units);
        if (title) this.title = title;
    }
}