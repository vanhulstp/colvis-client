/** 
 * A NotableFeature explains why the subject data unit is worth
 * annotating. It consists of a textual verb, and two sets of 
 * features: data-related features and visual-related features.
 * 
 * Data-related features 
 * 
 * Visual-related features are insights about graphical 
 * points of interest: position of the subject, color of
 * the subject, etc.
 */
export default class NotableFeature {
    /**
     * Sentence used to make the annotation.
     * @type {('stands out'|'share similar'|'is similar to')}
     * @public
     */
    verb;

    /**
     * @type {DataFeature[]}
     * @public
     */
    dataFeatures = [];

    /**
     * @type {VisualFeature[]}
     * @public
     */
    visualFeatures = [];

    /**
     * @param {Object} settings
     * @param {Object[]} settings.dataFeatures
     * @param {Object[]} settings.visualFeatures 
     */
    constructor ({ verb, dataFeatures, visualFeatures }) {
        this.verb = verb;
        this.dataFeatures = dataFeatures.map(f => new DataFeature(f));
        this.visualFeatures = visualFeatures.map(f => new VisualFeature(f));
    }
}

/**
 * A DataFeature is an insight about the product of
 * a combination between two data units (the subject and the
 * explanatory data units). 
 * 
 * Example: a product of Jean Valjean and Chapter 3 is
 * the frequency of appearance of the said character
 * for the said chapter. An annotation's DataFeature
 * could be high frequency of appearance for Jean Valjean
 * and Chapter 3.
 */
export class DataFeature {
    /**
     * Subject data unit
     * @type {string}
     * @public
     */
    subjectId;

    /**
     * Explanatory data unit
     * @type {string}
     * @public
     */
    explanationId;

    /**
     * The Identifier of the product you speak about
     * @type {string}
     */
    productId;

    /**
     * The description of the product.
     * "low" and "high" are extrema. "unusual" denotes
     * an outlier.
     * 
     * While seemingly less interesting,
     * "average" can be used in conjuction with
     * another feature to point out a singularity.
     * Example: "this character has an average level
     * of intellect but a high social skill",
     * probably implying that most characters
     * have similarly high intellect and social
     * skill. 
     * @type {('low'|'average'|'high'|'unusual')}
     */
    value;

    constructor ({ subject, explanation, product, value }) {
        this.subjectId = subject.id;
        this.explanationId = explanation[0].id;
        this.productId = product[0].id;
        this.value = value;
    }
}

/**
 * A VisualFeature is an insight about the visual
 * elements of the visualization, without refering
 * the data. It thus refers to the traditional 
 * visual channels used to encode data.
 */
export class VisualFeature {
    value;
}