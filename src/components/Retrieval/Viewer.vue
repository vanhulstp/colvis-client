<template lang="pug">
    svg.col-retrieval-viewer(
        v-if="$colvis.hasViewer()"
        :class="{ active: activeUnits.length }"
        ref="selection"
        :style="{ top: `${compTop}px`, left: `${compLeft}px`, width: `${width}px`, height: `${height}px` }"
    )
        path.col-retrieval-viewer__selected(
            v-for="(el, i) in pathPool" 
            :key="i" 
            :class="{ subject: el.role === 'subject', complement: el.role === 'complement' }"
            :transform="getTransform(el)"
            :d="el.domElement.attributes.d.value"
        )
        circle.col-retrieval-viewer__selected(
            v-for="(el, i) in circlePool"
            :key="i"
            :class="{ subject: el.role === 'subject', complement: el.role === 'complement' }"
            :transform="getTransform(el)"
            :cx="el.domElement.cx.animVal.value"
            :cy="el.domElement.cy.animVal.value"
            :r="el.domElement.r.animVal.value"
        )
        rect.col-retrieval-viewer__selected(
            v-for="(el, i) in rectPool"
            :key="i"
            :class="{ subject: el.role === 'subject', complement: el.role === 'complement' }"
            :transform="getTransform(el)"
            :x="el.domElement.attributes.x.value"
            :y="el.domElement.attributes.y.value"
            :width="el.domElement.attributes.width.value"
            :height="el.domElement.attributes.height.value"
        )
</template>

<script>

/**
 * Create a new Selector.
 * Selectors are the visual interface that allows analysts to select data units from their visualizations.
 * @extends Vue
 */
export default {
    props: {
        /** Whether the visualization has been initialized */
        init: { type: Boolean, required: true },
        /** The list of selected units */
        activeUnits: { type: Array, default: () => [] },
    },
    data () {
        /** The top position of the Selector, in pixels */
        const top = 0;
        /** The left position of the Selector, in pixels */
        const left = 0;
        /** The width of the Selector, in pixels */
        const width = 0;
        /** The height of the Selector, in pixels */
        const height = 0;
        /** Some parent elements of the Selector might be in Position relative  */
        const offsetTop = 0;
        const offsetLeft = 0;
        /** List of event listeners to remove once the component is destroyed */
        const eventListeners = []
        /** ResizeObserver to make sure that the selector matches the SVG it overlays */
        const resizeObserver = null;

        return { top, left, width, height, offsetTop, offsetLeft, eventListeners, resizeObserver };
    },
    computed: {
        /** @returns the list of selected DataBinders, either as subjects or complements. */
        pool() {
            return this.$colvis.getDataBinders(this.activeUnits);
        },

        /** @returns the list of paths within the selected DataBinders. */
        pathPool() {
            return this.pool.filter((binder) => binder.domElement.tagName === 'path');
        },

        /** @returns the list of circles within the selected DataBinders. */
        circlePool() {
            return this.pool.filter((binder) => binder.domElement.tagName === 'circle');
        },

        /** @returns the list of rectangles within the selected DataBinders. */
        rectPool() {
            return this.pool.filter((binder) => binder.domElement.tagName === 'rect');
        },

        compLeft() {
            return this.left - this.offsetLeft;
        },

        compTop() {
            return this.top - this.offsetTop;
        }
    },
    methods: {
        /**
         * Return the translation value of a given element, based on its transformation matrix.
         * It allows to restore the element's precise position despite potential transformations
         * implied by parents.
         * @param {DataBinder} element the element whose translate value is required
         */
        getTransform(element) {
            if (!(element.domElement instanceof SVGGraphicsElement))
                throw new Error('The selected element is not a SVG element');
            const matrix = element.domElement.getCTM();
            return matrix ? `translate(${matrix.e}, ${matrix.f})` : ``;
        },

        /**
         * Get the position of the container of the visualization.
         * Setting { top, left, width, height } properties of the instance.
         */
        getPosition() {
            function position () {
                const target = document.querySelector(this.$colvis.getSpecs().visualization.container);
                if (!target) {
                    throw new Error('Could not locate a target container for the Selector. Please make sure the specifications is set correctly.');
                }

                let offsetTop = 0;
                let offsetLeft = 0;
                let offsetter = this.$refs.selection.parentElement;
                while (offsetter.offsetParent) {
                    offsetTop += offsetter.offsetParent.offsetTop;
                    offsetLeft += offsetter.offsetParent.offsetLeft;
                    offsetter = offsetter.offsetParent;
                }
                const { top, left, width, height } = target.getBoundingClientRect();
                Object.assign(this.$data, { top, left, width, height, offsetTop, offsetLeft });
            }

            position.call(this);

            if (this.height <= 0) {
                console.warn('Could not attach Selector. Will try again in 1 second.');
                const interval = window.setInterval(e => {
                    position.call(this);
                    if (this.height > 0) {
                        console.log('Selector attached');
                        clearInterval(interval);
                    } else console.warn('Could not attach Selector. Will try again in 1 second.');
                }, 1000)
            }
        },
    },
    watch: {
        init () {
            if (this.$colvis.isInit()) this.getPosition();
        }
    },
    mounted () {
        this.getPosition();

        addEventListener('resize', this.getPosition);
        this.eventListeners.push({ resize: this.getPosition });
        /**
         * We resize the selector when the selector's parent's element is resized. We cannot observe
         * SVGs, since there are multiple issues with SVG and bounding boxes.
         */
        this.resizeObserver = new ResizeObserver(e => { console.log('triggering resize'); this.getPosition(); });
        this.resizeObserver.observe(this.$refs.selection.parentElement);
    },
    destroyed () {
        this.eventListeners.forEach(e => {
            Object.keys(e).forEach(key => {
                removeEventListener(key, e[key]);
            });
        });
        this.resizeObserver.disconnect();
    }
}
</script>

<style lang="sass">
@import "../../assets/styles/generic/_animations"
@import "../../assets/styles/generic/_variables"

.col-retrieval-viewer
    position: absolute
    pointer-events: none
    z-index: 4
    
    &.active
        background-color: rgba(sc("red"), .2)

        .col-input-selector__selection, .col-input-selector__selected
            opacity: 1

    &__selection, &__selected
        opacity: .9
        fill: rgba(100, 100, 100, .3)
        stroke: rgb(100, 100, 100)
        mix-blend-mode: multiply

        &.subject
            fill: rgba(sc("blue"), .3)
            stroke: sc("blue")
            stroke-dasharray: 5
            animation: col-rotate 2s infinite
        &.complement
            fill: rgba(sc("yellow"), .3)
            stroke: sc("yellow")
            stroke-dasharray: 5
            animation: col-rotate 2s infinite
</style>
