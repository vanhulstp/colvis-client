# colvis-client example
This example demonstrates how to use the current version of colvis-client.

## Project setup
```
// (in colvis-client/example directory)
yarn install
yarn link 'colvis-client' // assuming you linked colvis-client as proposed in the doc
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
