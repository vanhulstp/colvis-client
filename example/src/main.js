import Vue from 'vue';
import App from './App.vue';
import Colvis from '../../dist/colvis-client.common';
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@mdi/font/css/materialdesignicons.css';
import '../../dist/colvis-client.css';

Vue.config.productionTip = false;

Vue.use(Colvis);
new Vue({
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
